<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('admin')->default(0);
            $table->string('image')->nullable();
            $table->longText('description')->nullable();
            $table->integer('experience')->nullable();
            $table->longText('skills')->nullable();
            $table->integer('totalRead')->default(0);
            $table->float('readTime')->default(0);
            $table->string('website')->nullable();
            $table->string('company')->nullable();
            $table->string('location')->nullable();
            $table->integer('views')->default(0);
            $table->integer('hidden')->default(0);
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->float('rating')->default(0);
            $table->integer('totalRatings')->default(0);
            $table->integer('reviews')->default(0);
            $table->string('badge')->nullable();
            $table->string('cover')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
