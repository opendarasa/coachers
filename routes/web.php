<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/readerajaxfollow','FollowerController@ajaxfollow');
Route::get('/readerajaxunfollow','FollowerController@ajaxunfollow');
Route::post('/savetime','UserController@saveTime');
Route::get('ajaxfollow','FollowerController@ajaxfollow');
Route::get('ajaxunfollow','FollowerController@ajaxunfollow');
Route::get('users-search','UserController@search');
Route::resource('groups','GroupController');
Route::get('blogs/{cat}','BlogController@view_by_category');
Route::get('followers/{username}','UserController@followers');

Route::post('comment-reply','CommentController@reply');
Route::resource('comment','CommentController');
Route::post('/payment/process', 'PaymentController@process')->name('payment.process');
Route::get('all-notification','FollowerController@all_notifications');
Route::get('readNotification','FollowerController@readNotification');

Route::get('follow/{user_id}','FollowerController@add');
Route::get('unfollow/{user_id}','FollowerController@unfollow');

Route::get('/callback','ApiController@getData')
;
Route::get('blog-search','BlogController@search');

Route::post('add-rating','FollowerController@add_rating');

Route::get('/AuthCallback','ApiController@grantAccess');

Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => '4',
        'redirect_uri' => 'http://africanfreelancers.net/AuthCallback',
        'response_type' => 'code',
        'scope' => '*',
    ]);

    return redirect('https://www.opendarasa.com/oauth/authorize?'.$query);
});
Route::get('get-talents','ApiController@getTalents');
Route::get('services',function(){
  return view('services');
});
Route::get('language/{lang}','HomeController@changeLanguage');
Route::post('add_skills','UserController@addSkills');
Route::get('publishBlog/{slug}','HomeController@publishBlog');
Route::get('publishWork/{slug}','HomeController@publishWork');
Route::get('unpublishWork/{slug}','HomeController@unpublishWork');
Route::get('unpublishBlog/{slug}','HomeController@unpublishBlog');
Route::post('profile-type','HomeController@saveprofile');
/*Route::get('portofolio',function(){
  return view('portofolio');
});
Route::get('blog',function(){
   return view('blog');
});*/
Route::get('/delete-skill','UserController@deleteSkill');
Route::post('/save-skill','UserController@saveSkill');
Route::post('/edit-skill','UserController@editSkill');
Route::get('clap/{id}','ClapController@Clap');
Route::get('/like','ClapController@ajaxClap');
Route::get('/unlike','ClapController@ajaxremoveClap');

Route::get('clapr/{id}','ClapController@removeClap');

Route::get('new-web-case', function(){
  return view('newwebdesign');
});
Route::get('new-ui-case', function(){
  return view('newuidesign');
});

Route::post('newuicase','newcaseController@storeui');
Route::get('new-seo-case', function(){
  return view('newposterdesign');
});
Route::get('new-seo-case/{id}','newcaseController@storeSeoClient');
Route::get('new-web-case/{id}','newcaseController@storeWebClient');
Route::get('new-ui-case/{id}','newcaseController@storeMobileClient');
Route::post('newpostercase','newcaseController@storeposter');
Route::post('newwebcase','newcaseController@storeweb');
Route::get('/', function () {
    return redirect('login');
});
Route::get('newcase',function(){
	return view('newcase');
});
Auth::routes();
Route::post('user_edit_pricing','UserController@add_pricing');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('newcase','newcaseController@store');
Route::resource('blog','BlogController');
Route::post('message','HomeController@sendMessage');
Route::resource('portofolio','PortofolioController');
Route::get('add-user',function(){
  return redirect()->route('users.create');
})->middleware('auth');
Route::resource('users','UserController');
Route::get('/oauth/github', 'AuthController@redirectToProvider');
Route::get('/oauth/github/callback', 'AuthController@handleProviderCallback');
Route::post('change_pic','AuthController@changePic');
Route::get('/savecookie','newcaseController@savecookie');

Route::get('/block-user','UserController@blockUser');
Route::get('/unblock-user','UserController@unblockUser');