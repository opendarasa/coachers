<?php

return [
     //footer
   'leave_me_message'=>'Leave us a message',
   'i_always_reply'=>'We always reply!',
   'your_name'=>'Your Name',
    'name_validation_msg'=>'Please Enter Your Name',
    'your_email'=>'Your Email',
     'email_validation_msg'=>'Please Enter a Valid Email',
    'your_message'=>'Your Message',
    'message_validation_msg'=>'Your Message cannot be empty',
   'send_message'=>'Send Message',

   // shared_form 
   'title'=>'Title',
   'subtitle'=>'Subtitle',
   'select_type'=>'Select Type',
   'webdesign'=>'Web Developement',
   'uidesign'=>'Mobile Developement',
   'seo'=>'SEO',
   'content'=>'Post Content',
   'blog_image'=>'Attached Image',
   'tags_input_placeholder'=>'Tags,Separate them with [,]',

   //modal

   'date'=>'Publication Date',
   'client'=>'Client Name',
   'Category'=>'Project Type',
   'close'=>'Close Project',

   //Pricing

   'mobile_dev'=>'Mobile Developement',
   'delivery_in'=>'Delivery in',
   'weeks'=>'Weeks',
   'submit_project'=>'Submit Project',
   'web_design'=>'Web Developement',

   //sort

   'newest_first'=>'Newest First', 
   'oldest_first'=>'Oldest First',

   //testimony

   'what_my_client_think'=>'What Our Clients Think',

   //shared.users

   'worked_at'=>'Worked As',
   'years_experience'=>'years experience',
   'skills'=>'Skills',
   'no_skills-earned_yet'=>'No Kills Earned yet',
   'view_user'=>'View Freelancer',
   'joined'=>'Joined',
   //index
   'welcome_message'=>'Find the right freelancer',
   'welcome_message_continue'=>'Trained and vetted by AfrikaTech and Opendarasa',
   'what_are_you_looking_for'=>'What is your project type?',
   'web_dev'=>'Web Developement',
   'portofolio'=>'Portfolio',
   'team'=>'Freelancers',
   'see_more'=>'See More',
   'latest_bog_post'=>'Latest Blog Posts',
   'find_freelancers'=>'Recruit Freelancers',

   //login 

   'email'=>'Your email',
   'password'=>'Password',
   'login'=>'Login',
   'cancel'=>'Cancel',
   'forgot'=>'Forgot',
   'rebember_me'=>'remember me',
   'forgot_password'=>'Forgot password?',

   //nav

   'menu'=>'Menu',
   'home'=>'Home',
   'my_services'=>'Services',
   'see_all'=>'see all',
   'my_work'=>'Portfolio',
   'blog'=>'Blog',
   'new_blog'=>'Add Blog Post',
   'new_work'=>'Add New Work',
   'profile'=>'Profile',
   'new_user'=>'Add User',

   //newcase-template

   'submit'=>'Submit',
   'next'=>'Next',
   'previous'=>'Previous',
   //create new blog

   'new_blog'=>'New Blog',
   'save'=>'Save',
   'blog_types'=>[
          '1'=>'Enterprise',
          '2'=>'Science',
          '3'=>'Advertising',
          '4'=>'Media',
          '5'=>'Entertainment',
          '6'=>'Transport'
   ],

   //save new skill

   'skill_levels'=>[
          '1'=>'Beginner',
          '2'=>'Intermediate',
          '3'=>'Expert',
   ],
   //new work
   'new_work'=>'Add New Work',

   //edit blog or work
   'publish'=>'Publish',
   'unpublish'=>'Unpublish',
   'preview'=>'Preview',
   //submit seo
   'tell_me_about_your_company'=>'Tell us about your company',
   'company_name'=>'Company Name',
   'company_industry'=>'Company Industry',
   'your_personal_information'=>'Personal Info',
   'first_name'=>'Your First Name',
   'last_name'=>'Last Name',
   'contact_information'=>'Contact Information',
   'mobile'=>'Mobile Number',
   'about_your_event'=>'About your SEO project',
   'describe_your_event'=>'Describe your services',
   'do_your_booking'=>'Book your delivery Date',
   'upload_your_pdf_files'=>'Upload your .zip or PDF file',

   //submit mobile project

   'verify_email'=>'We just just you an email ,Verify your e-mail',
   'android'=>'Android',
   'ios'=>'IOS',
   'about_your_project'=>'Tell us about your  project',
   'describe_your_project'=>'Describe your project',
   'do_you_have_ux'=>'Upload your project .zip file or PDF file(UX/UI)',

   //view blog

   'latest_work'=>'Latest Works',
   'popular_post'=>'Popular Posts',
   'author'=>'Author',
   'follow_us'=>'Follow Us',
   'share'=>'Share',
   'pricing'=>'Pricing',
   //order details
   'web_dev_order_detail'=>'Web Developement Project Details',
   'client_email'=>'Client Email',
   'client_mobile'=>'Client Mobile Phone Number',
   'companyname'=>'Company Name',
   'companyIndustry'=>'Company Industry',
   'booking_date'=>'Delivery Date',
   'project_description'=>'Project Description',
   'mobile_dev_order_detail'=>'Mobile Developement Project Description',
   'seo_dev_order_detail'=>'SEO Project Description', 
   'freelancer_name'=>'Freelancer Name',
   'new_seo_case'=>'Your SEO project Order Details',
   'seo_msg'=>'We have just received your SEO project and will get in contact with you most lately tomorrow.',
  'order_more'=>'Order more',

  'your_ui_case_was_received'=>'Your Mobile Developement project Order Details',
  'your_ui_message'=>'We have just received your Mobile Developement project and will get in contact with you most lately tomorrow.',
  'you_have_just_made_a_web_request'=>'Your Web Developement project Order Details',
  'new_web_design_msg'=>'We have just received your Web Developement project and will get in contact with you most lately tomorrow.',
  //view user
  'upload_new_photo'=>'Change Profile Photo',
  'recent_badges'=>'Skills',
  'git_repos'=>'Github Repositories',
  'repos_yet'=>'No Github Repositories',
   'full_name'=>'Full Name',
   'username'=>'Username',
   'email'=>'Email',
  'description_user'=>'About You',
  'company'=>'Latest Working Company',
  'website'=>'Personal website',
  'skills'=>'Skills',
  'you_have_no_earned_skills_yet'=>'The skills are only given by Opendarasa',
  'repos'=>'Repositories',
  'edit'=>'Edit Profile',
  'hire_me'=>'Hire Me',
  'about'=>'About Freelancer:',
  'select_project_type'=>'Select Project Type',
  'reminder_about_pricing'=>'The prices below can are standard and can be reviewed',
  'short_description'=>'Describe you freelancing experience',
  'import_repos'=>'Import Repositories',
  'save_changes'=>'Save Changes',
  'add_new_blog'=>'Add Blog Post',
  'add_new_work'=>'Add New Work',

  //add user

  'welcome_subject'=>'Congratulations!You are now a certified Freelancer',
  'new_account_intro'=>'You have just been granted access to the best african freelancers communuty.',
  'welcome_msg_body'=>'You have done it ! after reviewing your profile and your projects , we have come to the decision that you having as member of our developers communuty  will be huge plus therefore on behalf of all the members , I would like to give you a warm welcome. Please go ahead and find your account credentials below.

   It is now your turn to show your brillant skills and make cash out of them.',
'enter_username'=>'Enter username',
  'add_skills'=>'Add skills',

  'description'=>'Find the right freelancer for your project, trained by AfrikaTech and Opendarasa',
  'my_latest_posts'=>'My latest blog posts',
  'location'=>'Location',

  //  meta image and description
  'blogtagline'=>'Read from the best talents in Africa.',
  'blogdescription'=>'Find the best tech and freelance ideas in africanfreelancers.net.',
  'defaulttagline'=>'Find the right freelancer for your project, trained by AfrikaTech and Opendarasa',
  'portfoliotagline'=>'Find more 1000 african freelancers portofolio in one place',
   'protfoliodescription'=>'Find more 1000 african freelancers portofolio in one place',
   'usertagline'=>'Find the right freelancer for your project, trained by AfrikaTech and Opendarasa',
   'userdescription'=>'Find the right freelancer for your project, trained by AfrikaTech and Opendarasa',
   'public'=>'Your profile is now public',
   'private'=>'Only you can see this',
   'claps'=>'Claps',

   'project_time'=>[
                    '(1)One Week'=>'(1)One Week',
                    '(1)One Week ~ (1)One Month'=>'(1)One Week ~ (1)One Month',
                    '(1)One Month'=>'(1)One Month',
                    '(1)One Month ~ (3)Three Months'=>'(1)One Month ~ (3)Three Months',
                    '(3)Tree Months'=>'(3)Three Months',
                    '(3)Three Months ~ (6)Six Months'=>'(3)Three Months ~ (6)Six Months',
                    '(6)Six Months'=>'(6)Six Months',
                    '(6)Six Months ~ (12)Twelve Months'=>'(6)Six Months ~ (12)Twelve Months',
                    '(12) Twelve Months +'=>'(12) Twelve Months +',

                    ],
    'connect_with_opendarasa'=>'Connect With Opendarasa',

  






];