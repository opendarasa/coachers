<?php

return [
     //footer
	'leave_me_message'=>'Envoyez nous votre message',
	'i_always_reply'=>'Nous sommes toujours à l\'écoute!',
	'your_name'=>'Votre Nom',
    'name_validation_msg'=>'Entrez votre nom',
    'your_email'=>'Entrez votre email',
     'email_validation_msg'=>'Entrez un email valide',
    'your_message'=>'Votre Message',
    'message_validation_msg'=>'Votre Message ne peut être vide',
   'send_message'=>'Envoyez',

   // shared_form 
   'title'=>'Titre',
   'subtitle'=>'Sous-titre ',
   'select_type'=>'Choisir le type de Poste',
   'webdesign'=>'Dévelopement Web',
   'uidesign'=>'Dévelopement Mobile',
   'seo'=>'SEO',
   'content'=>'Contenu du Poste',
   'blog_image'=>'Image de prévision',
   'tags_input_placeholder'=>'mots clés,Séparer les mots pas[,]',
   'blog_types'=>[
          '1'=>'Entreprise',
          '2'=>'Science',
          '3'=>'Publicité',
          '4'=>'Media',
          '5'=>'Show biz',
          '6'=>'Transport'
   ],

   //modal

   'date'=>'Date de Publication',
   'client'=>'Nom du Client',
   'Category'=>'Type de Projet',
   'close'=>'Fermer projet',

   //Pricing

   'mobile_dev'=>'Dévelopement Mobile',
   'delivery_in'=>'Temps ',
   'weeks'=>'Semaines',
   'submit_project'=>'Soumettre Projet',
   'web_design'=>'Dévelopement Web',

   //sort

   'newest_first'=>'Les plus nouveaux', 
   'oldest_first'=>'Les plus Anciens',

   //testimony

   'what_my_client_think'=>'Ce que nos clients pensent',

   //shared.users

   'worked_at'=>'Expérience Professionelle',
   'years_experience'=>'années d\'Expérience',
   'skills'=>'Compétences',
   'no_skills-earned_yet'=>'Pas encore de Compétences',
   'view_user'=>'Voir Free-lancer',
   'joined'=>'à rejoint',
   //index
   'welcome_message'=>'Touvez les meilleurs Free-lancers',
   'welcome_message_continue'=>'Formés et suivis par Afrikatech et Opendarasa',
   'what_are_you_looking_for'=>'Quel est votre type de projet?',
   'web_dev'=>'Dévelopement Web',
   'portofolio'=>'Portfolio',
   'team'=>'Freelancers',
   'see_more'=>'Voir Plus',
   'latest_bog_post'=>'Nos dernières Publications',
   'find_freelancers'=>'Je veux Embaucher',

   //login 

   'email'=>'Email',
   'password'=>'Mot de Passe',
   'login'=>'S\'identifier',
   'cancel'=>'Annuller',
   'forgot'=>'oublié',
   'rebember_me'=>'Rester connecté',
   'forgot_password'=>'Mot de passe oublié?',

   //nav

   'menu'=>'Menu',
   'home'=>'Acceuil',
   'my_services'=>'Services',
   'see_all'=>'Voir Tout',
   'my_work'=>'Portfolio',
   'blog'=>'Blog',
   'new_blog'=>'Nouveau article',
   'new_work'=>'Nouveau Projet',
   'profile'=>'Profil',
   'new_user'=>'Ajouter Utilisateur',

   //newcase-template

   'submit'=>'Sousmettre',
   'next'=>'Suivant',
   'previous'=>'Précédent',
   //create new blog

   'new_blog'=>'Nouveau article',
   'save'=>'Sauvergarder',
   //new work
   'new_work'=>'Ajouter Nouveau Projet',

   //edit blog or work
   'publish'=>'Publier',
   'unpublish'=>'Rendre privé',
   'preview'=>'Previsualiser',
   //submit seo
   'tell_me_about_your_company'=>'À propos de votre entreprise',
   'company_name'=>'Nom d\'entreprise',
   'company_industry'=>'Que fait votre entreprise? ',
   'your_personal_information'=>'Information personelles',
   'first_name'=>'Nom',
   'last_name'=>'Prénom',
   'contact_information'=>'Vos Contacts',
   'mobile'=>'Numéro tel',
   'about_your_event'=>'À Propos de votre projet SEO',
   'describe_your_event'=>'Décrivez votre projet',
   'do_your_booking'=>'Choisir délai de livraison',
   'upload_your_pdf_files'=>'Sousmettre un fichier .zip ou PDF du cahier de charge',

   //submit mobile project

   'verify_email'=>'Nous vous avons envoyé un email',
   'android'=>'Android',
   'ios'=>'IOS',
   'about_your_project'=>'À Propos de votre projet',
   'describe_your_project'=>'Décrivez en quelques les points importants du projet',
   'do_you_have_ux'=>'Soumettre le cahier de charge en format .zip ou PDF (UX/UI)',

   //view blog

   'latest_work'=>'Nos dernières réalisation',
   'popular_post'=>'Les articles les plus lus',
   'author'=>'Auteur',
   'follow_us'=>'Suivez Nous',
   'share'=>'Partager',
   'pricing'=>'Tarification',
   //order details
   'web_dev_order_detail'=>'Détails d\'un projet web',
   'client_email'=>'Email du Client',
   'client_mobile'=>'Tel du Client',
   'companyname'=>'Nom d\'entreprise ',
   'companyIndustry'=>'Secteur d\'opération',
   'booking_date'=>'Délai d\'execution',
   'project_description'=>'Description du projet',
   'mobile_dev_order_detail'=>'Détails d\'un projet d\'Application mobile',
   'seo_dev_order_detail'=>'Détails d\'un projet SEO', 
   'freelancer_name'=>'Nom du Free-lancer',
   'new_seo_case'=>'Détails de votre commande : Projet SEO',
   'seo_msg'=>'Nous avons réçu votre projet d\'analyse SEO et vous enverrons les meilleurs dévis et offres de nos free-lancers',
  'order_more'=>'Faire une autre commande',

  'your_ui_case_was_received'=>'Les Détails de votre projet d\'Application mobile',
  'your_ui_message'=>'Nous venons de recevoir votre projet de réalisation d\'une Application mobile et vous enverrons les dévis des meilleurs free-lancers au plus tard demain',
  'you_have_just_made_a_web_request'=>'Les Détails de votre commande de developpement Web',
  'new_web_design_msg'=>'Nous venons de recevoir votre projet de réalisation d\'un site web et vous enverrons les dévis des meilleurs free-lancers au plus tard demain.',
  //view user
  'upload_new_photo'=>'Changer Photo de profile',
  'recent_badges'=>'Compétences',
  'git_repos'=>'Github Repositories',
  'repos_yet'=>' Aucun Github Repositories',
   'full_name'=>'Nom Complet',
   'username'=>'Nom d\'Utilisateur',
   'email'=>'Email',
  'description_user'=>'À Propos de Vous',
  'company'=>'Dernier Poste Occuppé',
  'website'=>'Site web personel',
  'skills'=>'Compétences',
  'you_have_no_earned_skills_yet'=>'Les Compétences sont données uniquement par Opendarasa.',
  'repos'=>'Repositories',
  'edit'=>'Modifier Profil',
  'hire_me'=>'Embauche  Moi',
  'about'=>'À Propos du Free-lancer:',
  'select_project_type'=>'Choisir le type du Projet',
  'reminder_about_pricing'=>'Les Prix listés sont standards et négotiables',
  'short_description'=>'Décrivez votre Expérience matière de free-lance',
  
// add user
  'welcome_subject'=>'Félicitations! Vous Ëtes Maintenant Free-lancer Certifié! ',
  'new_account_intro'=>'Vous venez juste d\'être retenus comme membre de la communauté des free-lancers développeurs africains.',
  'welcome_msg_body'=>'Tout est bien qui fini bien ! Vous avez donné le meilleur de vous même en prenant les cours sur Opendarasa, vous avez également suivi toutes les évaluations d\'aptitudes et d\'attitudes réalisées par Opendarasa en collaboration avec Afrikatech , aujourd\'hui vos efforts vous donnent le droit d\'accès à la communauté des meilleurs talents sur le continent. Profitez-en tout en faisant du cash sur notre plateforme.Veuillez trouver les détails de votre compte ci-dessous.Encore Bienvenu  et faites comme chez vous!',
  'enter_username'=>'Entrez nom d\'Utilisateur',
  'add_skills'=>'Ajouter Compétences',

  'description'=>'Trouvez et recrutez les meilleurs free-lancers africains formés et suivis par Afrikatech et Opendarasa',
  'my_latest_posts'=>'mes derniers articles',
  'location'=>'Ville',

  'blogtagline'=>'Trouvez les meilleurs articles écrits par les talents africains.',
  'blogdescription'=>'Trouvez les meilleurs idées de tech sur www.africanfreelancers.net',
  'defaulttagline'=>'Trouvez et recrutez les meilleurs free-lancers africains formés et suivis par Afrikatech et Opendarasa.',
  'portfoliotagline'=>'Trouvez et recrutez les meilleurs free-lancers africains formés et suivis par Afrikatech et Opendarasa.',
   'protfoliodescription'=>'Trouvez et recrutez les meilleurs free-lancers africains formés et suivis par Afrikatech et Opendarasa.',
   'usertagline'=>'Trouvez et recrutez les meilleurs free-lancers africains formés et suivis par Afrikatech et Opendarasa.',
   'userdescription'=>'Trouvez et recrutez les meilleurs free-lancers africains formés et suivis par Afrikatech et Opendarasa.',
   'public'=>'Votre profile est publique.',
   'private'=>'Votre profile est présentement privé.',
   'claps'=>'Applaudissement(s)',

   'project_time'=>[
                    '(1)One Week'=>'(1)One Week',
                    '(1)One Week ~ (1)One Month'=>'(1)One Week ~ (1)One Month',
                    '(1)One Month'=>'(1)One Month',
                    '(1)One Month ~ (3)Three Months'=>'(1)One Month ~ (3)Three Months',
                    '(3)Tree Months'=>'(3)Three Months',
                    '(3)Three Months ~ (6)Six Months'=>'(3)Three Months ~ (6)Six Months',
                    '(6)Six Months'=>'(6)Six Months',
                    '(6)Six Months ~ (12)Twelve Months'=>'(6)Six Months ~ (12)Twelve Months',
                    '(12) Twelve Months +'=>'(12) Twelve Months +',

                    ],
'connect_with_opendarasa'=>'Se connecter avec Opendarasa',
  





];