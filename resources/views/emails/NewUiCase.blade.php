@component('mail::message')
# New UI Design Case

You have a new UI DESIGN CASE . See Details in attached file.

@component('mail::button', ['url' =>config('app.url')])
visit website
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
