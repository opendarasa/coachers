@component('mail::message')
# {{trans('app.your_ui_case_was_received')}}

{{trans('app.your_ui_message')}}.

@component('mail::button', ['url' =>config('app.url')])
{{trans('app.order_more')}}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
