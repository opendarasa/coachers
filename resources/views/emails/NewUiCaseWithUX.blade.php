@component('mail::message')
# New UI Case with Attached  UX.

You have got a new UI Design case with UX provided.

@component('mail::button', ['url' =>$ux])
download ux
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
