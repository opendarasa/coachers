@component('mail::message')
# New talents Found on Opendarasa

{{$name}} is a potential new talent has he has just finished the course {{$course}}.



Thanks,<br>
{{ config('app.name') }}
@endcomponent
