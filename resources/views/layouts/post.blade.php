<!DOCTYPE html>
<html lang="en">
  @include('layouts.head2')
   @include('shared.meta')
  <body>
  
     @include('layouts.nav2')


    	  

     <!-- ==============================================
     Banner Section
     =============================================== -->
	 <section class="banner-comment" style="background:url('{{asset('img/blog/'.$blog->image)}}');background-repeat: no-repeat;background-size:100%;" >
	  <div class="container" style="background-color: rgba(255, 255, 255, 0.4);">
	   <h1 style="color:#464a4c;">{{$blog->title}}</h1>
	  </div><!-- /container -->
	 </section><!-- /section -->	


     <!-- ==============================================
	 Post Content
	 =============================================== -->
     <article class="entry-content-2">
      <div class="container">
       <div class="row">
        <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
		<p style="font-weight: bold;:">{{$blog->subtitle}}</p>
		<div class="clearfix"></div>
		{!!$blog->content!!}
		<?php $class='pull-right' ?>
		   @include('shared.clap')
        </div><!-- /col-lg-8 -->
       </div><!-- /row -->
      </div><!-- /container -->
     </article><!-- /article -->	 
     <div class="clearfix">
     	
     </div>
     @include('shared.about-poster') 

     @include('shared.related-posts')
	 
      @include('layouts.comments')	   
	
     @include('layouts.footer2')
	 
     @include('layouts.js2')

     <script type="text/javascript">
      var timeSpent=0;
      window.onbeforeunload = function() {
          saveFormData();
          return null;
      }

      function saveFormData() {
          var user_id="{{Auth::user()->id}}";
          var blog_id="{{$blog->id}}";
          var _token="{{csrf_token()}}";
          if(timeSpent>=60)
          {
            $.ajax({
            type:"post",
            url:"/savetime",
            data:{user_id:user_id,blog_id:blog_id,_token:_token,timeSpent:timeSpent},
            success:function(data,status){
              console.log(status);
            },
            error:function(data,status){
              console.log(data);
            }
          })
          }
          
      }

       setInterval(function(){
        timeSpent=timeSpent+1;
       },1000);

     
     </script>

  </body>
</html>