<!-- Bootstrap core JavaScript -->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap2-toggle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Contact form JavaScript -->
    <script src="{{asset('js/jqBootstrapValidation.js')}}"></script>
    <script src="{{asset('js/contact_me.js')}}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{asset('js/agency.min.js')}}"></script>
   <script type="text/javascript">
   	$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
   	
   	$("#bell").hover($("#notifylist").show(),$("#notifylist").hide())

   	function readNotification(id) {
    $.ajax({
             type: "get",
              url:"/readNotification",
              data:{id:id},
              success:function(data,status){
                 console.log(status);
              },
               error:function(data,status){
                   console.log(status);
            }
});
  }
   </script>