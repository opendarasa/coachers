<!-- Contact -->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.leave_me_message')}}</h2>
            <h3 class="section-subheading text-muted">{{trans('app.i_always_reply')}}</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <form id="contactForm" name="sentMessage" novalidate="novalidate" action="{{url('message')}}" method="post">
              {{csrf_field( )}}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input name="name" class="form-control" id="name" type="text" placeholder="{{trans('app.your_name')}} *" required="required" data-validation-required-message="{{trans('app.name_validation_msg')}}">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input name="email" class="form-control" id="email" type="email" placeholder="{{trans('app.your_email')}} *" required="required" data-validation-required-message="{{trans('app.email_validation_msg')}}.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input name="phone" class="form-control" id="phone" type="tel" placeholder="{{trans('app.your_phone')}}" >
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea name="message" class="form-control" id="message" placeholder="{{trans('app.your_message')}} *" required="required" data-validation-required-message="{{trans('app.message_validation_msg')}}"></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">{{trans('app.send_message')}}</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; AfricanFreelancers 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>