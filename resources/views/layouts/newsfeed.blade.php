<!DOCTYPE html>
<html lang="en">
 
 @include('layouts.head2')
  <body>
  @include('layouts.nav2')
     

     <!-- ==============================================
     Posts Section
     =============================================== -->		  
	  
	 <section class="posts-2">
	  <div class="container-fluid">
	   <div class="row">

        @include('shared.panel')	  

        <div class="col-md-6"> 
		
	     @yield('postbox')
		
		 @yield('feed')
		 
		 
        </div><!-- /.col-md-6 -->	
		
        <div class="col-md-3"> 

		  @yield('mutuals')
		
	      @yield('ad')	
		
        </div><!-- /.col-md-3 -->	
	   
	   </div><!-- /.row -->
	  </div><!-- /.container -->
     </section><!-- /.section -->
	 
	 <!-- ==============================================
	 More Section
	 =============================================== -->		  
	@yield('readmore')
    @include('layouts.footer2')
	 
     
	 
    @include('layouts.js2')

  </body>
</html>
