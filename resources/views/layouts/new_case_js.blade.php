 <!-- Bootstrap core JavaScript -->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <!-- piexif.min.js is needed for auto orienting image files OR when restoring exif data in resized images and when you
    wish to resize images before upload. This must be loaded before fileinput.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/piexif.min.js" type="text/javascript"></script>
<!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview. 
    This must be loaded before fileinput.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/sortable.min.js" type="text/javascript"></script>
<!-- purify.min.js is only needed if you wish to purify HTML content in your preview for 
    HTML files. This must be loaded before fileinput.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/purify.min.js" type="text/javascript"></script>
<!-- popper.min.js below is needed if you use bootstrap 4.x. You can also use the bootstrap js 
   3.3.x versions without popper.min.js. -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>
<!-- optionally uncomment line below for loading your theme assets for a theme like Font Awesome (`fa`) -->
<!-- script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/themes/fa/theme.min.js"></script -->
<!-- optionally if you need translation for your language then include  locale file as mentioned below -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/locales/LANG.js"></script>



<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js">
    
</script>
    <!-- Plugin JavaScript -->
<script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Contact form JavaScript -->
<script src="{{asset('js/jqBootstrapValidation.js')}}"></script>
<script src="{{asset('js/contact_me.js')}}"></script>

    <!-- Custom scripts for this template -->
<script src="{{asset('js/agency.min.js')}}"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script src="{{asset('js/rater.js')}}" charset="utf-8"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">

  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
	
  function readNotification(id) {
    $.ajax({
             type: "get",
              url:"/readNotification",
              data:{id:id},
              success:function(data,status){
                 console.log(status);
              },
               error:function(data,status){
                   console.log(status);
            }
});
  }
  
  $(document).ready(function() {
    var rating=0;
    @if(isset($user))
     $(".rate").rate();
        $(".torate").rate();
                $(".torate").on("change", function(ev, data){
                
                console.log(data.from, data.to);

                rating=data.to;
            });
      $("#saverating").click(function(){
                if(rating==0)
                {

                  $("#ratinghelp").html("{{__('app.rating_cannot_be_empty')}}");

                  return false;

                }else{
                var _token="{!!csrf_token()!!}";
                var user_id="{{$user->id}}";
                var comment=$("#ratingcomment").val();
                $.ajax({
                    url:"{{url('add-rating')}}",
                    type:'POST',
                    data:{_token:_token,user_id:user_id,rating:rating,comment:comment},
                    success:function(data2,status){
                        var result=JSON.parse(data2);
                        if(result.status=='success'){
                          swal({
                                title: "{{__('app.rating_success')}}",
                                text:result.message,
                                icon: "success",
                                button: "ok",
                              }).then((value) => {
                                    location.reload();
                                  });
                        }else{
                          console.log(result.message);
                          swal({
                                title: "{{__('app.rating_error')}}",
                                text:result.message,
                                icon: "error",
                                button: "ok",
                              }).then((value) => {
                                    location.reload();
                                  });

                        }
                        //location.reload();
                    },
                    error:function(data2,status){
                        console.log(data2)
                    }

                    });
                }
                
      })
    @endif
         $("#myfile").fileinput({
        showPreview:true,
        showUpload: false,
        elErrorContainer: '#kartik-file-errors',
        allowedFileExtensions: ["jpg", "png", "gif"],
        @if(isset($blog->image))
         <?php 
         $path=public_path('img/blog');
          $image=$path.'/'.$blog->image;

          

         
         ?>
         initialPreview: [
        "<img src='{{asset('/img/blog/'.$blog->image)}}' class='file-preview-image' title='Desert' width='120' >"
       ],
       initialPreviewAsData: false, // identify if you are sending preview data only and not the raw markup
       initialPreviewFileType: 'image',
        initialPreviewConfig: [
        {caption: "{{$blog->image}}", size:{{filesize($image)}}, width: "120px", url: "{{$image}}"},
         ],
        
        @elseif(isset($work->image))
         <?php 
         $path=public_path('img/portfolio');
          $image=$path.'/'.$work->image;
         
         ?>
         initialPreview: [
        "<img src='{{asset('/img/portfolio/'.$work->image)}}' class='file-preview-image' title='Desert' width='120' >"
       ],
       initialPreviewAsData: false, // identify if you are sending preview data only and not the raw markup
       initialPreviewFileType: 'image',
        initialPreviewConfig: [
        {caption: "{{$work->image}}", size:{{filesize($image)}}, width: "120px", url: "{{$image}}"},
         ],
        @endif
    });

});
          
  $("#sort").change(function(){
   if($(this).val()=='Oldest'){
    //alert()
    $("#desc").css({display:"none"});
    $("#asc").css({display:"none"});
   }
   else if($(this).val()=="Newest")
   {
   	//alert("alert");
    $("#asc").css({display:"none"});
    $("#desc").css({display:"block"});
    
   }
  });



  

</script>


<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the crurrent tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";

  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "{{trans('app.submit')}}";
    
  } else {
    document.getElementById("nextBtn").innerHTML = "{{trans('app.next')}}";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
    
}

function nextPrev(n) {
  // This function will figure out which tab to display
  
  var x = document.getElementsByClassName("tab");
  var div=document.getElementsByTagName('div');
  //console.log(n);

  // get input value 
  var topic=document.getElementById("topic");
  var category=document.getElementById("category");
  var content=document.getElementById("content");
  var name=document.getElementById("name");
  var email=document.getElementById('email');
  var phone=document.getElementById("phone");


  // assign input value for view if n==1 
  if (n==1) {


    if(topic.value!="")
  {
    document.getElementById("ctopic").innerHTML="{{__('app.topic')}}: "+topic.value;
  }
  if(category.value!="")
  {
    var blog_types=new Array();
    @foreach(trans('app.blog_types') as $key=>$value)
      blog_types[{{$key}}]="{{$value}}";
    @endforeach
    
    document.getElementById("ccategory").innerHTML="{{__('app.category')}}: "+blog_types[category.value];
  }
  if(content.value!="")
  {
    document.getElementById("ccontent").innerHTML="{{__('app.content')}}: "+content.value;
  }
  if(name.value!="")
  {
    document.getElementById("cname").innerHTML="{{__('app.name')}}: "+name.value;
  }
  if(email.value!="")
  {
    document.getElementById("cemail").innerHTML="{{__('app.email')}}: "+email.value;
  }
  if(phone.value!="")
  {
    document.getElementById("cphone").innerHTML="{{__('app.phone')}}: "+phone.value;
  }

  }
  
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  if (n==1) {
   document.getElementById('step_'+currentTab).className="activebox"; 
   document.getElementById('step_'+(currentTab+1)).className="currentbox";
 }else{
  document.getElementById('step_'+currentTab).className="box";
  document.getElementById('step_'+(currentTab-1)).className="currentbox";

 }
  
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;

  console.log(x.length);
  console.log(currentTab);
  // if you have reached the end of the form...
  
  if (currentTab == 4 && n==1) {
    // ... the form gets submitted:
    
    document.getElementById("nextBtn").setAttribute("type","submit");
    return false;
  }
  
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  var textContent=x[currentTab].getElementsByTagName("textarea");
  var select=x[currentTab].getElementsByTagName("select");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }

  if (textContent.length>0) {
    {
      for (var i = 0; i < textContent.length; i++) {
        if(textContent[i].value==""){
          textContent[i].className+=" invalid";
          valid=false;
        }
      }
    }
  }

  if (select.length>0) {
    {
      for (var i = 0; i < select.length; i++) {
        if(select[i].value==""){
          select[i].className+=" invalid";
          valid=false;
        }
      }
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}


</script>


    <script type="text/javascript">
    






  </script>

  



