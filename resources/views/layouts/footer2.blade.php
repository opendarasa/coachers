 <style type="text/css">
 	.bottom-footer{
 		bottom:0;
 		right:0;
 		left:0;
 		z-index:2;
 		position:fixed;
 	}
 	@media screen and (max-width: 1000px){
 		.bottom-footer{
 			margin-top:550px;
 			position:fixed;
 		}
 	}
 </style>
 <!-- ==============================================
     Footer Section
     =============================================== -->	
     <footer class="bottom-footer" >
	  <div class="container">
	   <p><span>©{{date('Y')}} {{config('app.name')}}</span></p>			
		 
	   <ul id="menu-footer-menu">
	    <li>
	     <a href="#">Privacy</a>
	    </li><!-- /li -->
		<li>
		 <a href="#">Terms & Conditions</a>
		</li><!-- /li -->
		
	   </ul><!-- /ul -->	
	   
	  </div><!-- /container -->
     </footer><!-- /footer -->
     <a id="scrollup">Scroll</a>