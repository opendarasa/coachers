<!DOCTYPE html>
<html lang="en">
<meta name="viewport" content="width=device-width, initial-scale=1">
  @include('layouts.head2')
  <body>
  
     @include('layouts.nav2')

    @include('shared.posts')
	 
	@include('shared.readmore') 
	
     @include('layouts.footer2')
    @include('layouts.js2')

  </body>
</html>
