<!DOCTYPE html>
<html lang="en">
  @include('layouts.head2')
  <body>
  
     @include('layouts.nav2')

    @include('shared.related-posts')
	 
	@include('shared.readmore') 
	
     @include('layouts.footer2')
    @include('layouts.js2')

  </body>
</html>
