<!DOCTYPE html>
<html lang="en">
<head>
  @include('layouts.head')
</head>
  

  <body id="page-top">

    @include('layouts.nav')

    <!-- Header -->
    <header class="masthead" style="margin-bottom:2%;">
      <div class="container">
      
      
      <div class="intro-text" style="color:black;">
        <div class="box" style="background-color: rgba(255, 255, 255, 0.5);">
          <div class="intro-lead-in">{{trans('app.welcome_message')}}</div>
          <div class="intro-heading text-uppercase" style="font-size:30px;">{{trans('app.welcome_message_continue')}}</div>
          </div>
          <!--<a class="" href="#services" style=" display:inline-block; background:white;color:black;">{{trans('app.submit_project')}}</a>
          <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="{{url('users')}}" style="background:white;color:black; display:inline-block;">{{trans('app.find_freelancers')}}</a>-->
          @include('shared.search_form')
        </div>
      
      </div>
    </header>

    <!-- Services -->
    <section style="display:none;" id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.what_are_you_looking_for')}}</h2>
            <!--<h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>-->
            <hr width="50%" size="10px">
          </div>
        </div>
        <div  class="row text-center">
          <div class="col-md-4">
            
            
              <span class="fa-stack fa-5x">
              <!--<i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-mobile fa-stack-1x fa-inverse"></i>
              -->
              <a href="{{url('new-ui-case')}}">
              <img src="{{asset('img/mobie.png')}}" width="100%">
              </a>
            </span>
            <h4 class="service-heading">{{trans('app.mobile_dev')}}</h4>
          
            <!--<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>-->
          </div>
          <div class="col-md-4">
            
            <span class="fa-stack fa-5x">
              <!--<i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-desktop fa-stack-1x fa-inverse"></i>
            -->
            <a href="{{url('new-web-case')}}">
            <img src="{{asset('img/webdesign.png')}}" width="100%">
            </a>
            </span>
            <h4 class="service-heading">{{trans('app.web_dev')}}</h4>
          
            <!--<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>-->
          </div>
          <div class="col-md-4">
            
            <span class="fa-stack fa-5x">
              <!--<i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-pencil-square-o fa-stack-1x fa-inverse"></i>
            -->
            <a href="{{url('new-seo-case')}}">
            <img src="{{asset('img/SEO.png')}}" width="100%">
            </a>
            </span>
            
            <h4 class="service-heading">{{trans('app.seo')}}
            </h4>

            <!--<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>-->
          </div>
          <!--<div class="col-md-3">
            
            <span class="fa-stack fa-4x">
              <!--<i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
            -->
          <!--<a href="{{url('new-poster-case')}}">
            <img src="{{asset('img/posterdesign.png')}}" width="100%">
           </a>
            </span>
            <h4 class="service-heading">{{trans('app.poster_design')}}</h4>
            <!--<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>-->
         <!-- </div>-->
        </div>
      </div>
    </section>
<!--<section>
  <div class="container">
    <div class="row">

      <div class="col-md-6 myskilss">
        <h4 style="text-align:center;">{{trans('app.my_Skills')}}</h4>
    <img src="{{asset('img/sketch.png')}}" style="width:6%;float:left;margin-right:2%;" >
    <div class="progress" style="height: 20px; margin-bottom:10%">

     <div class="progress-bar bg-info" role="progressbar" style="width: 100%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
       SKETCH 100%
     </div>
    </div>
    <img src="{{asset('img/adobe1.png')}}" style="width:6%;float:left;margin-right:2%;" >
    <div class="progress" style="height: 20px;margin-bottom:10%">
     <div class="progress-bar bg-danger" role="progressbar" style="width:98%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
       ADOBE ALLUSTRATOR 98%
     </div>
    </div>
    <img src="{{asset('img/photoshop.png')}}" style="width:6%;float:left;margin-right:2%;" >
    <div class="progress" style="height: 20px;">
     <div class="progress-bar" role="progressbar" style="width: 95%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
       PHOTOSHOP 95%
     </div>
    </div>
      </div>
      <div class="col-md-6 mystudies">
        <h4 style="text-align:center;">{{trans('app.my_studies')}}</h4>
        <p>hello hello hello hello hello hello hello hello hello hello hello v hello hellohello hello hello hello hello hello hello hello hello hello hello hello hello hello hello  hello hello hello hello hello hello hello hello hello hello hello hello v hello hellohello hello hello hello hello hello hello hello hello hello hello hello hello hello hello  hellohello hello hello hello hello hello hello hello hello hello hello v hello hellohello hello hello hello hello hello hello hello hello hello hello hello hello hello hello  hello</p>
      </div>
      
    </div>
    
  </div>
</section>-->
    <!-- Portfolio Grid -->
    <section style="display:none;" class="bg-light" id="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.portofolio')}}</h2>
            <hr width="50%">
          </div>
        </div>
        
         @yield('portfolio')
      </div>
    </section>
    <div style="margin-bottom:4%; ">
      @include('shared.sort_bycategory')
    </div>
    
    @if(isset($favblogs) && $favblogs->count()>0)
    <section class="bg-light" id="portfolio" >
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.your_favorite_bog_post')}}</h2>
            
          </div>
        </div>
        
         @yield('favblog')
        
      </div>
      
    </section>
    @endif
    <section class="bg-light" id="portfolio" style="margin-top:-10%;">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.latest_bog_post')}}</h2>
            
          </div>
        </div>
        
         @yield('blog')
        
      </div>
      
    </section>
<section style="margin-top:-10%;" class="bg-light" id="portfolio">
      <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.team')}}</h2>
            
          </div>
        </div>
         @yield('team')
        <div style="text-align:center;">
        <a class="btn btn-primary" href="{{url('users')}}">{{trans('app.see_more')}}
        </a>
     </div>

</section>
<!-- my latest posts-->

    <!-- About -->
    @yield('pricing')
    <div id="blog">

  </div>
  

    

  <!--@include('shared.testimony')-->
    <!-- Clients -->
    <!--@include('layouts.partners')-->


    @include('layouts.footer')

    <!-- Portfolio Modals -->

    @yield('modals')

    @include('layouts.index_js')

    @include('shared.sort_by_categoryjs')

  </body>

</html>
