<!DOCTYPE html>
<html lang="en">
  @include('layouts.head2')

  <body>
  
    @include('layouts.nav2')

     <?php 
     $image= ($user->cover!=null)?asset('img/users/'.$user->cover):asset('img/blog/13.jpg');
    
     ?>
     @include('layouts.header')
    @include('shared.user-section')		  
	  
     @include('shared.followers2')
	
    @include('layouts.footer2')
    @include('layouts.js2')

  </body>
</html>