<head>

	
	    <!-- ==============================================
		Title and Meta Tags
		=============================================== -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		 @yield('meta')
		
		<!-- ==============================================
		Favicons
		=============================================== --> 
		<link rel="shortcut icon" href="{{asset('Medium/img/favicons/favicon.ico')}}">
		<link rel="apple-touch-icon" href="{{asset('Medium/img/favicons/apple-touch-icon.png')}}">
		<link rel="apple-touch-icon" sizes="72x72" href="{{asset('Medium/img/favicons/apple-touch-icon-72x72.png')}}">
		<link rel="apple-touch-icon" sizes="114x114" href="{{asset('Medium/img/favicons/apple-touch-icon-114x114.png')}}">
		
	    <!-- ==============================================
		CSS
		=============================================== -->
        <!-- Style-->
        
        
        
        <link type="text/css" href="{{asset('Medium/bower_components/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet" />
        
        <link type="text/css" href="{{asset('Medium/css/app.css')}}" rel="stylesheet" />
        <!-- dependency: React.js -->
		<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
		
		
				
		<!-- ==============================================
		Feauture Detection
		=============================================== -->
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="{{asset('Medium/js/modernizr-custom.js')}}"></script>

		

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<style type="text/css">
			.popover-content{
				display:none;
			}
			.note-toolbar a{
				color: black;
			}
			.loader {
  border: 1px solid #f3f3f3; /* Light grey */
  border-top: 1px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 15px;
  height: 15px;
  animation: spin 2s linear infinite;
  display:none;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

.skill :hover{
 display:block;
}
#editskill{
display:none;
}
		</style>	
		
  </head>