
<!DOCTYPE html>
<html lang="en">
  @include('layouts.head3')
  
  @include('shared.meta')
  
  <body>
  
    @include('layouts.nav2')


    <?php 
     $image= ($user->cover!=null)?asset('img/users/'.$user->cover):asset('img/blog/13.jpg');
    
     ?>
     @include('layouts.header')
	 
     	  @include('shared.user-section')
	  
	 <section class="posts-2">
	  <div class="container">
	   <div class="row">
	   
	    

         @include('shared.panel2')	
		
		
		
	    <div class="col-lg-6">
		
		 @include('shared.feed')	
		 
		</div>
		
		<div class="col-lg-3">
	     @include('shared.ad')

	     @include('shared.ratings')
		 </div>
		
		
	   </div>
	  </div>
    </section>		
	 
	 @include('shared.readmore')	 
	
    @include('layouts.footer2')
    @include('layouts.js2')

  </body>
</html>