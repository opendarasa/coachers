<!-- ==============================================
     Navigation Section
     =============================================== -->
	 <nav class="navbar navbar-light navbar-toggleable-sm bg-faded justify-content-between" id="mainNav-2">
	  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
	   <span class="navbar-toggler-icon"></span>
	  </button>
      <a href="{{url('home')}}" class="navbar-brand mr-0">BUDU</a>
	  <div class="navbar-collapse collapse justify-content-between" id="collapsingNavbar2">
	   <div><!--placeholder to evenly space flexbox items and center links--></div>
	   <ul class="navbar-nav">
		<!-- Search in right of nav -->
		<li class="nav-item hidden-xs-down">
		 <form class="top_search clearfix" action="{{url('blog-search')}}" method="get">
		 	{{csrf_field()}}
		  <div class="top_search_con">
		   <input class="s" placeholder="Search Here ..." type="text" name="keyword" id="searchbykeyword" onkeyup="autoCompleteSearch()">
		   <span class="top_search_icon"><i class="fa fa-search"></i></span>
		   <input class="top_search_submit" type="submit">
		  </div>
		 </form>
		</li>
		<!-- Search Ends -->  
	   </ul>
	   <ul class="nav navbar-nav">
	   	<li  class="nav-item">
			<a class="nav-link active" href="{{url('followers/'.Auth::user()->username)}}">
				<i class="fa fa-users">
					
				</i>
			</a>
		</li>
		<li  class="nav-item">
			<a class="nav-link active" href="{{route('blog.create')}}">
				<i class="fa fa-pencil">
					
				</i>
			</a>
		</li>
		<li style="display:none;" class="nav-item dropdown hidden-xs mega-menu">
		 <a href="javascript:;" data-toggle="dropdown" class="nav-link dropdown-toggle" aria-expanded="true">Mega </a>
		  <ul class="dropdown-menu dropdown-menu-yamm pull-right">
		   <li>
		   <!-- Content container to add padding -->
		   <div class="yamm-content">
			<div class="row">
			 <div class="col-sm-6 col-md-4 list-unstyled megamenu-list">
			  <h5 class="m-t-0">Description</h5>
			   <p class="text-muted font-13">
				 Medium Redesigned is a fully featured social network template built on top of awesome Bootstrap 4 Alpha 6, modern web technology HTML5, CSS3 and jQuery. It has many ready to use hand crafted components. The theme is fully responsive and easy to customize. The code is super easy to understand and gives power to any developer to turn this theme into real web application.
			   </p>
			 </div><!-- /col-md-3 -->
			 <ul class="col-sm-6 col-md-3 list-unstyled megamenu-list">
			  <li><h5 class="m-t-0">Pages</h5></li>
			  <li><a href="index.html">Home</a></li>
			  <li><a href="index-2.html">Home Page 2</a></li>
			  <li><a href="index-3.html">Home Page 3 </a></li>
			  <li><a href="index-4.html">Home Page 4 </a></li>
			  <li><a href="index-5-sidebar.html">Home Page 5 sidebar <span class="badge badge-danger">New</span></a></li>
			  <li><a href="newsfeed.html">News Feed <span class="badge badge-danger">New</span></a></li>
			 </ul><!-- /ul -->
             <ul class="col-sm-6 col-md-3 list-unstyled megamenu-list">
			  <li><h5 class="m-t-0">&nbsp;</h5></li>
			  <li><a href="profile.html">Profile</a></li>
			  <li><a href="profile-2.html">Profile Page 2</a></li>
			  <li><a href="followers.html">Followers/Following</li>
			  <li><a href="followers-2.html">Followers/Following 2</li>
			 </ul><!-- /ul -->
             <ul class="col-sm-6 col-md-2 list-unstyled megamenu-list">
			  <li><h5 class="m-t-0">&nbsp;</h5></li>
			  <li><a href="post.html">Post Page</li>
			  <li><a href="post-2.html">Post Page 2</li>
			 </ul><!-- /ul -->
            
			</div><!-- /row -->
		   </div><!-- /yam-content -->
		  </li><!-- /li -->
	     </ul><!-- /ul -->
        </li><!-- /li -->
			
		 <li class="nav-item dropdown mega-pills">
		  <a href="" class="nav-link" data-toggle="dropdown" aria-expanded="true">
		   <i class="fa fa-th"></i> 
		   <span class="label up p-a-0 warn"></span>
		  </a><!-- /nav-link -->
		  <div class="dropdown-menu w-xl pull-right p-a-0">
		   <div class="row no-gutter text-primary-hover">
		   	@foreach(trans('app.blog_types') as $type)
			<div class="col-xs-4 b-r b-b">
			 <a href="{{url('blogs/'.$type
			 )}}" class="p-a block text-center">
			  <i class="fa fa-files-o text-muted m-v-sm"></i> 
			  <span class="block">{{$type}}</span>
			 </a>

			</div><!-- /col-xs-4 -->
			@endforeach
			
		   </div>
		  </div>
		 </li><!-- /navbar-item -->		
		
         <li class="nav-item dropdown mega-notification">
		  <a href="" class="nav-link" data-toggle="dropdown" aria-expanded="true">
		   <i class="fa fa-bell-o"></i>			  
		   <span class="label up p-a-0 danger"></span>
		  </a>
		  <div class="dropdown-menu pull-right w-xl no-bg no-border no-shadow">
		   <div class="scrollable" style="height:400px;">
		    <ul class="list-group list-group-gap m-a-0">
		    	 
              @foreach(Auth::user()->unreadNotifications as $notification)
              
			 <li class="list-group-item dark-white box-shadow-z0 b">
			  <span class="pull-left m-r">
			   <img src="{{$notification->data['image']}}" alt="..." class="w-40 img-circle">
			  </span> 
			  <span class="clear block "><a onclick="readNotification('{{$notification->id}}')" href="{{$notification->data['url']}}" class="text-primary">{!!$notification->data['data']!!}</a><br>
			   <small class="text-muted">{{(new Carbon\Carbon($notification->created_at))->diffForHumans()}}</small>
			  </span>
			 </li><!-- /list-group-item -->
			 
			 @endforeach
			
			 
			   
			</ul><!-- /list-group -->
		   </div><!-- /scrollable -->
		  </div><!-- /dropdown-menu -->
		 </li><!-- /navbar-item -->
			
		 <li class="nav-item dropdown mega-avatar">
		  <a class="nav-link dropdown-toggle clear" data-toggle="dropdown" aria-expanded="true">
		   <span class="avatar w-32"><img src="{{asset('img/users/'.Auth::user()->image)}}" class="w-full rounded" width="25" height="25" alt="..."></span>
		   <i class="mini"></i>
		   <!-- hidden-xs hides the username on small devices so only the image appears. -->
		   <span class="hidden-xs">
			{{Auth::user()->name}}
		   </span>
		  </a>
		  <div class="dropdown-menu w dropdown-menu-scale pull-right">
		   <a class="dropdown-item" href="{{route('blog.create')}}"><span>{{__('app.new_article')}}</span></a> 
		   <a class="dropdown-item" href="#"><span>{{__('app.new_group')}}</span></a> 
		   <div class="dropdown-divider"></div>
		   <a class="dropdown-item" href="{{route('users.show',['username'=>Auth::user()->username])}}">
		   	<span>{{__('app.profile')}}</span></a> 
		   <a class="dropdown-item" href="#"><span>{{__('app.setting')}}</span></a> 
		   <a class="dropdown-item" href="#">{{__('app.help')}}</a> 
		   <div class="dropdown-divider"></div>
		   <a class="dropdown-item" 
		   onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();">
             {{__('app.signout')}}
             </a>
		   <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
		   @csrf
		  </form>
		  </div>
		 </li><!-- /navbar-item -->		
		 
	   </ul><!-- /navbar-nav -->
	  </div><!-- /navbar-collapse -->
	 </nav><!-- /nav --> 