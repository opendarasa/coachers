<!DOCTYPE html>
<html lang="en">

  @include('layouts.newcase_head')
  
  <body id="page-top">

   @include('layouts.nav')


    

    <!-- Services -->
    @yield('pricing')

    

    <section id="services">

      <div class="container">
        
        <div class="row">
    
      @yield('content')
    </div>
  </div>
    </section>

<!-- my latest posts-->

  

   @include('layouts.footer')

   
   @include('layouts.new_case_js')
   <script src="{{asset('js/literallycanvas.js')}}"></script>
    
  </body>

</html>
