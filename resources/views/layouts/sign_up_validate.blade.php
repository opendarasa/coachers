<script type="text/javascript">
  (function ($) {
    "use strict";

    /*==================================================================
    [ Focus Contact2 ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })


    /*==================================================================
    [ Validate after type ]*/
    $('.validate-input .input100').each(function(){
        
        $(this).on('blur', function(){
            if(validate(this) == false){
                //alert($(this).val());
                showValidate(this);
            }
            else {
                $(this).parent().addClass('true-validate');
            }
        })    
    })

    /*==================================================================
    [ Validate ]*/
    var input = $('#signup .validate-input .input100');

    $('#signup').on('submit',function(){
       // alert("enter1")

       //return true;
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }
        
    var password=$("#password").val();
    var passwordconfirm=$("#passwordconfirm").val();
    if ($("#ckb1").is(":checked")==false) {
      $("#agreealert").html("{{__('app.aggree_alert')}}");
      check=false;
    }else{
      $("#agreealert").html("");
      check=true;
    }

    if(password.length<6)
    {
      //alert();
      $("#password_alert").html("{{__('app.password_should_be_at_least_6')}}");
      $("#password").focus();
      check=false;
    }else{
      $("#password_alert").html("");
      //$("#password").focus();
      check=true;
    }
    if(passwordconfirm!=password)
    {
      $("#passwordconfirm_alert").html("{{__('app.pass_does_not_match')}}");
      $("#passwordconfirm").focus();
      check=false;
    }else{
       $("#passwordconfirm_alert").html("");
      //$("#passwordconfirm").focus();
      check=true;
    }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
           $(this).parent().removeClass('true-validate');
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else if ($(input).attr('name') == 'username') {
           if($(input).val().trim().match(/^[a-z0-9]+$/i) == null) {
               $("#username_alert").html("{{__('app.invalid_username')}}")
                return false; 
            }else{
                $("#username_alert").html("")
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    


})(jQuery);
  
</script>
