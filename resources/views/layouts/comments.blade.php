 <?php
 $divid=Session::get('divid');
  ?>
 <!-- ==============================================
	 Post Content
	 =============================================== -->
     <article class="post-comments">
      <div class="container">
       <div class="row">
        <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
		
		  <div class="comments-list-notification">
		   <h4><span class="text">{{count($comments)}} {{__('app.comments')}}</span></h4>
		  </div><!-- /.comment-list -->
		   <div class="comments-list">
			<!-- The contactform -->
			<form action="{{route('comment.store')}}" method="post" name="contactform" id="contactform">
				{{csrf_field()}}
			 <fieldset>
			   <!-- Comments / Message -->
			   <label for="comments" accesskey="C"><i class="fa fa-comment"></i></label>
			   <textarea name="comment" id="comment"></textarea>
			   <!-- Send button -->
			   <input type="hidden" name="blog_id" value="{{$blog->id}}">
			   <div class="text-center">
			   <button  type="submit" class="kafe-btn kafe-btn-mint full-width">{{__('app.comment')}}</button>
			   </div>
			 </fieldset>
			</form>	
		  </div><!-- /.comment-list -->	
		@foreach($comments as $comment)
		  <div class="comments-list" id="mycomment_{{$comment->id}}">
			
			 <div class="comment-body">
			  <div class="author-avatar">
			   <img alt="..." src="{{asset('img/users/'.$comment->image)}}" class="img-fluid" width="100" height="100">        
			  </div>
			  <div class="comment-content">
				<span class="author-name">{{$comment->name}}</span>                
				<span class="author-time">{{(new Carbon\Carbon($comment->created_at))->diffForHumans()}}</span>
				<p class="author-post">{{$comment->comment}}</p>
				<a rel="nofollow" class="comment-reply-link"  aria-label="Reply to Bablofil" id="" onclick="showReplyForm('{{$comment->id}}')">Reply</a>
				<a style="display:none;display:" rel="nofollow" class="comment-reply-link"  aria-label="Reply to Bablofil" id="cancel_{{$comment->id}}" onclick="cancelReplyForm('{{$comment->id}}')">cancel</a>
					   <!--- reply form-->
			  <div id="replyform_{{$comment->id}}" class="comments-list" style="display:none;margin-top:4%;">
			<!-- The contactform -->
			<form  action="{{url('comment-reply')}}" method="post" name="contactform" id="contactform_{{$comment->id}}">
				{{csrf_field()}}
			 <fieldset>
			   <!-- Comments / Message -->
			   
			   <textarea class="form-control" name="comment" id="comment" placeholder="{{__('app.leave_a_reply')}}"></textarea>
			   <!-- Send button -->
			   <input type="hidden" name="comment_id" value="{{$comment->id}}">
			   <div class="text-center">
			   <button onclick="submitReply('{{$comment->id}}')" type="button" class="kafe-btn kafe-btn-mint full-width">{{__('app.reply')}}</button>
			   </div>
			 </fieldset>
			</form>	
		  </div><!-- /.reply form -->
				<?php

				$replies=json_decode("[".$comment->replies."]");
				$limit=1;
				 ?>
				 @if($replies!=null)

				  <hr>
			
				  <span>{{count($replies)}} {{__('app.replies')}}</span>
				  @if(count($replies)>6)
				  <span style="float:right;"><a  id="showbtn_{{$comment->id}}" value="show more"  onclick="showMore('{{$comment->id}}')">show more</a></span>
				  @endif
				  <input type="hidden" value="{{count($replies)}}" name="" id="total">
				  @foreach($replies as $reply)
				  <?php $limit++; ?>

				 <div id="reply_{{$limit}}_{{$comment->id}}" class="replies" style="margin-top:4%;display:{{($limit<6)?'block':'none'}}">
				  <div class="author-avatar" style="width:50px;height:50px;">
				   <img alt="..." src="{{asset('img/users/'.$reply->image)}}" class="img-fluid" width="50" height="50">        
				  </div>
					<div class="comment-content" >
					<span class="author-name">{{$reply->name}}</span>                
					<span class="author-time">{{(new Carbon\Carbon($reply->created_at))->diffForHumans()}}</span>
					<p class="author-post">{{$reply->comment}}</p>
					 
				       </div><!-- /.comment-content -->
				 </div>
				 <br>

				 @endforeach
				 @endif 
			  </div><!-- /.comment-content -->
			 </div><!-- .comment-body -->

				 
		  </div><!-- /.comment-list -->
		    
		
		  @endforeach
		
		  	
		
		   
		
        </div><!-- /col-lg-8 -->
       </div><!-- /row -->
      </div><!-- /container -->
     </article><!-- /article -->

     <script type="text/javascript">
     	window.onload=function(){
     		@if(isset($divid))
     			window.location="{{Request::url()}}#{{$divid}}"
     		@endif
     	}
     	
     	function showReplyForm(id) {
     		
     		$("#replyform_"+id).css({display:'block'});
     		$("#cancel_"+id).css({display:'inline-block'});

     	}

     	function cancelReplyForm(id) {
     		$("#replyform_"+id).css({display:'none'});
     		$("#cancel_"+id).css({display:'none'});
     	}
     	function submitReply(id){
     		$("#contactform_"+id).submit();
     	}

     	function showMore(id) {
     		var total=$("#total").val();
              //console.log(total);
              var option=$("#showbtn_"+id).html();
             if(option=='show more')
             {
             	for (var i = 1; i < total+1; i++) {
     			
     				$("#reply_"+i+"_"+id).css({display:'block'});
     			
     		    }
     		    $("#showbtn_"+id).html('show less');
             }else{
             	for (var i =6; i < total+1; i++) {
     			
     				$("#reply_"+i+"_"+id).css({display:'none'});
     			
     		  }
     		  $("#showbtn_"+id).html('show more');
             }
     		
     	}
     </script>