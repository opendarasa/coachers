<?php
  $language='';
  switch (App::getLocale()) {
    case 'en':
      $language='en_US';
      break;
    
    default:
      $language='fr_FR';

      break;
  }
?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/{{$language}}/sdk.js#xfbml=1&version=v3.2&appId=285895765279599&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

 <!-- Navigation -->
    <nav style="background: white;" class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{url('home')}}">
          <img src="{{asset('img/500design.png')}}" width="8%">
          <span style="font-family:Tahoma; color:#AED9D6;">AfricanFreelancers</span>
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          {{trans('app.menu')}}
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a style="color:black;" class="nav-link js-scroll-trigger" href="{{url('home')}}">{{trans('app.home')}}</a>
            </li>
            <li style="display:none;" class="nav-item dropdown">
 <a style="color:black;" id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
  {{ trans('app.my_services') }} 
  <span class="caret">
  </span>
  </a>

<div style="position:auto;"  class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
  <a style="color:black;" class="dropdown-item" href="{{url('new-web-case')}}">{{trans('app.web_dev')}}
  </a>
  <a style="color:black;" class="dropdown-item" href="{{url('new-ui-case')}}">{{trans('app.mobile_dev')}}
  </a>
  <a style="color:black;" class="dropdown-item" href="{{url('new-seo-case')}}">{{trans('app.seo')}}
  </a>
  
  <a style="color:black;" class="dropdown-item" href="{{url('services')}}">{{trans('app.see_all')}}
  </a>
  
  
  </div>
  </li>
            <li class="nav-item">
              <a style="color:black;display:none;" class="nav-link js-scroll-trigger" href="{{url('portofolio')}}">{{trans('app.my_work')}}</a>
            </li>
            <li class="nav-item">
              <a style="color:black;" class="nav-link js-scroll-trigger" href="{{url('users')}}">{{trans('app.team')}}</a>
            </li>
            <li class="nav-item">
              <a style="color:black;" class="nav-link js-scroll-trigger" href="{{url('blog')}}">{{trans('app.blog')}}</a>
            </li>
            <!--<li class="nav-item">
              <a style="color:black;" class="nav-link js-scroll-trigger" href="{{url('services')}}">{{trans('app.pricing')}}</a>
            </li>-->
            @if(Auth::check())
            
 <li class="nav-item dropdown">
 <a style="color:black;" id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
  {{ Auth::user()->name }} 
  <span class="caret">
  </span>
  </a>

<div style="position:auto;"  class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
  <a style="color:black;" class="dropdown-item" href="{{route('blog.create')}}">{{trans('app.new_blog')}}
  </a>
  <a style="color:black;display:none;" class="dropdown-item" href="{{route('portofolio.create')}}">{{trans('app.new_work')}}
  </a>
  <a style="color:black;" class="dropdown-item" href="{{route('users.show',Auth::user()->username)}}">{{trans('app.profile')}}
  </a>
  @if(Auth::user()->admin==1)
  <a style="color:black;" class="dropdown-item" href="{{route('users.create')}}">{{trans('app.new_user')}}
  </a>
  @endif
  <a style="color:black;" class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
   @csrf
  </form>
  </div>
  </li>
  @else
      <li class="nav-item">
      <a style="color:black;" class="nav-link js-scroll-trigger" href="{{url('login')}}">{{trans('app.login')}}</a>
    </li>    
  @endif
  @if(Auth::check())
  <li class="nav-item dropdown">
   <a href="" class="notif" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="num">{{Auth::user()->unreadNotifications->count()}}</span></a>
   <div style="margin-right:4%;margin-top:-1%;" class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
    <?php $limit=0;?>
    @foreach(Auth::user()->unreadNotifications as $notification)
      @if($limit<=6)
    <a style="font-size:11px;" id="{{$notification->id}}" class="dropdown-item" href="{{$notification->data['url']}}" onclick="readNotification('{{$notification->id}}')">{{$notification->data['data']}}</a>
    <hr>
    <?php $limit++; ?>
      @endif
    @endforeach
    <a style="font-size:11px;" href="{{url('all-notification')}}" class="dropdown-item">
      {{__('app.see_all')}}
    </a>
  </div>
</li>
@endif
  <li class="nav-item" style="margin-left:2%;margin-top: 2%;margin-bottom:2%;">
    <a href="{{route('blog.create')}}" class="badge badge-dark">
       <i class="fa fa-plus">
         
       </i>
       {{__('app.add_article')}}
     </a>

  </li>
  
</ul>

      



<!--<div class="dropdown">
  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">{{App::getLocale()}}
  <span class="caret"></span></button>
  <ul class="dropdown-menu" style="text-align:left;width:50%; float:left;margin-left:10%;">
    @if(App::getLocale()=='fr')
    <li><a  style="color:black;" href="{{url('language/en')}}">En</a></li>
    @else
    <li><a style="color:black;" href="{{url('language/fr')}}">Fr</a></li>
    @endif
    
    
  </ul>
</div>-->


        </div>
      </div>
    </nav>
