<!DOCTYPE html>
<html lang="en">
  @include('layouts.head2')
  
  <body>
  
    @include('layouts.nav2')


    
	  
	 <section class="posts-2">
	 @yield('content')
    </section>		
	 
	 	 
	
    @include('layouts.footer2')
    @include('layouts.js2')
 	<script type="text/javascript">
 		$(document).ready(function() {


 			
        

 	$('#editor').summernote({
      height: 800,                 // set editor height
      minHeight: null,             // set minimum height of editor
      maxHeight: null,             // set maximum height of editor
      focus: true ,
                    // set focus to editable area after initializing summernote
    });

 $("#bio").summernote({
            height:400,
            focus:true,
  });

 		$("#myfile").fileinput({
        showPreview:true,
        showUpload: false,
        elErrorContainer: '#kartik-file-errors',
        allowedFileExtensions: ["jpg", "png", "gif"],
        @if(isset($blog->image))
         <?php 
         $path=public_path('img/blog');
          $image=$path.'/'.$blog->image;

          

         
         ?>
         initialPreview: [
        "<img src='{{asset('/img/blog/'.$blog->image)}}' class='file-preview-image' title='Desert' width='120' >"
       ],
       initialPreviewAsData: false, // identify if you are sending preview data only and not the raw markup
       initialPreviewFileType: 'image',
        initialPreviewConfig: [
        {caption: "{{$blog->image}}", size:{{filesize($image)}}, width: "120px", url: "{{$image}}"},
         ],
        
        @elseif(isset($work->image))
         <?php 
         $path=public_path('img/portfolio');
          $image=$path.'/'.$work->image;
         
         ?>
         initialPreview: [
        "<img src='{{asset('/img/portfolio/'.$work->image)}}' class='file-preview-image' title='Desert' width='120' >"
       ],
       initialPreviewAsData: false, // identify if you are sending preview data only and not the raw markup
       initialPreviewFileType: 'image',
        initialPreviewConfig: [
        {caption: "{{$work->image}}", size:{{filesize($image)}}, width: "120px", url: "{{$image}}"},
         ],
        @endif
    });

 	 $("#image").fileinput({
        showPreview:true,
        showUpload: false,
        elErrorContainer: '#kartik-file-errors',
        allowedFileExtensions: ["jpg", "png", "gif"],
        @if(isset($user->image))
         <?php 
         $path=public_path('img/users');
          $image=$path.'/'.$user->image;

          

         
         ?>
         initialPreview: [
        "<img src='{{asset('/img/users/'.$user->image)}}' class='file-preview-image' title='Desert' width='120' >"
       ],
       initialPreviewAsData: false, // identify if you are sending preview data only and not the raw markup
       initialPreviewFileType: 'image',
        initialPreviewConfig: [
        {caption: "{{$user->image}}", size:{{filesize($image)}}, width: "120px", url: "{{$image}}"},
         ],
        
        
    
        @endif
    });

        $("#cover").fileinput({
        showPreview:true,
        showUpload: false,
        elErrorContainer: '#kartik-file-errors',
        allowedFileExtensions: ["jpg", "png", "gif"],
        @if(isset($user->cover))
         <?php 
         $path=public_path('img/users');
          $image=$path.'/'.$user->cover;

          

         
         ?>
         initialPreview: [
        "<img src='{{asset('/img/users/'.$user->cover)}}' class='file-preview-image' title='Desert' width='120' >"
       ],
       initialPreviewAsData: false, // identify if you are sending preview data only and not the raw markup
       initialPreviewFileType: 'image',
        initialPreviewConfig: [
        {caption: "{{$user->cover}}", size:{{filesize($image)}}, width: "120px", url: "{{$image}}"},
         ],
        
        
    
        @endif
    });
 	})
 	</script>
  </body>
</html>