 <!-- ==============================================
	 Scripts
	 =============================================== -->
	

	<script src="{{asset('Medium/bower_components/tether/dist/js/tether.min.js')}}"></script>
	<script src="{{asset('Medium/bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
	<script src="{{asset('Medium/js/medium.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/piexif.min.js" type="text/javascript"></script>
<!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview. 
    This must be loaded before fileinput.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/sortable.min.js" type="text/javascript"></script>
<!-- purify.min.js is only needed if you wish to purify HTML content in your preview for 
    HTML files. This must be loaded before fileinput.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/plugins/purify.min.js" type="text/javascript"></script>
<!-- popper.min.js below is needed if you use bootstrap 4.x. You can also use the bootstrap js 
   3.3.x versions without popper.min.js. -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js"></script>
<!-- optionally uncomment line below for loading your theme assets for a theme like Font Awesome (`fa`) -->
<cript src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/themes/fa/theme.min.js"></script>
<!-- optionally if you need translation for your language then include  locale file as mentioned below -->


  
  
  

<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script src="{{asset('js/rater.js')}}" charset="utf-8"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<?php
    $searchlogs=App\SearchLog::where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get();

    $searchlogsArray=array();

    foreach ($searchlogs as $key => $value) {
      # code...
      $searchlogsArray[$key]=$value->keyword;

    }



   ?>
	<script type="text/javascript">


	document.getElementById("copybtn").addEventListener("click", function() {
    var copy=copyToClipboard(document.getElementById("copytarget"));
    //console.log(copy);
});


	
  function readNotification(id) {
    $.ajax({
             type: "get",
              url:"/readNotification",
              data:{id:id},
              success:function(data,status){
                 console.log(status);
              },
               error:function(data,status){
                   console.log(status);
            }
});
  }

function copyToClipboard(elem) {
	  // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
    	  succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}

function deleteFunction(id) {
	  swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to recover this post!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    
    swal("Poof! Your post has been deleted!", {
      icon: "success",
    });
    $("#deleteform_"+id).submit();

  } else {
    swal("Your post is  safe!");
  }
});
}

// ==============================autocomplete search===============

var availableTags=['{{implode(',',$searchlogsArray)}}'];

var fakedata = ['test1','test2','test3','test4','ietsanders'];

function autoCompleteSearch()
{
  //alert(availableTags);
  $("#searchbykeyword").autocomplete({
      source: fakedata
    });

}

 
      

       var rating=0;
    @if(isset($user))
     $(".rate").rate();
        $(".torate").rate();
                $(".torate").on("change", function(ev, data){
                
                console.log(data.from, data.to);

                rating=data.to;
            });
      $("#saverating").click(function(){
                if(rating==0)
                {

                  $("#ratinghelp").html("{{__('app.rating_cannot_be_empty')}}");

                  return false;

                }else{
                var _token="{!!csrf_token()!!}";
                var user_id="{{$user->id}}";
                var comment=$("#ratingcomment").val();
                $.ajax({
                    url:"{{url('add-rating')}}",
                    type:'POST',
                    data:{_token:_token,user_id:user_id,rating:rating,comment:comment},
                    success:function(data2,status){
                        var result=JSON.parse(data2);
                        if(result.status=='success'){
                          swal({
                                title: "{{__('app.rating_success')}}",
                                text:result.message,
                                icon: "success",
                                button: "ok",
                              }).then((value) => {
                                    location.reload();
                                  });
                        }else{
                          console.log(result.message);
                          swal({
                                title: "{{__('app.rating_error')}}",
                                text:result.message,
                                icon: "error",
                                button: "ok",
                              }).then((value) => {
                                    location.reload();
                                  });

                        }
                        //location.reload();
                    },
                    error:function(data2,status){
                        console.log(data2)
                    }

                    });
                }
                
      })
    @endif  
       



function sendFollow(id) {
        $("#follow_"+id).hide();
        $("#unfollow_"+id).show();
        $.ajax({
            type:"GET",
            url:"/readerajaxfollow",
            data:{id:id},
            success:function(data,status){
                //location.reload();
            },
            beforeSend:function(){
                $("#loader_"+id).show();
            },
            complete:function(){
                $("#loader_"+id).hide();
            },
            error:function(data,status){
                 console.log(data);
            }
        })
        // body...
    }

    function sendUnfollow(id) {
        $("#unfollow_"+id).hide();
        $("#follow_"+id).show();
        $.ajax({
            type:"GET",
            url:"/readerajaxunfollow",
            data:{id:id},
            success:function(data,status){
                //location.reload();
            },
             beforeSend:function(){
                $("#loader_"+id).show();
            },
            complete:function(){
                $("#loader_"+id).hide();
            },
            error:function(data,status){
                console.log(data);
            }
        })
        // body...
    }

     function clap(id)
     {
        $("#clap_"+id).hide();
        $("#unclap_"+id).show()
        var currentClap=$("#blogclaps_"+id).val();
        //console.log("before: "+parseInt(currentClap));
        $("#unclapI_"+id).html("     "+(parseInt(currentClap)+1));
        $("#blogclaps_"+id).attr("value",parseInt(currentClap)+1);
        currentClap=$("#blogclaps_"+id).val();
        //console.log("after: "+parseInt(currentClap));
        
        $.ajax({
            type:"GET",
            url:"/like",
            data:{id:id},
            success:function(data,status){
                //location.reload();
            },
             beforeSend:function(){
                $("#loader_"+id).show();
            },
            complete:function(){
                $("#loader_"+id).hide();
            },
            error:function(data,status){
                console.log(data);
            }
        })
     }

     function unclap(id){
      $("#unclap_"+id).hide();
        $("#clap_"+id).show()
         var currentClap=$("#blogclaps_"+id).val();
         //$("#clapI_"+id).html("");
          $("#clapI_"+id).html("     "+(currentClap-1));
          $("#blogclaps_"+id).attr("value",parseInt(currentClap)-1);
        $.ajax({
            type:"GET",
            url:"/unlike",
            data:{id:id},
            success:function(data,status){
                //location.reload();
            },
             beforeSend:function(){
                $("#loader_"+id).show();
            },
            complete:function(){
                $("#loader_"+id).hide();
            },
            error:function(data,status){
                console.log(data);
            }
        })
     }

     $("#submitskill").click(function(){
       var skill_name=$("#skill_name").val();
       var _token="{{csrf_token()}}";
       var level=$("#level").val();

       if(skill_name==''){
          $("#skill_name").focus();
        $("#skill_name_alert").html("{{__('app.skill_is_required')}}");
        return false;
       }else{
        $.ajax({
          type:"POST",
          url:"/save-skill",
          data:{skill:skill_name,level:level,_token:_token},
          success:function(data,status){
            console.log(status);
            window.location.reload();

          },
          error:function(data,status){
            console.log(data);
          },
          beforeSend:function(){
            $("#skillloader").show();
          },
          complete:function(){
            $("#skillloader").hide();
          }
        })
       }
     });

     $("#submiteditskill").click(function(){
       var skill_name=$("#edit_skill_name").val();
       var _token="{{csrf_token()}}";
       var level=$("#editlevel").val();
       var id=$("#skill_id").val();

       if(skill_name==''){
          $("#edit_skill_name").focus();
        $("#edit_skill_name_alert").html("{{__('app.skill_is_required')}}");
        return false;
       }else{
        $.ajax({
          type:"POST",
          url:"/edit-skill",
          data:{skill:skill_name,level:level,_token:_token,id:id},
          success:function(data,status){
            console.log(status);
            window.location.reload();

          },
          error:function(data,status){
            console.log(data);
          },
          beforeSend:function(){
            $("#skillloader2").show();
          },
          complete:function(){
            $("#skillloader2").hide();
          }
        })
       }
     });


     function editSkill(id) {
       $("#saveskill").hide();
       $("#editskill").show();
       var skill=$("#skill_"+id).val();
       var level=$("#level_"+id).val();


       $("#edit_skill_name").attr("value",skill);
       $("#editlevel").val(level);
       $("#skill_id").attr("value",id);
       $("#edit_skill_name").focus();
     }

      function deleteSkill(id) {

         swal({
  title: "Are you sure?",
  text: "{{__('app.once_deleted_you_will_not_see_your_skill_anymore')}}",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    
    swal("{{__('app.poof_your_skill_is_delted')}}", {
      icon: "success",
    });
    $.ajax({
      type:"GET",
      url:"/delete-skill",
      data:{id:id},
      success:function(data,status){
        console.log(status);
        window.location.reload();
      },
      error:function(data,status){
        console.log(data);
      }

    });

  } else {
    swal("{{__('app.your_post_is_safe')}}");
  }
});
        
      }


      function followUser(id) {

         $.ajax({
            type:"GET",
            url:"/readerajaxfollow",
            data:{id:id},
            success:function(data,status){
                location.reload();
            },
            beforeSend:function(){
               // $("#loader_"+id).show();
            },
            complete:function(){
               // $("#loader_"+id).hide();
            },
            error:function(data,status){
                 console.log(data);
            }
        })
        // body...
      }

       function unfollowUser(id) {
         // body...

         $.ajax({
            type:"GET",
            url:"/readerajaxunfollow",
            data:{id:id},
            success:function(data,status){
                //location.reload();

            },
             beforeSend:function(){
                //$("#loader_"+id).show();
            },
            complete:function(){
                //$("#loader_"+id).hide();
            },
            error:function(data,status){
                console.log(data);
            }
        })
       }

        function blockUser(id)
        {
          $.ajax({
            type:"GET",
            url:"/block-user",
            data:{user_id:id},
            success:function(data,status){
              $("#realblocka_"+id).css({display:"none"});
              $("#unblocka_"+id).show();
              $("#followinblockedicon_"+id).show()
                //location.reload();
            },
             beforeSend:function(){
                //$("#loader_"+id).show();
            },
            complete:function(){
                //$("#loader_"+id).hide();
            },
            error:function(data,status){
                console.log(data);
            }
        })

        }

        function unblockUser(id)
        {

          $.ajax({
            type:"GET",
            url:"/unblock-user",
            data:{user_id:id},
            success:function(data,status){
              $("#realunblocka_"+id).css({display:"none"});
              $("#blocka_"+id).show();
              $("#realblockicon_"+id).hide();
               $("#followinblockedicon_"+id).hide()
                //location.reload();
            },
             beforeSend:function(){
                //$("#loader_"+id).show();
            },
            complete:function(){
                //$("#loader_"+id).hide();
            },
            error:function(data,status){
                console.log(data);
            }
        })

        }

	</script>