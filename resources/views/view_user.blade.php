    @extends('layouts.newcase-template')
@section('meta')

    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta property="og:description" content="{!!$ogdescription!!}">
    <title>{{$ogtitle}}</title>
    <meta property="og:image" content="{{asset('img/users/'.$ogimage)}}">
    <meta property="og:url" content="{{Request::url()}}">
    <meta property="og:type" content="website">
    <meta property="author" content="">
    <meta property="keywords" content="{{trans('app.keywords')}}">


    @include('shared.payment_css')
    
@endsection
    @section('content')

    <div class="container">
    <div class="row my-2">
    <div class="col-lg-4 order-lg-1 text-center">
    <img  src="{{asset('img/users/'.$user->image)}}" class="profileimage mx-auto img-fluid img-circle d-block" alt="avatar">
    @if(Auth::check()&& Auth::user()->id==$user->id)
    <h6 class="mt-2">{{trans('app.upload_new_photo')}}</h6>
    <form method="post" action="{{url('change_pic')}}" enctype="multipart/form-data">

    {{csrf_field()}}
    <label for="file">

    <input data-show-upload="true" type="file" id="file" class="file" name="image">

    </label>
    </form>
    @else


    @endif
    
    <div>
        @if($user->facebook!="")
        <a style="margin-right:3%;" href="{{$user->facebook}}">
            <i class="fa fa-facebook">
               Facebook 
            </i>
        </a>
        @endif
        @if($user->twitter!="")
        <a href="{{$user->twitter}}">
            <i class="fa fa-facebook">
               Twitter
            </i>
        </a>
        @endif
        
        
    </div>
    <div class="col-sm-12" style="text-align:center;">
            @if(Auth::check())

          <?php 
                     $checkIfFollow=App\Followers::where('follower_id',Auth::user()->id)->where('user_id',$user->id)->count();
                     $checkIfFollowing=App\Followers::where('user_id',Auth::user()->id)->where('follower_id',$user->id)->count();
                    ?>
                      
                      @if($user->id!=Auth::user()->id)
                    <div class="col-sm-12 ">
                          @if($checkIfFollow==0)
                     
                         
                     
                    <a  href="{{url('follow/'.$user->id)}}" class="badge badge-dark " style="width:50%;">
                        <i class="fa fa-plus">
                            
                        </i>
                        {{__('app.follow')}}
                     </a>
                          @else
                        <a style=""class="badge badge-success ">
                        
                        {{__('app.following')}}
                       </a>

                          @endif
                        </div>
                       @endif

                       @if($checkIfFollowing && Auth::user()->id!=$user->id)
                       <div class="col-sm-12">
                        <small class=""  style="color:green;">
                           {{__('app.is_following_you')}}
                       </small>
                       </div>
                       @endif
                       <div>
                        
                         
                       </div>

        @else
       <a href="{{url('follow/'.$user->id)}}" class="badge badge-dark">
           <i class="fa fa-plus">
               
           </i>
           {{__('app.follow')}}
       </a>
      
       @endif

    <div>
        <span style="display:inline-block;">
            {{number_format($user->rating,1)}}
        </span>
        <div class="rate" data-rate-value="{{$user->rating}}" style="font-size:14px;display:inline-block;">
        
         </div>
    </div>
    @if(Auth::check() && Auth::user()->id!=$user->id)
    <div>
        <a href="#rating" data-toggle="modal" class="badge badge-dark">
           {{__('app.rate_writer')}}
       </a>
    </div>
     @endif 
            
        </div>
    </div>
    <div class="col-lg-8 order-lg-2">
    <ul class="nav nav-tabs">
    <li class="nav-item">
    <a id="aprofile" href="" data-target="#profile" data-toggle="tab" class="nav-link active">{{trans('app.profile')}}</a>
    @if(Session::has('username'))
    {{session()->get('username')}}
    @endif
    </li>
    @if(Auth::check())
    @if(Auth::user()->id==$user->id)
    <li class="nav-item">
    <a id="aedit" href="" data-target="#edit" data-toggle="tab" class="nav-link">
        {{trans('app.edit')}}
    </a>
    </li>
    @endif
    @endif
    <li class="nav-item">
    <a id="afollowers" href="" data-target="#followers" data-toggle="tab" class="nav-link">
        <i class="fa fa-user">
           
        </i>
         <span class="badge badge-warning">
                {{$followers->count()}}
        </span>
    {{trans('app.followers')}}

    </a>
    </li>
    <li class="nav-item">
    <a id="afollowings" href="" data-target="#following" data-toggle="tab" class="nav-link">
        <i class="fa fa-user">
           
        </i>
         <span class="badge badge-warning">
                {{$followings->count()}}
        </span>
    {{trans('app.following')}}
   </a>
    </li>
    @if(Auth::check() && Auth::user()->id==$user->id)
    <li class="nav-item">
    <a id="anotifications" href="" data-target="#notifications" data-toggle="tab" class="nav-link">
        <i class="fa fa-bell">
           
        </i>
         
    {{trans('app.notifications')}}
   </a>
    </li>
    @endif
    </ul>
    <div class="tab-content py-4">
    <div class="tab-pane active" id="profile">
        <div class="" style="display:inline-block;">
                
    </div>
    <h5 class="mb-3">
        {{$user->name}}
        <small style="display:inline-block;font-weight:bold;" class="text-muted">
                    @if($user->badge=='G')
                    
                    <img style="width:50%;" src="{{asset('img/icons/medal_gold.png')}}">
                    <i class="fa fa-info" class="tooltip"  data-toggle="tooltip" data-placement="top" title="{{__('app.golden')}}">
                        
                    </i>
                    @elseif($user->badge=='S')
                    
                    <img style="width:50%;" src="{{asset('img/icons/medal_silver.png')}}">
                    <i class="fa fa-info"  data-toggle="tooltip" data-placement="top" title="{{__('app.silver')}}">
                        
                    </i>
                    @elseif($user->badge=='B')
                    
                    <img style="width:50%;" src="{{asset('img/icons/medal_bronze.png')}}">
                    <i class="fa fa-info"  data-toggle="tooltip" data-placement="top" title="{{__('app.bronze')}}">
                        
                    </i>
                    @else
                    
                    @endif
         </small>
        
    </h5>
     
    <span class="text-muted" style="margin-bottom:4%;"> {{$user->company}}</span>
    <div class="row">
    <div class="col-md-6">
    <h6>{{trans('app.about')}}</h6>
    <p>
    {{$user->description}}
    </p>
    <p>
    <i class="fa fa-map-marker"></i>
    {{$user->location}}
    </p>
    <p>
    <i class="fa fa-clock-o"></i>
    {{trans('app.joined').'  '.(new Carbon\Carbon($user->created_at))->diffForHumans()}}
    </p>
    <p>
        @if(Auth::check()&&Auth::user()->id==$user->id)
        <a href="{{route('blog.create')}}" class="btn btn-warning">
            <i class="fa fa-plus">
                {{trans('app.add_new_blog')}}
            </i>
        </a>
        <a style="display:none;" href="{{route('portofolio.create')}}" class="btn btn-info">
            <i class="fa fa-plus">
                {{trans('app.add_new_work')}}
            </i>
        </a>
        <!--<a href="#user_pricing_modal" data-toggle="modal"  class="btn btn-info">
            <i class="fa fa-plus">
                {{trans('app.edit_pricing')}}
            </i>
        </a>-->
        @endif
    </p>
    </div>
    <div class="col-md-6">
        @if(Auth::check()&& (Auth::user()->id==$user->id||Auth::user()->admin==1))
        <p>
<form id="changeprofiletype" class="form-inline" method="post" action="{{url('profile-type')}}">
      {{csrf_field()}}
    <div class=" form-group checkbox">
        <input onclick="mychange();" id="checkprofile" class="form-control" name="check" type="checkbox"  data-toggle="toggle" {{($user->hidden==0)?'checked':''}} >
        
        
   </div>
   <div class="form-group">
       <input type="hidden" name="user_id" value="{{$user->id}}">
   </div>
   <div style="margin-left:7%;" class=" form-group ">
         <label>

             @if($user->hidden==0)
             {{trans('app.public')}}
             @else
             {{trans('app.private')}}
             @endif
         </label>
         
              
       
    </div>
</form>
        </p>
    @endif
    <?php $skills=explode(",",$user->skills); ?>

    <h6>
    {{trans('app.recent_badges')}}
    </h6>
    @foreach($skills as $skill)
    <a href="#" class="badge badge-dark badge-pill">{{$skill}}</a>
    @endforeach
    @if(Auth::check()&&Auth::user()->admin==1)
     <form class="form-inline" method="post" action="{{url('add_skills')}}">
        {{csrf_field()}}
         <div class="form-group">
            <input placeholder="{{trans('app.add_skills')}}" type="text" name="skills" class="form-control" required>
            <input type="hidden" name="user_id" value="{{$user->id}}">

         </div>
         <div class="form-group">
             <button type="submit" class="btn btn-warning">
                 {{trans('app.save')}}
             </button>
         </div>
     </form>
    @endif
    <hr>
    @if(!empty($repos))
    <h6>{{trans('app.git_repos')}}</h6>
    @foreach($repos as $repo)
    <span class="badge badge-primary">
    <a href="{{$repo->repos_url}}">

    <i class="fa fa-github"></i>
    {{$repo->repos_name}}
    </a>


    </span>
    @endforeach
    @else

    {{trans('app.no_repos_yet')}}
    }
    @endif
    <hr>
    <span class="badge badge-success"><i class="fa fa-cog"></i> {{$user->experience}} {{trans('app.years_experience')}}</span>
    <span class="badge badge-danger"><i class="fa fa-eye"></i>
    <?php 
    $user_view=App\User::find($user->id);
    $user_view->views+=1;
    $user_view->save();
    //Auth::user()->save();
    ?>
    {{$user_view->views}}</span>
    </div>

    </div>
    <!--/row-->
    </div>

    <div class="tab-pane" id="followers">
       @include('shared.followers') 
    </div>

    <div class="tab-pane" id="following">
       @include('shared.followings') 
    </div>
    @if(Auth::check() && Auth::user()->id==$user->id)
    <div class="tab-pane" id="notifications">
       @include('shared.notifications') 
    </div>
    @endif
    <div class="tab-pane" id="edit">
    @if($errors->any())  
    <div class="alert alert-danger">
    {!!$errors->first()!!}
    </div>

    @endif
    {!! Form::model($user, [
    'method' => 'PATCH',
    'route' => ['users.update', $user->username]
    ]) !!}

    {{csrf_field()}}

    {{Form::hidden('password',$user->password)}}
    <div class="form-group row">
    {{Form::label('name',trans('app.full_name'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::text('name',null,['class'=>'form-control'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('username',trans('app.username'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::text('username',$user->username,['class'=>'form-control','disabled'=>'false'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('email',trans('app.email'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::email('email',null,['class'=>'form-control','disabled'=>'false'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('description',trans('app.description_user'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::textarea('description',null,['class'=>'form-control','placeholder'=>trans('app.short_description'),'id'=>'bio','maxlength'=>'500','minlength'=>'100'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('company',trans('app.company'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::text('company',null,['class'=>'form-control'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('website',trans('app.website'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::url('website',config('app.url').'/users/'.$user->username,['class'=>'form-control'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('facebook','Facebook',['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::url('facebook',null,['class'=>'form-control','id'=>'facebook'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('twitter','Twitter',['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::url('twitter',null,['class'=>'form-control','id'=>'twitter'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label(trans('app.location'),'location',['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::text('location',null,['class'=>'form-control'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label(trans('app.experience'),'experience',['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::number('experience',null,['class'=>'form-control'])}}
    </div>
    </div>
    @if(isset($user->skills)&& $user->skills!="")
    <div class="form-group row">
    {{Form::label('skills',trans('app.skills'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    @foreach($skills as $skill)
    <a href="#" class="badge badge-dark badge-pill">
    {{$skill}}
    </a>
    @endforeach
    </div>
    </div>
    @else
    <div class="form-group row">
    <label class="col-lg-3 col-form-label form-control-label""></label>
    <div class="col-lg-9">
    <p>
    {{trans('app.you_have_no_earned_skills_yet')}}
    </p>

    </div>

    </div>
    @endif
    @if(count($repos)>0)
    <div class="form-group row">
    {{Form::label('repos',trans('app.repos'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    @foreach($repos as $repo)
    <span class="badge badge-primary">
    <a href="{{$repo->repos_url}}">

    <i class="fa fa-github"></i>
    {{$repo->repos_name}}
    </a>


    </span>
    @endforeach
    </div>
    </div>
    @else
    <div class="form-group row">
    <label class="col-lg-3 col-form-label form-control-label""></label>
    <div class="col-lg-9">
    <a style="width:100%;" href="{{url('/oauth/github')}}" class="btn btn-info pull-left" role="button">
    <i class="fa fa-github">
    {{trans('app.import_repos')}}   
    </i>

    </a>

    </div>

    </div>
    @endif
    <div class="form-group row">
    <label class="col-lg-3 col-form-label form-control-label">

    </label>
    <div class="col-lg-9">
    <input type="reset" class="btn btn-secondary" value="{{trans('app.cancel')}}">
    <input type="submit" class="btn btn-primary" value="{{trans('app.save_changes')}}">
    </div>
    </div>
    {{Form::close()}}



    </div>
    </div>
    @if(Auth::guest()||(Auth::check()&&Auth::user()->id!=$user->id))
    <div style="margin-top:3%;">
    <a id="hire" href="#pricing" data-toggle="modal" class="btn btn-secondary" style="width:100%;">
    {{trans('app.contact_me')}}
    </a>
    </div>
    @endif
    </div>

    </div>
    </div>

   <!-- user pricing plan-->
   

    <!--Portfolio-->
    <section class="bg-light" id="portfolio">
      <div class="container">
        <div class="row">
          @if(count($works)>0)
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.portofolio')}}</h2>
            <hr width="50%">
          </div>
          @endif
        </div>
        
          @include('shared.grid')
          
      
        
      </div>
    </section>
    <!-- Portfolio Modals -->

    
    @include('shared.modal')

    <!--Blog Posts-->
     <section class="bg-light" id="portfolio">
      <div class="container">
        <div class="row">
          @if(count($blogs)>0)
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.my_latest_posts')}}</h2>
            <hr width="50%">
          </div>
          @endif
        </div>
        <div class="row">
          @include('shared.blog_post')
          
        </div>
      </div>
      
    </section>
    
    <div class="portfolio-modal modal fade" id="pricing" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="close-modal" data-dismiss="modal">
    <div class="lr">
    <div class="rl"></div>
    </div>
    </div>
    <div class="container">
    <div class="row">
    <div class="col-lg-12 mx-auto">
    <div class="modal-body">
        <div class=" steps col-sm-12">
            <ul>
                <li id="step_0" class="currentbox">
                    <a href="">
                        <i class="fa fa-list"></i>
                        {{__('app.article')}}
                    </a>
                    
                </li>
                <li id="step_1" class="box">
                    <a href="">
                        <i class="fa fa-phone"></i>
                        {{__('app.contact')}}
                    </a>
                </li>
                <li id="step_2" class="box">
                    <a href="">
                        <i class="fa fa-eye"></i>
                        {{__('app.confirm')}}
                    </a>
                </li>
                <li id="step_3" class="box">
                    <a href="">
                        <i class="fa fa-euro"></i>
                        {{__('app.payment')}}
                    </a>
                </li>
                
            </ul>
        </div>
       <form class="col-sm-12" id="regForm" action="{{route('payment.process')}}" method="post">
  {{csrf_field()}}
  <h2 style="background-color:#f4b942;text-align:center;font-size:3vw;">{{trans('app.submit_project')}}</h2>
  <!-- One "tab" for each step in the form: -->
  <div class="tab" style="margin-bottom:2%;" >{{trans('app.tell_me_about_your_article')}}
<p><input placeholder="{{trans('app.what_do_you_want_to_know')}}" oninput="this.className = ''" name="topic" id="topic"></p>
    <p><select class="form-control"   name="topicCategory" id="category">
        <option value="">{{__('app.select_type')}}</option>
        @foreach(trans('app.blog_types') as $key=>$value)
        <option value="{{$key}}">{{$value}}</option>
        @endforeach
    </select>
    </p>
    <p>
        <textarea  name="articleContent" id="content" class="form-control" style="height:400px;" placeholder="{{__('app.tell_me_more_about_it')}}"></textarea>
    </p>
    
    
  </div>
  <div class="tab">{{trans('app.tell_me_about_yourself')}}:
    <p><input placeholder="{{trans('app.your_name')}}" oninput="this.className = ''" name="name" id="name"></p>
    <p><input placeholder="{{trans('app.your_email')}}" oninput="this.className = ''" name="email" id="email"></p>
    <p><input type="number" placeholder="{{trans('app.your_phone_number')}}" oninput="this.className = ''" name="phone" id="phone"></p>
  </div>
  <div class="tab"  style="width:100%;margin-bottom:2%;">{{trans('app.confirm_information')}}:

    <p>
        <div class="row">
            <div class="col-sm-6">
                <p>
                    <span id="ctopic"></span>
                </p>
                <p>
                    <span id="ccategory"></span>
                </p>
                <p style="text-align:left;width:400px;">
                    <p id="ccontent"></p>
                </p>
                
            </div>
            <div class="col-sm-6">
                <p>
                   <span id="cname"></span> 
                </p>
                <p>
                    <span id="cemail"></span>
                </p>
                
                <p>
                    <span id="cphone"></span>
                </p>
                
            </div>
            
        </div>
        
    </p>
   
     
    
  </div>
  
  
  <div class="tab" style="text-align:center;">{{trans('app.resume')}}:
    <div id="dropin-container"></div>
  </div>
  <div style="overflow:auto;">
    <div style="float:right;">
      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
    </div>
  </div>
  <!-- Circles which indicates the steps of the form: -->
  <div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    
  </div>
</form>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>

    <!-- Modal -->
<div class="modal fade" id="rating" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{__('app.rate_user')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align:center;">
        <div class="torate" data-rate-value="0" style="font-size:40px;display:inline-block;"></div>
        <small id="ratinghelp" style="color:red"></small>
        <textarea id="ratingcomment" style="height:150px;width:80%;" id="comment" placeholder="{{__('app.add_a_comment')}}" maxlength="500"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('app.close')}}</button>
        <button type="button" class="btn btn-primary" id="saverating">{{__('app.save')}}</button>
      </div>
    </div>
  </div>
</div>

    
    <script type="text/javascript">

    window.onload=function(){


    @if(Session::has('profile-tab'))

    $("#aprofile").removeClass('active');
    $("#anotifications").click();

    <?php
      Session::forget('profile-tab');
     ?>

    @endif
  }

    function mychange() {
        $("#changeprofiletype").submit();
    }
    $("#hire_mobile").click(function(){
    console.log('enter');
    $.ajax({
    url:"/savecookie",
    type:"GET",
    data:{username:{{$user->username}}},
    success:function(data,status){
    console.log(status);
    },
    error:function(data,status){
    console.log(status);
    }

    });
    });
    </script>

    
    @endsection