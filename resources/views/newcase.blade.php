@extends('layouts.newcase-template')
@section('meta')
@include('shared.meta')
@endsection
@section('content')
@if($errors->any())
<div class=" col-md-6 col-xs-offset-12 alert alert-danger">
  {{$errors->first()}}
</div>
@endif
@if(isset($message))
<div class=" col-md-6 col-xs-offset-12 alert alert-danger">
  {{$message}}
</div>
@endif
<form id="regForm" action="{{url('newcase')}}" method="post">
  {{csrf_field()}}
  <h2 style="background-color:#f4b942;text-align:center;">{{trans('app.submit_project')}}</h2>
  <!-- One "tab" for each step in the form: -->
  <div class="tab" style="margin-bottom:2%;" id="drawing">{{trans('app.tell_me_about_your_style')}}

  	<div class="my-drawing">
  		
  	</div>
    
  </div>
  <div class="tab">{{trans('app.tell_me_about_your_company')}}:
    <p><input placeholder="{{trans('app.company_name')}}" oninput="this.className = ''" name="companyname" id="companyname"></p>
    <p><input placeholder="{{trans('app.what_does_your_company_do')}}" oninput="this.className = ''" name="industry" id="companyIndustry"></p>
  </div>
  <div class="tab"  style="width:100%;margin-bottom:2%;">{{trans('app.how_do_i_contact_u')}}:

  	<p><input type="text" placeholder="{{trans('app.your_name')}}" oninput="this.className = ''" name="client_name" id="clientName"></p>
    <p><input type="email" placeholder="{{trans('app.client_email')}}" oninput="this.className = ''" name="clientEmail" id="clientEmail" required></p>
     
    
  </div>
  <div class="tab">{{trans('app.book_a_delivery_date')}}:
    <p><input type="date" min="{{Carbon\Carbon::now()->toDateString()}}"  oninput="this.className = ''" name="booking" id="booking"></p>
    <p><input type="number" placeholder="{{trans('app.how_long_can_you_wait_more')}}" oninput="this.className = ''" name="extra_days"  id="extra_days"></p>
  </div>
  <div class="tab form-group">{{trans('app.book_a_delivery_date')}}:
    <textarea id="more" name="more" placeholder="{{trans('app.tell_me_more')}}" class="form-control" style="height:200px;"></textarea>
  </div>
  <div class="tab" style="text-align:center;">{{trans('app.resume')}}:
    <p id="company"></p>
    <p id="industry"></p>
    <p id="client_name"></p>
    <p id="client_email"></p>
    <p id="booking_date"></p>
    <p id="booking_date_extra"></p>
    <p id="text"></p>
    <div class="form-group-">
      <label for="logo_image">{{trans('app.your_image')}}</label>
    <img src="" id="logo_image" style="width:50%;margin-left:20%;" class="form-control">
    <input type="hidden" name="drawing" value="" id="hidden_image" >
    </div>
    

  </div>
  <div style="overflow:auto;">
    <div style="float:right;">
      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
    </div>
  </div>
  <!-- Circles which indicates the steps of the form: -->
  <div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
  </div>
</form>

@endsection