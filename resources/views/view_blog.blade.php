@extends('layouts.newcase-template')
@section('meta')
<meta property="og:description" content="{{$ogdescription}}">
    
    <meta property="og:image" content="{{asset('img/blog/'.$ogimage)}}">
    <title>{{$ogtitle}}</title>
    <meta property="og:image:width" content="300" />
    <meta property="og:image:height" content="300" />
    <meta property="og:url" content="{{Request::url()}}">
    <meta property="og:type" content="article">
    <meta property="author" content="">
    <meta property="keywords" content="{{trans('app.keywords')}}">
    
    
@endsection
@section('content')
@include('layouts.blog_css')
<div class="row">
  <div class="col-sm-9 col-xsoffset-3 leftcolumn">
    <div class="card">
      <?php $author=App\User::find($blog->user_id); ?>
     
      <h2 style="font-size:40px;" >{{$blog->title}},
        </h2>
        
      <h5>{{$blog->subtitle}}, {{$blog->created_at}}</h5>
      <div class="fakeimg">
      	<img  src="{{asset('img/blog/'.$blog->image)}}" class=" img-fluid img-thumbnail blogmainimg">
      </div>
      <p style="margin-left:20%;margin-right: 14%">{!!$blog->content!!}</p>
      <p>
        
        <ul class="social-icons">
          <li class="share">
            <h4>
          {{trans('app.share')}}:
          </h4>
          </li>
            <li class="facebook">
              <a target="_blank" href="javascript:void(window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent(location.href)),'Facebook Share','width=640, height=480, scrollbars=yes' ));"><i class="fa fa-facebook"></i></a>
            </li>
            <li class="google-plus">
              <a target="_blank" href="javascript: void(window.open('https://plus.google.com/share?url='.concat(encodeURIComponent(location.href)), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'));"><i class="fa fa-google-plus"></i></a>
            </li>
            <li class="linkedin">
              <a target="_blank" href="javascript: void( window.open('https://www.linkedin.com/shareArticle?mini=true&url='.concat(encodeURIComponent(location.href)), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'));"><i class="fa fa-linkedin"></i></a>
            </li>
            <li class="twitter">
              <a  target="_blank" href="javascript: void(window.open('http://twitter.com/home/?status='.concat(encodeURIComponent(document.title)) .concat(' ') .concat(encodeURIComponent(location.href)), 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'));"><i class="fa fa-twitter"></i></a>
            </li>
            <li class="facebook">
              
              
              <?php
                //$userID=Auth::user()->id;
                $numclaps=App\Claps::where('blogid',$blog->id)->count();
              //$clapped=($numclaps==0)?false:true;
               
               ?>
                
              
              
               <a href="{{url('clap/'.$blog->id)}}">
                <img src="{{asset('img/clap.png')}}" style="width: 35px;height:35px;">
              </a>
              
                    {{$blog->claps}}  {{trans('app.claps')}}
            </li>
          </ul>
        
      </p>
    </div>
    <div style="margin-top:10%;" class="fb-comments" data-href="{{Request::url()}}" data-width="50%;"></div>
  </div>
  <div class="rightcolumn">
    <div class="card">

      <h3>{{trans('app.author')}}</h3>
      
      <div class="fakeimg">
        <a href="{{route('users.show',$user->username)}}">
        <img src="{{asset('img/users/'.$user->image)}}">
        <h5>{{$user->username}}</h5>
        </a>
        <p>
            {{trans('app.joined')}}
           <i class="fa fa-clock-o">
                  
               </i>
              {{(new Carbon\Carbon($user->created_at))->diffForHumans()}}</p>
      </div><br>
      
      
    </div>
    <div class="card">
      <h3>{{trans('app.latest_work')}}</h3>
      @foreach($latest_works as $work)
      <div class="fakeimg">
        <a href="{{route('users.show',$user->username)}}">
      	<img src="{{asset('img/portfolio/'.$work->image)}}" class="sidebarimg">
      	<h6>{{mb_substr(strip_tags($work->title),0,30)}}..</h6>
      </a>
      	<p>{{mb_substr(strip_tags($work->subtitle),0,70)}}..</p>
        <p>
           <i class="fa fa-clock-o">
                  
               </i>
              {{(new Carbon\Carbon($work->created_at))->diffForHumans()}}</p>
      </div><br>
      @endforeach
      
    </div>
    <div class="card">
      <h3>{{trans('app.popular_post')}}</h3>
      @foreach($blogs as $related)
      <div class="fakeimg">
        <a href="{{route('blog.show',$related->slug)}}">
      	<img class="sidebarimg" src="{{asset('img/blog/'.$related->image)}}">
      	<p>{{mb_substr(strip_tags($related->title),0,40)}}...</p>
        </a>
        <br>
      	<p>{{mb_substr(strip_tags($related->subtitle),0,70)}}...</p>
        <p>
          <i class="fa fa-clock-o">        
          </i>
              {{(new Carbon\Carbon($related->created_at))->diffForHumans()}}</p>
      </div><br>
      @endforeach
      
    </div>
    <div class="card">
      <h3>{{trans('app.follow_us')}}</h3>
      <p>
      	<a href="{{($user->facebook)?$user->facebook:'https://www.facebook.com/opendarasa/'}}">
      	<i class="fa fa-facebook">
      	 FACEBOOK
      </i>
        </a>
   </p>

      <p>
      	<a href="{{($user->twitter)?$user->twitter:'https://twitter.com/opendarasa'}}">
      	<i class="fa fa-twitter">
      		TWITTER
      	</i>
      </a>
      </p>
    </div>

  </div>
  
</div>





@endsection