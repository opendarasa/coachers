@extends('layouts.newtemplate')

@section('meta')

    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta property="og:description" content="{!!$ogdescription!!}">
    <title>{{$ogtitle}}</title>
    <meta property="og:image" content="{{asset('img/users/'.$ogimage)}}">
    <meta property="og:url" content="{{Request::url()}}">
    <meta property="og:type" content="website">
    <meta property="author" content="">
    <meta property="keywords" content="{{trans('app.keywords')}}">


    
    
@endsection

@section('content')

      <div class="container">
        
          
        
        
      <div class="col-sm-12 ">
        @if($errors->any())
        <div class="col-lg-9 alert alert-danger text-center" style="margin-left:25%;">
          {{$errors->first()}}
        </div>
        @endif
         {!! Form::open( array(
             'route' => 'groups.store', 
             'class' => 'form', 
             'files' => true,
             'id'=>'create_form'
             ))!!}

    {{csrf_field()}}

    
    <div class="form-group row">
    {{Form::label('name',trans('app.full_name'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::text('name',null,['class'=>'form-control'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('username',trans('app.groupusername'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::text('username',null,['class'=>'form-control'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('email',trans('app.email'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::email('email',null,['class'=>'form-control'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('description',trans('app.description_user'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::textarea('description',null,['class'=>'form-control','placeholder'=>trans('app.description_your_start_up'),'id'=>'bio','maxlength'=>'800','minlength'=>'100'])}}
    </div>
    </div>
    
    <div class="form-group row">
    {{Form::label('website',trans('app.website'),['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::url('website',null,['class'=>'form-control'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('facebook','Facebook',['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::url('facebook',null,['class'=>'form-control','id'=>'facebook'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('twitter','Twitter',['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::url('twitter',null,['class'=>'form-control','id'=>'twitter'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label(trans('app.location'),'location',['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::text('location',null,['class'=>'form-control'])}}
    </div>
    </div>
    
    
    <div class="form-group row">
    {{Form::label(trans('app.logo'),'image',['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::file('image',['id'=>'image'])}}
    </div>
    </div>
    <div class="form-group row">
    {{Form::label(trans('app.group_cover'),'cover',['class'=>'col-lg-3 col-form-label form-control-label '])}}
    <div class="col-lg-9">
    {{Form::file('cover',['id'=>'cover'])}}
    </div>
    </div>
    <div class="form-group row">
    <label class="col-lg-3 col-form-label form-control-label">

    </label>
    <div class="col-lg-9">
    <a href="{{url('home')}}" type="button" class="btn btn-secondary pull-left">
        {{trans('app.cancel')}}
    </a>

    <input type="submit" class="btn btn-primary pull-right" value="{{trans('app.save_changes')}}">
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    </div>
    
    {{Form::close()}}



      </div>
        
        
      </div>
    
    
  
@endsection

