@extends('layouts.newtemplate')

@section('meta')
@include('shared.meta')
@endsection

@section('content')

      <div class="container">
        
          <div class="col-sm-12 text-center">
            <h2 style="display:inline-block;" class="section-heading text-uppercase">{{trans('app.edit_blog')}}</h2>

            <a id="deletebtn" style="display: inline-block;margin-left:2%;margin-bottom:3%;float:right;color:white;"  class="badge badge-danger " target="_blank">
              <i class="fa fa-trash" onclick="deleteFunction('{{$blog->id}}')">{{trans('app.delete')}}</i>
            </a>
            <form style="display:none;" id="deleteform_{{$blog->id}}" action="{{route('blog.destroy',[$blog->id])}}" method="POST">
               @method('DELETE')
               @csrf  
          </form>
            
            @if($blog->status==0)
            <a style="display: inline-block;margin-left:2%;margin-bottom:3%; float: right;" href="{{action('HomeController@publishBlog',$blog->slug)}}" class="badge badge-danger">
              <i class="fa fa-unlock-alt">{{trans('app.publish')}}</i>
            </a>
            @else
             <a style="display: inline-block;margin-left:2%;margin-bottom:3%;float:right;" href="{{action('HomeController@unpublishBlog',$blog->slug)}}" class="badge badge-warning">
              <i class="fa fa-lock">{{trans('app.unpublish')}}</i>
            </a>
            @endif
            <a style="display: inline-block;margin-left:2%;margin-bottom:3%;float:right;" href="{{route('blog.show',$blog->slug)}}" class="badge badge-info" target="_blank">
              <i class="fa fa-eye">{{trans('app.preview')}}</i>
            </a>
            
            <div>
              <small id="saving2" style="color:gray;"></small>
            </div>
            
          </div>
        
        
      <div class="col-sm-12">
        @if($errors->any())
        <div class="alert alert-danger">
          {{$errors->first()}}
        </div>
        @endif
         {{Form::model($blog, [
         'method' => 'PATCH',
         'route' => ['blog.update', $blog->slug],
         'files'=>'true',
         'id'=>'edit_form'
         ]) }}
          @include('shared.shared_form')

          <div class="form-group">
            {{Form::button(trans('app.save'),['class'=>'btn btn-info pull-right','id'=>'save_edit'])}}

            <!--<button type="submit" class="btn btn-info pull-right" name="save">
              Save
            </button>-->
            
          </div>
        {{Form::close()}}
      </div>
        
        
      </div>
    <script type="text/javascript">
      window.onload=function(){


        $("#save_edit").click(function(){
      //alert();
        var content=$("#editor").val();
        var title=$("#title").val();
        var subtitle=$("#subtitle").val();
        localStorage.setItem('blogTitle_{{$blog->id}}',title);
        localStorage.setItem('blogSubtitle_{{$blog->id}}',subtitle);
        localStorage.setItem('blogContent_{{$blog->id}}',content);

        $("#edit_form").submit();
      });
        if(localStorage.getItem("blogContent_{{$blog->id}}")!=undefined)
    {
      $("#editor").summernote('code',localStorage.getItem("blogContent_{{$blog->id}}"));
    }
    if(localStorage.getItem("blogTitle_{{$blog->id}}")!=undefined){
      $("#title").val(localStorage.getItem("blogTitle_{{$blog->id}}"));
    }

    if(localStorage.getItem("blogSubtitle_{{$blog->id}}")!=undefined){
      $("#subtitle").val(localStorage.getItem("blogSubtitle_{{$blog->id}}"));
    }


    var formCheck=true;
     $("#edit_form").submit(function(){
      //alert();
      var title=$("#title").val();
      var subtitle=$("#subtitle").val();
      var type=$("#type").val();
      var editor=$("#editor").val();
      //var myfile=$("#myfile").val();
      var tags=$("#tags").val();
      if(title==''){
        $("#title").focus();
        $("#title_alert").html("{{__('app.title_is_required')}}");
        formCheck=false;

      }else{
        $("#title_alert").html("");
        formCheck=true;
      }
      if(subtitle==''){
        $("#subtitle").focus();
        $("#subtitle_alert").html("{{__('app.subtitle_is_required')}}");
        formCheck=false;
      }else{
        $("#subtitle_alert").html("");
         formCheck=true;
      }
      if(type==''){
        $("#type").focus();
        $("#type_alert").html("{{__('app.type_is_required')}}");
        formCheck=false;
      }else{
        $("#type_alert").html("");
         formCheck=true;
      }
      if(type==''){
        $("#type").focus();
        $("#type_alert").html("{{__('app.type_is_required')}}");
        formCheck=false;
      }else{
        $("#type_alert").html("");
         formCheck=true;
      }

      if(editor==''){
        $("#editor").focus();
        $("#editor_alert").html("{{__('app.editor_is_required')}}");
        formCheck=false;
      }else{
        $("#editor_alert").html("");
         formCheck=true;
      }
      /*if(myfile==''){
        $("#myfile").focus();
        $("#myfile_alert").html("{{__('app.myfile_is_required')}}");
        formCheck=false;
      }else{
        $("#myfile_alert").html("");
         formCheck=true;
      }*/
      if(tags==''){
        $("#tags").focus();
        $("#tags_alert").html("{{__('app.tags_is_required')}}");
        formCheck=false;
      }else{
        $("#tags_alert").html("");
         formCheck=true;
      }

      return formCheck;
     });

       }

      setInterval(function(){
        var content=$("#editor").val();
        var title=$("#title").val();
        var subtitle=$("#subtitle").val();
        //console.log("saving")
        localStorage.setItem('blogTitle_{{$blog->id}}',title);
        localStorage.setItem('blogSubtitle_{{$blog->id}}',subtitle);
          if(content.length>10){
            $("#saving2").html("{{__('app.autosaved')}}...");

            localStorage.setItem('blogContent_{{$blog->id}}',content);
            setTimeout(function(){
              $("#saving2").html("");
            },3000)
          }
        

      },20000)

    
    </script>
  
@endsection