@extends('layouts.newcase-template')
@section('meta')
@include('shared.meta')
@endsection

@section('content')
<form>
	
<div class="form-group">
	<input type="text" name="title" class="form-control">
</div>
<div class="form-group">
	<textarea class="form-control" name="content" id="summernote">
		
	</textarea>
</div>
</form>

@endsection