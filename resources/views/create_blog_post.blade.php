@extends('layouts.newtemplate')

@section('meta')
@include('shared.meta')
@endsection
@section('content')

      <div class="container">
        @if(count($hottopics)>0)
          <div class="row">
            <span>
              {{__('app.hot_topics')}} :&nbsp;&nbsp;
            </span>
            @foreach($hottopics as $topic)
            <a href="" class="badge badge-default" style="color:white;">
              #{{$topic->keyword}}
            </a>&nbsp;
            @endforeach
        
          </div>
          <hr>
          @endif
          <div class="col-sm-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.new_blog')}}</h2>
              <small style="color:gray" id="saving"></small>
          </div>
        
        
      <div class="col-sm-12" style="margin-bottom:4%;">
        @if($errors->any())
        <div class="alert alert-danger text-center">
          {{$errors->first()}}
        </div>
        @endif

        @if(Session::has('SlugError'))
        <div class="alert alert-danger text-center">
          {{trans('app.slug_error')}}
        </div>
        @endif
           <!--<form method="post" action="{{route('blog.store')}}" enctype="multipart/form-data">-->
       {{Form::open( array(
             'route' => 'blog.store', 
             'class' => 'form', 
             'files' => true,
             'id'=>'create_form'
             ))}}

           @include('shared.shared_form')

          <div class="form-group">
            {{Form::button(trans('app.save'),['class'=>'btn btn-info btn-lg pull-right','id'=>'save_blog'])}}
            <!--<button type="submit" class="btn btn-info pull-right" name="save">
              Save
            </button>-->
            
          </div>
           {{Form::close()}}
         <!--</form>-->
      </div>
        
        
      </div>
    
    <script type="text/javascript">
      window.onload=function(){
        
         $("#save_blog").click(function(){
          var content=$("#editor").val();
        var title=$("#title").val();
        var subtitle=$("#subtitle").val();
        localStorage.setItem('blogTitle',title);
        localStorage.setItem('blogSubtitle',subtitle);
        localStorage.setItem('blogContent',content);
        $("#create_form").submit();
      });

    if(localStorage.getItem("blogContent")!=undefined)
    {
      $("#editor").summernote('code',localStorage.getItem("blogContent"));
    }
    if(localStorage.getItem("blogTitle")!=undefined){
      $("#title").val(localStorage.getItem("blogTitle"));
    }

    if(localStorage.getItem("blogSubtitle")!=undefined){
      $("#subtitle").val(localStorage.getItem("blogSubtitle"));
    }



    var formCheck=true;
     $("#create_form").submit(function(){
      //alert();
      var title=$("#title").val();
      var subtitle=$("#subtitle").val();
      var type=$("#type").val();
      var editor=$("#editor").val();
      var myfile=$("#myfile").val();
      var tags=$("#tags").val();
      if(title==''){
        $("#title").focus();
        $("#title_alert").html("{{__('app.title_is_required')}}");
        formCheck=false;

      }else{
        $("#title_alert").html("");
        formCheck=true;
      }
      if(subtitle==''){
        $("#subtitle").focus();
        $("#subtitle_alert").html("{{__('app.subtitle_is_required')}}");
        formCheck=false;
      }else{
        $("#subtitle_alert").html("");
         formCheck=true;
      }
      if(type==''){
        $("#type").focus();
        $("#type_alert").html("{{__('app.type_is_required')}}");
        formCheck=false;
      }else{
        $("#type_alert").html("");
         formCheck=true;
      }
      if(type==''){
        $("#type").focus();
        $("#type_alert").html("{{__('app.type_is_required')}}");
        formCheck=false;
      }else{
        $("#type_alert").html("");
         formCheck=true;
      }

      if(editor==''){
        $("#editor").focus();
        $("#editor_alert").html("{{__('app.editor_is_required')}}");
        formCheck=false;
      }else{
        $("#editor_alert").html("");
         formCheck=true;
      }
      if(myfile==''){
        $("#myfile").focus();
        $("#myfile_alert").html("{{__('app.myfile_is_required')}}");
        formCheck=false;
      }else{
        $("#myfile_alert").html("");
         formCheck=true;
      }
      if(tags==''){
        $("#tags").focus();
        $("#tags_alert").html("{{__('app.tags_is_required')}}");
        formCheck=false;
      }else{
        $("#tags_alert").html("");
         formCheck=true;
      }

      return formCheck;
     })

     
       }

      setInterval(function(){
        var content=$("#editor").val();
        var title=$("#title").val();
        var subtitle=$("#subtitle").val();

        localStorage.setItem('blogTitle',title);
        localStorage.setItem('blogSubtitle',subtitle);
          if(content.length>10){
            $("#saving").html("{{__('app.autosaved')}}...");

            localStorage.setItem('blogContent',content);
            setTimeout(function(){
              $("#saving").html("");
            },3000)
          }
        

      },20000)


  
    </script>


  
@endsection