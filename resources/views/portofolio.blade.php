@extends('layouts.newcase-template')

@section('meta')
@include('shared.meta')
@endsection
@section('content')

<section class="bg-light" id="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.portofolio')}}</h2>
            <hr width="50%">
          </div>
        </div>
        
          @include('shared.grid')
          
        
      </div>
    </section>
    <!-- Portfolio Modals -->

    <!-- Modal 1 -->
    @include('shared.modal')
    <!-- Modal 2 -->
    
@endsection