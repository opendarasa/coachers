@extends('layouts.newcase-template')

@section('meta')
@include('shared.meta')
@endsection
@section('content')
<div style="margin-top:3%;width:100%;">
  @include('shared.search_form')
</div>
 @include('shared.sort_bycategory')
 
  @if(Auth::check() && $favblogs->count()>0)
<section class="bg-light" id="" style="margin-top:-4%;">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.blog_post')}}</h2>
            <hr width="50%">
          </div>
        </div>
        <div class="row">
         
          @include('shared.favblogs')
          
          
        </div>
      </div>
      
    </section>
<hr>
@endif
<section class="bg-light" id="portfolio" style="margin-top:-4%;">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.latest_bog_post')}}</h2>
            <hr width="50%">
          </div>
        </div>
        <div class="row">
          
          @include('shared.blog_post')
          
        </div>
      </div>
      
    </section>

    @include('shared.sort_by_categoryjs')

  @endsection