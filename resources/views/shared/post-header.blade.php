 <!-- ==============================================
	 Header
	 =============================================== -->
	 <header class="page-post-2" style="background:url('{{($blog->cover!=null)?asset('img/users/'.$blog->cover):asset('img/blog/13.jpg')}}');background-repeat: no-repeat;background-size:100%;">
      <div class="container">
       <div class="row">
	   
        <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
         <div class="post-heading" style="background-color: rgba(255, 255, 255, 0.4);">
		  <p class="entry-meta" >
		   <span class="date updated time" >
		    <time style="font-size:16pt;color:#4a4b52"  class="entry-modified-time">{{$blog->created_at}}</time>
		   </span> &nbsp; / &nbsp; 
		   <span class="entry-categories" >
		    <a style="font-size:16pt;color:#33655b;" href="#" rel="category tag">{{trans('app.blog_types.'.$blog->cat_id)}}</a>
		   </span>
		  </p>
		  <h1 style="color:#464a4c;">{{$blog->title}}</h1>
         </div><!-- /.post-heading -->
        </div><!-- /.col-lg-8 -->
		
       </div><!-- /.row -->
	  </div><!-- /.container -->
     </header><!-- /header -->
	 
     <!-- ==============================================
	 Header
	 =============================================== -->	