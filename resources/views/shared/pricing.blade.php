<section id="about" style="margin-left:10%;">
      <div class="container">
        <div class="row">
          <div class=" col-sm-12 col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{{trans('app.pricing')}}</h2>
            
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4">
            <div class="card" style="width: 18rem;">
              <div class="card-header">
                {{trans('app.mobile_dev')}}
              </div>
             <ul class="list-group list-group-flush">
                <li class="list-group-item">50$/hr</li>
                <li class="list-group-item">{{trans('app.delivery_in')}} : 3 {{trans('app.weeks')}}</li>
                <li class="list-group-item">
                <a id="hire_mobile" href="{{url('new-ui-case')}}" class="btn btn-danger" role="button">{{trans('app.submit_project')}}</a>
                </li>
             </ul>
           </div>
            
          </div>
          <div class="col-lg-4">
            <div class="card" style="width: 18rem;">
              <div class="card-header">
                {{trans('app.web_design')}}
              </div>
             <ul class="list-group list-group-flush">
                <li class="list-group-item">120$/hr</li>
                <li class="list-group-item">{{trans('app.delivery_in')}} : 3 {{trans('app.weeks')}}</li>
                <li class="list-group-item">
                <a id="hire_web" href="{{url('new-web-case')}}" class="btn btn-danger" role="button">{{trans('app.submit_project')}}</a>
                </li>
             </ul>
           </div>
            
          </div>
          <div class="col-lg-4">
            <div class="card" style="width: 18rem;">
              <div class="card-header">
                {{trans('app.seo')}}
              </div>
             <ul class="list-group list-group-flush">
                <li class="list-group-item">20$/hr</li>
                <li class="list-group-item">{{trans('app.delivery_in')}} : 3 {{trans('app.weeks')}}</li>
                <li class="list-group-item">
                <a id="hire_seo" href="{{url('new-seo-case')}}" class="btn btn-danger" role="button">{{trans('app.submit_project')}}</a>
                </li>
             </ul>
           </div>
            
          </div>
          
      </div>
    </section>