<div class="col-lg-12" style="text-align:center;">
            @foreach(trans('app.blog_types') as $key=>$value)
            <button onclick="filterBlog('{{$key}}')" id="{{$key}}" class="badge badge-dark ">{{$value}}</button>

            @endforeach
            <button onclick="filterBlog('all')" id="all" class="badge badge-dark ">{{trans('app.see_all')}}</button>
  </div>