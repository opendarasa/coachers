 <div class="col-md-3">
		
		 <div class="card widget-info-one">
		  <div class="widget-avatar-img">
		  	@if($user->cover!=null)
		  <img class="card-img-top img-fluid" src="{{asset('img/users/'.$user->cover)}}" alt="">
		  @else
		  <img class="card-img-top img-fluid" src="img/posts/image.jpg" alt="">
		  @endif
		 </div><!-- /.avatar-img -->
		    <div class="card-block">
		 <div class="inner">
		  <div class="widget-avatar pull-left"><img alt="" src="{{asset('img/users/'.$user->image)}}"></div>
		   <h5 style="display:inline-block;"><a href="{{route('users.show',$user->username)}}">{{$user->name}}</a>
		   		
		   </h5>
		   <a style="display:inline-block;" href="{{route('users.edit',$user->username)}}" title="{{trans('app.edit_profile')}}">
             		<i class="fa fa-pencil" style="font-size:10px;">
             		
             	    </i>
             	</a>
		   <br><br>
		   <span class="subtitle">{{$user->description}}</span>
		  </div><!-- /.inner -->
		  </div>
          <div class="card-footer">
           <ul class="post-view">
			<li><a href="{{url('followers/'.$user->username)}}" title="{{trans('app.followers')}}"><i class="fa fa-users"></i></a>{{$followers->count()}}</li>
            <li class="active"><a href="{{url('followers/'.$user->username)}}" title="{{trans('app.following')}}"><i class="fa fa-user-plus"></i></a>{{$followings->count()}}</li>
            <li><a href="{{route('blog.create')}}" title="{{trans('app.add_post')}}"><i class="fa fa-edit"></i></a>{{$personalblogs->count()}}</li>
           </ul><!-- /.post-view -->
          </div><!-- /.card-footer -->
         </div><!-- /.panel -->
		 
		<div class="card widget-info-two">
		  <ul class="list-group list-group-flush">
			<li class="list-group-item"><i class="fa fa-map-marker"></i>&nbsp; {{$user->location}}</li>
			@if($user->badge=='G')
			<li class="list-group-item"><i class="fa fa-check"></i>&nbsp; {{__('app.golden_writer')}}</li>
			@elseif($user->badge=='B')
             <li class="list-group-item"><i class="fa fa-check"></i>&nbsp; {{__('app.bronze_writer')}}</li>
			@elseif($user->badge=='S')
             <li class="list-group-item"><i class="fa fa-check"></i>&nbsp; {{__('app.silver_writer')}}</li>
			@else
             <li class="list-group-item"><i class="fa fa-check"></i>&nbsp; {{__('app.simple_writer')}}</li>
			@endif
			<li class="list-group-item"><i class="fa fa-calendar"></i>&nbsp; {{trans('app.joined').'  '.(new Carbon\Carbon($user->created_at))->diffForHumans()}}</li>
			<li class="list-group-item"><i class="fa fa-globe"></i>&nbsp; 
			 
			 <input style="border-radius:4px;"  id="copytarget" type="text" value="{{config('app.url').'/users/'.$user->username}}">
			 	<button id="copybtn">
			 		<i class="fa fa-files-o">
			 			
			 		</i>
			 	</button>
			</li>
		  </ul>
		</div>


		<div class="card widget-info-two">
		  <div class="card-body">
		   <div class="card-block">
			<h4 class="card-title info">{{__('app.add_skills')}}</h4>
			
	          
            <div>
				@if($user->id==Auth::user()->id)
			
				{{Form::open( array( 
	             'class' => 'form', 
	             'files' => true,
	             'id'=>'saveskill'
	             ))}}
	             <div class="form-group">
	             	{{Form::text('skill',null,['class'=>'form-control','placeholder'=>trans('app.skill_name'),'id'=>'skill_name'])}}
	             	<small id="skill_name_alert" style="color:red"></small>
	             </div>
	             <div class="form-group">
	             	{{Form::label('level',trans('app.your_skill_level'))}}
	             	{{Form::select('level',trans('app.skill_levels'),'',['class'=>'form-control','id'=>'level'])}}
	             </div>
	             <div class="form-group">
	             	{{Form::button(trans('app.save'),['class'=>'btn btn-primary pull-right','id'=>'submitskill'])}}
	             	<div class="loader" id="skillloader"></div>
	             </div>
	             {{Form::close()}}

	             {{Form::open( array( 
	             'class' => 'form', 
	             'files' => true,
	             'id'=>'editskill'
	             ))}}
	             <div class="form-group">
	             	{{Form::text('skill',null,['class'=>'form-control','placeholder'=>trans('app.skill_name'),'id'=>'edit_skill_name'])}}
	             	<small id="edit_skill_name_alert" style="color:red"></small>
	             </div>
	             <div class="form-group">
	             	{{Form::label('editlevel',trans('app.your_skill_level'))}}
	             	{{Form::select('level',trans('app.skill_levels'),'',['class'=>'form-control','id'=>'editlevel'])}}
	             </div>
	             {{Form::hidden('id',null,['id'=>'skill_id'])}}

	             <div class="form-group">
	             	{{Form::button(trans('app.save'),['class'=>'btn btn-primary pull-right','id'=>'submiteditskill'])}}
	             	<div class="loader" id="skillloader2"></div>
	             </div>
	             {{Form::close()}}
	             @endif
			</div>

			<div class="row" style="margin-top:30%;">
				
				
				
				 @if(count($skills)>0)
            <div class="text-left" style="margin-top:10%;" >
            	
            	
            	@foreach($skills as $skill)
             	  <span  class="badge badge-default " style="color:white;margin-bottom:4%;">
             	  	{{$skill->skill}}
             	  	@if(Auth::user()->id==$user->id)
             	  	<a  onclick="editSkill('{{$skill->id}}')">
             	  		<i class="fa fa-pencil">
             	  		
             	  	    </i>
             	  	</a>
             	  	<a  onclick="deleteSkill('{{$skill->id}}')">
             	  		<i class="fa fa-trash">
             	  		
             	  	    </i>
             	  	</a>
             	  	<input type="hidden" name="" id="skill_{{$skill->id}}" value="{{$skill->skill}}">
             	  	<input type="hidden" name="" id="level_{{$skill->id}}" value="{{$skill->level}}">
             	  	@endif
             	  	
             	  	
             	  </span>
             	 @endforeach
                 
            </div>	
            @endif
				
			</div>			
		   </div>
		  </div>
	     </div>		 
		
        </div><!-- /.col-md-4 -->