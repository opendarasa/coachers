
    
             
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta property="og:description" content="{{(isset($ogdescription))?$ogdescription:trans('app.description')}}">
    <title>{{(isset($ogtitle))?$ogtitle:trans('app.defaulttagline')}}</title>
    <meta property="og:image" content="{{asset('img/'.config('meta.defaultimage'))}}">
    <meta property="og:url" content="{{Request::url()}}">
    <meta property="og:type" content="website">
    <meta property="author" content="">
    <meta property="og:keywords" content="{{trans('app.keywords')}}">
<meta name="viewport" content="width=device-width, initial-scale=1">