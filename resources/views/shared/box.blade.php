<div class="box">
		  <form>
		   <textarea class="form-control no-border" rows="3" placeholder="Type something..."></textarea>
		  </form>
		  <div class="box-footer clearfix">
		   <button class="btn btn-info pull-right btn-sm">Post</button>
		   <ul class="nav nav-pills nav-sm">
			<li class="nav-item"><a class="nav-link" href=""><i class="fa fa-camera text-muted"></i></a></li>
			<li class="nav-item"><a class="nav-link" href=""><i class="fa fa-video-camera text-muted"></i></a></li>
		   </ul>
		  </div>
</div>		