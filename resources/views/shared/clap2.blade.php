<?php
               
  $clapped=App\Claps::where('blogid',$related->id)->where('uid',Auth::user()->id)->count();
              
 ?>
 @if($clapped >0)
<a id="clap" href="{{url('clapr/'.$related->id)}}" class="">
	<i class="fa fa-heart">&nbsp;&nbsp;{{$related->claps}}</i>
</a>
@else
<a id="clap" href="{{url('clap/'.$related->id)}}" class="">
	<i class="fa fa-heart-o">&nbsp;&nbsp;{{$related->claps}}</i>
</a>
@endif