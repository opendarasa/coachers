
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {
  alert()
        var flag=true;
        if($("#name").val()!="")
        {
            $("#cname").html($("#name").val());
        }
        else{
            $("#namealert").html("name cannot be empty");
            $("#name").focus();
            flag=false;
        }
        if($("#email").val()!="")
        {
            var email=$("#email").val();
            if(validateEmail(email))
            {
                $("#cemail").html($("#email").val());
            }else{
                $("#emailalert").html("please input an email address");
                $("#email").focus();
                flag=false;
            }
            
        }else{
            $("#emailalert").html("email cannot be empty");
            $("#email").focus();
            flag=false;
        }
        if($("#receiptno").val()!="")
        {
            $("#creceiptno").html($("#receiptno").val());
        }
        if($("#phone").val()!="")
        {
            $("#cphone").html($("#phone").val());
        }
        if (flag) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);
        }
        




    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});


    function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
