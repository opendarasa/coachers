 <article class="post-comments" id="aboutposter">
   <div class="container">
       <div class="row">
        <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
			 <div class="comments-list">
			 	<div class="comment-body">
						<div class="author-avatar">
						   <img alt="..." src="{{asset('img/users/'.$blog->userimage)}}" class="img-fluid" width="100" height="100">        
						</div>
						  <div class="comment-content">
							<a href="{{route('users.show',$blog->username)}}">{{$blog->postername}}</a>
							<?php $reader=$user; ?>
							@include('shared.follow_btn')
							<p>
								{!!$blog->userdescription!!}
							</p>
						 </div>
				</div>

			</div>
		</div>
	</div>
</div>
</article>

	
