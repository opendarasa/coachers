<style type="text/css">
    .loader {
  border: 1px solid #f3f3f3; /* Light grey */
  border-top: 1px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 15px;
  height: 15px;
  animation: spin 2s linear infinite;
  display:none;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<div id="list" style="">
@foreach($users as $user)
<?php
  $user_skills=App\Skills::where('user_id',$user->id)->get();


 ?>
<div class="container" id="user_{{$user->id}}">
    <div class="card flex-row flex-wrap">
        <div class="col-sm-12" style="display:inline-block;">
                <span style="float:right;">
                    @if($user->badge=='G')
                    <img style="width:160%;" src="{{asset('img/icons/medal_gold.png')}}">
                    @elseif($user->badge=='S')
                    <img style="width:160%;" src="{{asset('img/icons/medal_silver.png')}}">
                    @elseif($user->badge=='B')
                    <img style="width:160%;" src="{{asset('img/icons/medal_bronze.png')}}">
                    @else
                    
                    @endif
                </span>
            </div>
        <div class="card-header border-0" style="display:inline-block;">

            <a href="{{route('users.show',$user->username)}}">
            <div style="margin-bottom:4%;">
                <img height="200" width="200" class="thumb-lg img-circle" src="{{asset('img/users/'.$user->image)}}" alt="profile image">
            </div>
            </a>
            <div class="col-sm-12" style="text-align:center;">
               
            </div>
            
        </div>
        <div class="card-block px-2">
            
            
                <h4 class="card-title">
                <span style="display:inline-block;">
                    <a href="{{route('users.show',$user->username)}}">{{$user->name}}</a>
                </span>
                <?php $reader=$user; ?>
                @include('shared.follow_btn')
                
               </h4>


            
            
            <span class="text-muted">{{trans('app.worked_at')}}: {{$user->company}}</span>
            <p class="card-text">{{mb_substr(strip_tags($user->description),0,100)}}...</p>
            <p class="card-text">
                {{(isset($user->experience))?$user->experience:''}} {{trans('app.years_experience')}}
            </p>
            <p>
                
                <?php $skills=explode(",",$user->skills);
                 
                 ?>
                @if(count($user_skills)>0)
                <h6>{{trans('app.skills')}}</h6>
                @foreach($user_skills as $skill)
                <a href="#" class=" badge badge-default" style="color:white;">{{$skill->skill}}
                </a>
                @endforeach

                @else
                <h6>{{trans('app.no_skills-earned_yet')}}</h6>
                @endif

            </p>
            <p>
                <i class="fa fa-map-marker">
                    {{$user->location}}
                </i>
            </p>
            <a  href="{{route('users.show',$user->username)}}" class="badge badge-info">{{trans('app.view_user')}}</a>
            <br><br>
          </div>

        <div class="w-100"></div>
        <div class="card-footer w-100 text-muted">
            {{trans('app.joined').'  '.(new Carbon\Carbon($user->created_at))->diffForHumans()}}
        </div>
    </div>
    <br>
</div>
@endforeach
</div>

<script type="text/javascript">

    
    function Mysort() {
    // Declare variables
    var input, filter, listDiv, span, h4, i;
    input = document.getElementById('mysearch');
    filter = input.value.toUpperCase();
    listDiv = document.getElementById("list");
    h4 = listDiv.getElementsByTagName('h4');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < h4.length; i++) {
        span = h4[i].getElementsByTagName("span")[0];
        var parentDiv=h4[i].parentNode.parentNode.parentNode;
        if (span.innerHTML.toUpperCase().indexOf(filter) > -1) {
            parentDiv.style.display = "";
        } else {
            parentDiv.style.display = "none";
        }
    }
}
</script>
