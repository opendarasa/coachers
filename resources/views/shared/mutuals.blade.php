@if(count($mutuals)>0)
<section class="box-typical">
			<header class="box-typical-header-sm">People you may know</header>
			<div class="people-rel-list">
				<ul class="people-rel-list-photos">
					@foreach($mutuals as $mutual)
					 	@if($mutual['id']!=Auth::user()->id)
					<li><a href="{{route('users.show',$mutual['username'])}}"><img src="{{asset('img/users/'.$mutual['image'])}}" alt="{{$mutual['name']}}" title="{{$mutual['name']}}"></a></li>
					    @endif
					@endforeach
					
				</ul>
				<form class="site-header-search" action="{{url('users-search')}}" method="get">
					{{csrf_field()}}
					<input placeholder="Search for people" type="text" name="keyword">
					<button type="submit">
						<span class="fa fa-search"></span>
					</button>
					<div class="overlay"></div>
				</form>
			</div>
		</section>	
@endif