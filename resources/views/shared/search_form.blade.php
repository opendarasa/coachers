<form class="col-sm-12" method="post" action="{{url('blog-search')}}">
	{{csrf_field()}}
	
	<div class="form-group col-sm-12">
			<input type="text" name="keyword" class="form-control" placeholder="{{__('app.type_keword_to_search')}}">
	</div>
	
	<div class="form-group col-sm-3 pull-right">
			<button style="width:100%;" type="submit" class="btn btn-primary ">
				{{__('app.search')}}
	 		</button>
	</div>
</form>