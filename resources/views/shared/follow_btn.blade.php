<span style="float:right;">
                    @if(Auth::check())

                    <?php 
                     $checkIfFollow=App\Followers::where('follower_id',Auth::user()->id)->where('user_id',$reader->id)->count();
                     $checkIfFollowing=App\Followers::where('user_id',Auth::user()->id)->where('follower_id',$reader->id)->count();
                    ?>
                      
                      @if($reader->id!=Auth::user()->id)
                          @if($checkIfFollow==0)
                      
                    <a style="color:white;" id="follow_{{$reader->id}}" style="" onclick="sendFollow('{{$reader->id}}')"  class="badge badge-default ">
                        <i class="fa fa-plus">
                            
                        </i>
                        {{__('app.follow')}}
                        <div class="loader" id="loader_{{$reader->id}}"></div>
                    </a>
                    <a style="display:none;color:white;" id="unfollow_{{$reader->id}}" style="" onclick="sendUnfollow('{{$reader->id}}')" class="badge badge-default ">
                        <i class="fa fa-minus">
                            
                        </i>
                        {{__('app.unfollow')}}
                        <div class="loader" id="loader_{{$reader->id}}"></div>
                       </a>
                          @else
                        <a style="color:white;" id="unfollow_{{$reader->id}}" style="" onclick="sendUnfollow('{{$reader->id}}')" class="badge badge-default ">
                        <i class="fa fa-minus">
                            
                        </i>
                        {{__('app.unfollow')}}
                        <div class="loader" id="loader_{{$reader->id}}"></div>
                       </a>
                       <a style="display:none;color:white;" id="follow_{{$reader->id}}" style="" onclick="sendFollow('{{$reader->id}}')"  class="badge badge-default ">
                        <i class="fa fa-plus">
                            
                        </i>
                        {{__('app.follow')}}
                        <div class="loader" id="loader_{{$reader->id}}"></div>
                    </a>

                          @endif
                       @endif
                       
                    @else
                    <a  style="color:white;" id="follow_{{$reader->id}}" style="" onclick="sendFollow('{{$reader->id}}')" class="badge badge-dark ">
                        <i class="fa fa-plus">
                            
                        </i>
                        {{__('app.follow')}}
                        <div class="loader" id="loader_{{$reader->id}}"></div>
                    </a>
                     <a style="display:none;color:white;" id="unfollow_{{$reader->id}}" style="" onclick="sendUnfollow('{{$reader->id}}')" class="badge badge-default ">
                        <i class="fa fa-minus">
                            
                        </i>
                        {{__('app.unfollow')}}
                        <div class="loader" id="loader_{{$reader->id}}"></div>
                       </a>

                       
                    @endif
                </span>