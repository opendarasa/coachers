<!-- ==============================================
	 User Section
	 =============================================== -->
     <section class="user-section">	 
	  <div class="container">
	   <div class="profile-user-box">
	    <div class="row">
		 <div class="col-sm-6">
		  <span class="pull-left m-r-15"><img src="{{asset('img/users/'.$user->image)}}" alt="" class="thumb-lg img-circle"></span>
		  <div class="media-body">
		   <h4 class="m-t-5 m-b-5 ellipsis">{{$user->name}}</h4>
		   <p class="font-13"> {{$user->company}}</p>
		   <p class="text-muted m-b-0"><small>{{$user->location}}</small></p>
		  </div><!-- /media-body -->
		 </div><!-- /col-sm-6 -->
		 <div class="col-sm-6">
		  <div class="text-right">
		  	@if(Auth::user()->id==$user->id)
		   <a href="{{route('users.edit',$user->username)}}" role="button" class="btn btn-success waves-effect waves-light">
		    <i class="fa fa-user m-r-5"></i> {{__('app.edit_profile')}}
		   </a>
		   @else

		   <?php $reader=$user; ?>
		   @include('shared.follow_btn')
		  
		   @endif
		  </div><!-- /text-right -->
		 </div><!-- /col-sm-6 -->
	    </div><!-- /row -->
	   </div><!--/ profile-user-box -->
	  </div><!-- /container -->
	 </section><!-- /section --> 
	  
    <!-- ==============================================
     Rate the user
     =============================================== -->

     <div class="modal fade" id="rating" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{__('app.rate_user')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align:center;">
        <div class="torate" data-rate-value="0" style="font-size:40px;display:inline-block;"></div>
        <small id="ratinghelp" style="color:red"></small>
        <textarea id="ratingcomment" style="height:150px;width:80%;" id="comment" placeholder="{{__('app.add_a_comment')}}" maxlength="100"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('app.close')}}</button>
        <button type="button" class="btn btn-primary" id="saverating">{{__('app.save')}}</button>
      </div>
    </div>
  </div>
</div>	

