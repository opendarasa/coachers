<!-- ==============================================
     Posts Section
     =============================================== -->		  
	  @if(count($relateds)>0)
	 <section class="posts-2"  style="background:#fff">
	  <div class="container">
	  		<h3>
	   		{{__('app.related_articles')}}
	   	</h3>
	   <div class="row">
	   
	     @foreach($relateds as $related)
	     <?php $comment_count=App\Comments::where('blog_id',$related->id)->count(); ?>
	    <div class="col-sm-4">
		 <div class="blog-row blog-compact blog-compact-sm">
		  <div class="blog-col">
		   <div class="blog-relative blog-hover blog-hover-scale">
		    <div class="blog-image blog-vgradient-black-lg">
		   <div class="overlay">
		     <a href="{{route('blog.show',$related->slug)}}"><img class="img-fluid blog-img" src="{{asset('img/blog/'.$related->image)}}" alt="blog image" width="750" height="500">
		     </a>
			</div> 
		    </div>
		    <div class="blog-content">
		     <p class="blog-user"><img class="" src="{{asset('img/users/'.$related->userimage)}}" alt="..." width="25" height="25"/> &nbsp;
		     	<em>
		     	<a href="{{route('users.show',$related->username)}}">{{$related->postername}}</a>
		        </em>
		    </p>
		     <a href="{{route('blog.show',$related->slug)}}">
		      <h5>{{$related->title}}</h5>
		     </a>
		     <div class="blog-detail">
		      <span class="blog-detail-text"><i class="fa fa-clock-o"></i> {{(new Carbon\Carbon($related->created_at))->diffForHumans()}}</span>
		      <span class="blog-detail-text"><i class="fa fa-comments"></i> {{$comment_count}}</span>
		      <span class="blog-detail-text">@include('shared.clap2')</span>
		     </div><!-- /blog-detail -->
		    </div><!-- /blog-content -->
		   </div><!-- /blog-relative -->
		  </div><!-- /blog-col -->
		 </div><!-- /blog-row -->
		</div><!-- /col-sm-4 -->
	     @endforeach
	    
	   
	   </div><!-- /.row -->
	  </div><!-- /.container -->
     </section><!-- /.section -->
     @endif