 <?php $class=''; ?>
 
@if(isset($blogs))
 @foreach($blogs as $blog)
 <?php $comment_count=App\Comments::where('blog_id',$blog->id)->count(); ?>
 <?php 
  $checkIfBlocked=App\BlockedUser::where('blocked_id',Auth::user()->id)->where('user_id',$blog->user_id)->count();
  $checkIfBlocked2=App\BlockedUser::where('blocked_id',$blog->user_id)->where('user_id',Auth::user()->id)->count();
  ?>
  @if($checkIfBlocked==0 && $checkIfBlocked2==0)
 <div class="tr-section feed">
		  <div class="tr-post">
		  	<a href="{{route('blog.show',['slug'=>$blog->slug])}}">
		   <div class="entry-header">
		    <div class="entry-thumbnail">
		     <img class="img-fluid" src="{{asset('img/blog/'.$blog->image)}}" alt="Image">
		    </div><!-- /entry-thumbnail -->
	       </div><!-- /entry-header -->
	       </a>
		   <div class="post-content">
		    <div class="author-post">
		     <a href="{{route('users.show',$blog->username)}}"><img class="img-fluid rounded-circle" src="{{asset('img/users/'.$blog->userimage)}}" alt="Image"></a>
		    </div><!-- /author -->
		    <div class="entry-meta">
		     <ul>
			  <li><a href="#">{{$blog->postername}}</a></li>
			  <li>{{(new Carbon\Carbon($blog->created_at))->diffForHumans()}}</li>
			  <li><i class="fa fa-align-left"></i>&nbsp;{{rand(3,10)}} {{__('app.min_read')}}</li>
			  @if($blog->status==0)
			  <li><i  class="fa fa-lock"></i>&nbsp; {{__('app.private')}}<</li>

			  @endif
			  @if($blog->user_id==Auth::user()->id)
			  <li><a  href="{{route('blog.edit',$blog->slug)}}">
			  	<i  class="fa fa-pencil"></i></a>&nbsp;
			  </li>
			  <li><a id="deletebtn" onclick="deleteFunction('{{$blog->id}}')">
			  	<i  class="fa fa-trash"></i>
			     </a>&nbsp;
			  </a>
			  </li>
			  <form style="display:none;" id="deleteform_{{$blog->id}}" action="{{route('blog.destroy',[$blog->id])}}" method="POST">
               @method('DELETE')
               @csrf  
          </form>
			  @endif
		     </ul>
			</div><!-- /.entry-meta -->
			<h2><a href="{{route('blog.show',['slug'=>$blog->slug])}}" class="entry-title">{{$blog->title}}</a></h2>
			<p>{{$blog->subtitle}}</p>
			<div class="read-more">
		     <div class="feed pull-left">
			  <ul>
			  <li><i class="fa fa-comments"></i>{{$comment_count}}</li>&nbsp;
			  
			  <li>@include('shared.clap')</li>
			  </ul>	
		     </div><!-- /feed -->
			 <div class="continue-reading pull-right">
			  <a href="{{route('blog.show',['slug'=>$blog->slug])}}">Continue Reading <i class="fa fa-angle-right"></i></a>
			 </div><!-- /continue-reading -->
			</div><!-- /read-more -->											
		   </div><!-- /.post-content -->									
		  </div><!-- /.tr-post -->	
 </div><!-- /.tr-post -->
 @endif
@endforeach
@endif	

@if(count($blogs)==0)
<div class="alert alert-danger col-sm-12 text-center">{{__('app.no_blog_post')}}</div>
@endif
 	 