<section class="posts-2 followers">
	  <div class="container">
	   <div class="row">
	    <div class="col-lg-12">

		<ul class="nav nav-tabs" role="tablist" id="myTabs">
		  <li class="nav-item">
			<a class="nav-link {{(count($followers)>0)?'active':''}}" href="#followers" role="tab" data-toggle="tab">{{__('app.followers')}}</a>
		  </li>
		  <li class="nav-item">
			<a id="followinga" class="nav-link " href="#following" role="tab" data-toggle="tab">{{__('app.followings')}}</a>
		  </li>
		</ul>
		<br></br>
 
		<!-- Tab panes -->
		<div class="tab-content">
		  <div role="tabpanel" class="tab-pane active" id="followers">
            @foreach($followers as $follower)
           <div class="card card-profile">
              <div class="card-block">
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object rounded-circle" src="{{asset('img/users/'.$follower->image)}}" alt="">
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">
                      <a href="{{route('users.show',$follower->username)}}">{{$follower->name}}
                      </a>
                      </h4>
                    <p class="media-usermeta"><i class="fa fa-briefcase"></i> {{$follower->company}}</p>
                  </div>
                </div><!-- media -->
                @if($user->id==Auth::user()->id)
                <ul class="card-options">
                  <li><a class="dropdown-toggle" href="" data-toggle="dropdown" title="" data-original-title="View Options" id="dropdownMenuButton_{{$follower->id}}" role="button">
				            <i class="fa fa-ellipsis-v"></i>
				           </a>
                   <?php $checkIfFollowing=App\Followers::where('user_id',$follower->id)->where('follower_id',Auth::user()->id)->count(); ?>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton_{{$follower->id}}">
                    <a class="dropdown-item" href="#">{{__('app.block')}}</a>
                    @if($checkIfFollowing==0)
                    <a class="dropdown-item" onclick="followUser('{{$follower->id}}')">{{__('app.follow')}}</a>
                    @endif
                    
                  </div>
                 </li>
                </ul>
                @endif

              <div class="people-info">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="info-group">
                      <label>{{__('app.location')}}</label>
                      {{$follower->location}}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="info-group">
                      <label>{{__('app.email')}}</label>
                      {{$follower->email}}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="info-group">
                      <label>{{__('app.website')}}</label>
                      {{$follower->website}}
                    </div>
                  </div>
                </div><!-- row -->
                <div class="row">
                  <div class="col-sm-4">
                    <div class="info-group">
                      <label>{{__('app.followers')}}</label>
                      <h4>{{$follower->followerscount}}</h4>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="info-group">
                      <label>{{__('app.followings')}}</label>
                      <h4>{{$follower->followingscount}}</h4>
                    </div>
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div><!-- row -->
              </div><!-- panel-info -->
              </div><!-- card-block -->
            </div><!-- card -->	
             @endforeach

           			
		  
		  </div><!-- tabpanel -->
		  <div role="tabpanel" class="tab-pane fade" id="following">
		   @foreach($followings as $following)

                    <?php 
                      $checkIfBlocked=App\BlockedUser::where('blocked_id',$following->id)->where('user_id',Auth::user()->id)->count();
                      ?>
            <div class="card card-profile">
              <div class="card-block">
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object rounded-circle" src="{{asset('img/users/'.$following->image)}}" alt="">
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">
                      <a href="{{route('users.show',$following->username)}}">{{$following->name}}</a>
                       @if(Auth::user()->id==$user->id && $checkIfBlocked>0)
                          <i style="color:red" class="fa fa-ban" id="realblockicon_{{$following->id}}" title="blocked user">
                            
                          </i>
                       @endif
                       <i class="fa fa-ban" id="followinblockedicon_{{$following->id}}" style="display:none;">
                            
                       </i>
                      </h4>
                    <p class="media-usermeta"><i class="fa fa-briefcase"></i> {{$following->company}}</p>
                  </div>
                </div><!-- media -->
                @if($user->id==Auth::user()->id)
                <ul class="card-options">
                  <li><a class="dropdown-toggle" href="" data-toggle="dropdown" title="" data-original-title="View Options" id="followingdropdownMenuButton_{{$following->id}}" role="button">
                    <i class="fa fa-ellipsis-v"></i>
                   </a>
                   
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="followingdropdownMenuButton_{{$following->id}}">
                    @if($checkIfBlocked>0)
                    <a class="dropdown-item" id="realunblocka_{{$following->id}}" onclick="unblockUser('{{$following->id}}')">{{__('app.unblock')}}</a>
                    <a style="display:none;" id="blocka_{{$following->id}}" class="dropdown-item" onclick="blockUser('{{$following->id}}')">{{__('app.block')}}</a>
                   @else
                   <a class="dropdown-item" id="realblocka_{{$following->id}}" onclick="blockUser('{{$following->id}}')">{{__('app.block')}}</a>
                   <a style="display:none;" id="unblocka_{{$following->id}}" class="dropdown-item" onclick="unblockUser('{{$following->id}}')">{{__('app.unblock')}}</a>
                   @endif
                    
                  </div>
                 </li>
                </ul>
                @endif

              <div class="people-info">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="info-group">
                      <label>{{__('app.location')}}</label>
                      {{$following->location}}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="info-group">
                      <label>Email</label>
                      {{$following->email}}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="info-group">
                      <label>{{__('app.website')}}</label>
                      {{$following->website}}
                    </div>
                  </div>
                </div><!-- row -->
                <div class="row">
                  <div class="col-sm-4">
                    <div class="info-group">
                      <label>{{__('app.followers')}}</label>
                      <h4>{{$following->followerscount}}</h4>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="info-group">
                      <label>{{__('app.followings')}}</label>
                      <h4>{{$following->followingscount}}</h4>
                    </div>
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div><!-- row -->
              </div><!-- panel-info -->
              </div><!-- card-block -->
            </div><!-- card --> 
		  @endforeach
		  </div><!-- tabpanel -->
		</div>	   
	   
	    </div>
	   </div><!--/ row -->
	  </div><!--/ container -->
    </section><!--/ section -->		
    <script type="text/javascript">
      window.onload=function(){

        @if(count($followers)==0)
        
          $("#followinga").click()
        @endif
      }
    </script>