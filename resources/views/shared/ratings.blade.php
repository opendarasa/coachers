
<?php $ratinglimit=0; ?>
   

   <ul class=list-group>
   	@if($user->id!=Auth::user()->id)
   	<?php $checkifRated=App\Ratings::where('rated_id',$user->id)->where('rater_id',Auth::user()->id)->count(); ?>
   	  @if($checkifRated==0)
   	<li class="list-group-item text-center">
   		<a href="#rating" data-toggle="modal" class="badge badge-default">
           {{__('app.rate_writer')}}
         </a>
   	</li>
   		@endif
   	@endif

   	@if(count($ratings)>0)
   	@foreach($ratings as $rating)
   	<?php $ratinglimit++; ?>
   	   @if($ratinglimit<=5)
   	<li class="list-group-item">
   		
   		<div class="col-sm-3 pull-left">
   			<a href="{{route('users.show',$rating->username)}}"><img src="{{asset('img/users/'.$rating->image)}}" alt="user image" title="rater image" width="50" height="50"></a>
   		</div>
   		<div class="col-sm-8" style="margin-left:4%;">
   			<span style="margin-bottom:2%;">
   				{{$rating->name}}
   			</span><br>
   			<span style="display:inline-block;font-size:25px;">
   				{{number_format($rating->rating)}}
   			</span>
   			
   			<div class="rate" data-rate-value="{{$rating->rating}}" style="font-size:19px;display:inline-block;">
        
          	</div>
          	<br>
          	<p style="margin-top:2%;font-size:12px;">
          		{{$rating->comment}}
          	</p>

          	{{(new Carbon\Carbon($rating->created_at))->diffForHumans()}}

   		</div>
   	</li>
   	  @endif
   	@endforeach
   	@endif
   	@if(count($ratings)>5)
   	 <li class="list-group-item">
   	 	<a href="#raters" data-toggle="modal" style="color:gray;">
   	  	{{__('app.see_all')}}
   	  </a>
   	 </li>
   	 @endif
   	  
   	
   </ul>


<!-- ==============================================
      user raters
     =============================================== -->

     <div class="modal fade" id="raters" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{__('app.user_raters')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align:center;">
      <ul class="list-group">
      	
        @foreach($ratings as $rating)
   	<?php $ratinglimit++; ?>
   	   
   	<li class="list-group-item">
   		
   		<div class="col-sm-3 pull-left">
   			<a href="{{route('users.show',$rating->username)}}"><img src="{{asset('img/users/'.$rating->image)}}" alt="user image" title="rater image" width="80" height="80"></a>
   		</div>
   		<div class="col-sm-8" style="text-align:left;">
   			<span style="margin-bottom:2%;">
   				{{$rating->name}}
   			</span><br>
   			<span style="display:inline-block;font-size:25px;">
   				{{number_format($rating->rating)}}
   			</span>
   			
   			<div class="rate" data-rate-value="{{$rating->rating}}" style="font-size:19px;display:inline-block;">
        
          	</div>
          	<br>
          	<p style="margin-top:2%;">
          		{{$rating->comment}}
          	</p>

          	{{(new Carbon\Carbon($rating->created_at))->diffForHumans()}}

   		</div>
   	</li>
   	 
   	@endforeach
   </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('app.close')}}</button>
        
      </div>
    </div>
  </div>
</div>	
   
