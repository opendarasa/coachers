<style type="text/css">
    .loader {
  border: 1px solid #f3f3f3; /* Light grey */
  border-top: 1px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 15px;
  height: 15px;
  animation: spin 2s linear infinite;
  display:none;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<div id="list" style="">
@foreach($readers as $reader)
<div class="container" id="user_{{$reader->id}}"style="height:140px;">
    <div class="card flex-row flex-wrap">
        
        <div class="card-header border-0" style="display:inline-block;">

            <a href="{{route('users.show',$reader->username)}}">
            <div style="margin-bottom:4%;">
                <img height="50" width="50" class="thumb-lg img-circle" src="{{asset('img/users/'.$reader->image)}}" alt="profile image">
            </div>
            </a>
            <div class="col-sm-12" style="text-align:center;">
               
            </div>
            
        </div>
        <div class="card-block px-2">
            
            
                <h4 class="card-title">
                <span style="display:inline-block;">
                    <a href="{{route('users.show',$reader->username)}}">{{$reader->name}}

                    </a>
                    <small style="margin-right:2%;">
                        ({{__('app.reads')}} {{$reader->read_count}} {{__('app.times')}})
                    </small>
                    
                </span>
                 @include('shared.follow_btn')
                
               </h4>
          </div>

        <div class="w-100"></div>
        <div class="card-footer w-100 text-muted">
            {{trans('app.joined').'  '.(new Carbon\Carbon($reader->created_at))->diffForHumans()}}
        </div>
    </div>
    
</div>
@endforeach
</div>

<script type="text/javascript">

    
    function Mysort() {
    // Declare variables
    var input, filter, listDiv, span, h4, i;
    input = document.getElementById('mysearch');
    filter = input.value.toUpperCase();
    listDiv = document.getElementById("list");
    h4 = listDiv.getElementsByTagName('h4');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < h4.length; i++) {
        span = h4[i].getElementsByTagName("span")[0];
        var parentDiv=h4[i].parentNode.parentNode.parentNode;
        if (span.innerHTML.toUpperCase().indexOf(filter) > -1) {
            parentDiv.style.display = "";
        } else {
            parentDiv.style.display = "none";
        }
    }
}
</script>
