 {{csrf_field()}}
          <div class="form-group">
            {{Form::text('title',null,['class'=>'form-control','placeholder'=>trans('app.title').'*','id'=>'title'])}}
            <small style="color:red;" id="title_alert"></small>
            <!--<input placeholder="title" type="text" name="title" class="form-control" value="">-->
            
          </div>
           <div class="form-group">
            {{Form::textarea('subtitle',null,['class'=>'form-control','placeholder'=>trans('app.subtitle').'*','id'=>'subtitle'])}}
            <small style="color:red;" id="subtitle_alert"></small>
            <!--<input placeholder="subtitle" type="text" name="subtitle" class="form-control" value="">-->
          </div>
          <div class="form-group">
            {{Form::label('type',trans('app.select_type').'*')}}
            {{Form::select('cat_id',trans('app.blog_types'),'',['class'=>'form-control','id'=>'type'])}}
            <small style="color:red;" id="type_alert"></small>
            <!--<select class="form-control" name="cat_id" value="">
              <option value="1">{{trans('app.webdesign')}}
              </option>
             <option value="2">{{trans('app.uidesign')}}
             </option>
             <option value="3">{{trans('app.seo')}}
             </option>
             
            </select>-->
          </div>
          <div class="form-group">
            <!--<label for="editor">Blog content</label>-->
            {{Form::label('editor',trans('app.content').'*')}}
            <small style="color:red;" id="editor_alert"></small>
           <!-- <textarea name="content"  class="form-control" id="editor">
             
            </textarea>-->
            {{Form::textarea('content',null,['class'=>'form-control','id'=>'editor','height'=>'1200'])}}
          </div>
          <div class="form-group">
            
            {{Form::label('myfile',trans('app.blog_image').'*')}}
            <small style="color:red;" id="myfile_alert"></small>
             {{Form::file('image',['id'=>'myfile'])}}

            <!--<input id="input-id" data-show-upload="false" type="file"  class="file" name="image">-->
            <div id="kartik-file-errors"></div>
          </div>
          <div class="form-group">
             
             {{Form::text('tags',null,['class'=>'form-control','placeholder'=>trans('app.tags_input_placeholder').'*','id'=>'tags'])}}
             <small style="color:red;" id="tags_alert"></small>
            <!--<input placeholder="tags, separate by [,]" type="text" name="tags" class="form-control" value="">-->
          </div>

    
          