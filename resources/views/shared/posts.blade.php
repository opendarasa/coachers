 <!-- ==============================================
     Posts Section
     =============================================== -->		  
	  
	 <section class="posts-2">
	  <div class="container">
	   
	  
	   <div class="row">
	   	
	   
        
		 	<?php $n=0; ?>
		 @if(count($blogs)>0)
		 @foreach($blogs as $blog)

		   @if($blog->status==1)
		 <?php $n++; ?>
		 <?php $comment_count=App\Comments::where('blog_id',$blog->id)->count(); ?>
		 <?php 
  $checkIfBlocked=App\BlockedUser::where('blocked_id',Auth::user()->id)->where('user_id',$blog->user_id)->count();
  $checkIfBlocked2=App\BlockedUser::where('blocked_id',$blog->user_id)->where('user_id',Auth::user()->id)->count();
  ?>
		@if($checkIfBlocked==0 && $checkIfBlocked2==0)
		  <div class="card col-sm-4">
		   <a href="{{route('blog.show',$blog->slug)}}">
		    <img class="card-img-top img-fluid" src="{{asset('img/blog/'.$blog->image)}}" alt="...">
		   </a>	
		    <div class="card-block">
		     <h4 class="card-title"><a href="{{route('blog.show',$blog->slug)}}">{{$blog->title}}</a></h4>
			 <h6 class="card-text"><a href="{{url('blogs/'.trans('app.blog_types.'.$blog->cat_id))}}">{{trans('app.blog_types.'.$blog->cat_id)}}</a>,&nbsp; <small>{{(new Carbon\Carbon($blog->created_at))->diffForHumans()}}</small></h6>
		     <p class="card-text">{{mb_substr($blog->subtitle,0,30)}}</p>
		    </div>
		    <div class="card-footer">
		     <hr/>
		     <ul class="author-2">
			  <li><a href="#"><img class="" src="{{asset('img/users/'.$blog->userimage)}}" alt="profile image"/> &nbsp; <span> {{$blog->postername}}</span></a></li>
		     </ul><!-- /.bottom_data -->
		     <ul class="bottom_data pull-right hidden-xs-down">
			  <li><i class="fa fa-comment-o"></i>{{$comment_count}}</li>
			  <?php $class='' ?>
			  <li>@include('shared.clap')</li>
			  <li><i class="fa fa-align-left"></i>{{rand(1,10)}} {{__('app.min_read')}}</li>
			  @if($blog->user_id==Auth::user()->id&&$blog->status==0)
			  <li><i  class="fa fa-lock"></i>&nbsp; {{__('app.private')}}</li>

			  @endif
			  @if($blog->user_id==$user->id)
			  <li>
			  	<a  href="{{route('blog.edit',$blog->slug)}}">
			  	<i  class="fa fa-pencil"></i>
			    </a>&nbsp;
			  </li>
			  <li><a id="deletebtn" onclick="deleteFunction('{{$blog->id}}')">
			  	<i  class="fa fa-trash"></i>
			     </a>&nbsp;
			  </a>
			  </li>
			  <form style="display:none;" id="deleteform_{{$blog->id}}" action="{{route('blog.destroy',[$blog->id])}}" method="POST">
               @method('DELETE')
               @csrf  
              </form>       
			  @endif

		     </ul><!-- /.bottom_data --> 
		    </div>
		  </div><!-- /.card -->
		 @endif
		  @endif
		  @endforeach
		  @endif

		  <div class=" col-sm-12 alert alert-danger text-center" style="text-align:center;">
		  	  {{__('app.no_result_found')}}
		  </div>
		 
		  </div>
        
	  </div><!-- /.container -->
     </section><!-- /.section -->
	 