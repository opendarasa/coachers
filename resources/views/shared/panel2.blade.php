  <div class="col-lg-3">

         <div class="tbl">
		  <div class="tbl-row">
		   <div class="tbl-cell">
		   	<a style="color:#00b495" href="{{url('followers/'.$user->username)}}">
			<span class="number">
				
					{{$followers->count()}}

			</span>
			{{__('app.followers')}}
		    </a>
		   </div>
		   <div class="tbl-cell">
		   	<a style="color:#00b495" href="{{url('followers/'.$user->username)}}">
			<span class="number">{{$followings->count()}}</span>{{__('app.followings')}}
		    </a>
		   </div>
		  </div>
		 </div>		

         <div class="card about-user">
		  <div class="card-body">
		   <div class="card-block">
			<h4 class="card-title info">{{__('app.about')}}</h4>
			<p class="card-text">{{$user->description}}</p>
            <div class="text-left">
             <p class="card-text"><strong>{{__('app.full_name')}} :</strong> <span class="m-l-15">
             	
             	{{$user->name}}
             	
             	
             </span></p>
             <p class="card-text"><strong>{{__('app.email')}} :</strong><span class="m-l-15"><br>{{mb_substr($user->email,0,60)}}</span></p>
             <p class="card-text"><strong>{{__('app.website')}} :</strong> <span class="m-l-15"><br>
             	<input style="border-radius:4px;width:80%;"  id="copytarget" type="text" value="{{config('app.url').'/users/'.$user->username}}">
			 	<button id="copybtn" style="size:20px;">
			 		<i class="fa fa-files-o">
			 			
			 		</i>
			 	</button></span></p>
             <p class="card-text"><strong>{{__('app.location')}} :</strong> <span class="m-l-15">{{$user->location}}</span></p>
            </div>			
		   </div>
		  </div>
	     </div>		
	     
	     
	     <div class="card about-user">
		  <div class="card-body">
		   <div class="card-block">
			<h4 class="card-title info">{{__('app.add_skills')}}</h4>
			
	          
            <div>
				@if($user->id==Auth::user()->id)
			
				{{Form::open( array( 
	             'class' => 'form', 
	             'files' => true,
	             'id'=>'saveskill'
	             ))}}
	             <div class="form-group">
	             	{{Form::text('skill',null,['class'=>'form-control','placeholder'=>trans('app.skill_name'),'id'=>'skill_name'])}}
	             	<small id="skill_name_alert" style="color:red"></small>
	             </div>
	             <div class="form-group">
	             	{{Form::label('level',trans('app.your_skill_level'))}}
	             	{{Form::select('level',trans('app.skill_levels'),'',['class'=>'form-control','id'=>'level'])}}
	             </div>
	             <div class="form-group">
	             	{{Form::button(trans('app.save'),['class'=>'btn btn-primary pull-right','id'=>'submitskill'])}}
	             	<div class="loader" id="skillloader"></div>
	             </div>
	             {{Form::close()}}

	             {{Form::open( array( 
	             'class' => 'form', 
	             'files' => true,
	             'id'=>'editskill'
	             ))}}
	             <div class="form-group">
	             	{{Form::text('skill',null,['class'=>'form-control','placeholder'=>trans('app.skill_name'),'id'=>'edit_skill_name'])}}
	             	<small id="edit_skill_name_alert" style="color:red"></small>
	             </div>
	             <div class="form-group">
	             	{{Form::label('editlevel',trans('app.your_skill_level'))}}
	             	{{Form::select('level',trans('app.skill_levels'),'',['class'=>'form-control','id'=>'editlevel'])}}
	             </div>
	             {{Form::hidden('id',null,['id'=>'skill_id'])}}

	             <div class="form-group">
	             	{{Form::button(trans('app.save'),['class'=>'btn btn-primary pull-right','id'=>'submiteditskill'])}}
	             	<div class="loader" id="skillloader2"></div>
	             </div>
	             {{Form::close()}}
	             @endif
			</div>

			<div class="row" style="margin-top:30%;">
				
				
				
				 @if(count($skills)>0)
            <div class="text-left" style="margin-top:10%;" >
            	
            	
            	@foreach($skills as $skill)
             	  <span  class="badge badge-default " style="color:white;margin-bottom:4%;">
             	  	{{$skill->skill}}
             	  	@if(Auth::user()->id==$user->id)
             	  	<a  onclick="editSkill('{{$skill->id}}')">
             	  		<i class="fa fa-pencil">
             	  		
             	  	    </i>
             	  	</a>
             	  	<a  onclick="deleteSkill('{{$skill->id}}')">
             	  		<i class="fa fa-trash">
             	  		
             	  	    </i>
             	  	</a>
             	  	<input type="hidden" name="" id="skill_{{$skill->id}}" value="{{$skill->skill}}">
             	  	<input type="hidden" name="" id="level_{{$skill->id}}" value="{{$skill->level}}">
             	  	@endif
             	  	
             	  	
             	  </span>
             	 @endforeach
                 
            </div>	
            @endif
				
			</div>			
		   </div>
		  </div>
	     </div>	
	     
		
		
		</div>

