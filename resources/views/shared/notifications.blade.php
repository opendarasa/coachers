

@foreach(Auth::user()->unreadNotifications as $unreadnotif)
<div class="container" id="desc">
    <div class="card flex-row flex-wrap">
        
        <div class="card-block px-2">
            <a style="color:orange" href="{{$unreadnotif->data['url']}}">
                <p class="card-text">{{$unreadnotif->data['data']}}</p>
            </a>
          </div>

        <div class="w-100"></div>
        <div class="card-footer w-100 text-muted">
            {{(new Carbon\Carbon($unreadnotif->created_at))->diffForHumans()}}
        </div>
    </div>
    <br>
</div>
@endforeach
@foreach(Auth::user()->notifications as $notif)
@if($notif->read_at!=null)
<div class="container" id="desc">
    <div class="card flex-row flex-wrap">
        
        <div class="card-block px-2">
            <a style="color:black" href="{{$notif->data['url']}}"><p class="card-text">{{$notif->data['data']}}</p></a>
          </div>

        <div class="w-100"></div>
        <div class="card-footer w-100 text-muted">
            {{(new Carbon\Carbon($notif->created_at))->diffForHumans()}}
        </div>
    </div>
    <br>
</div>
@endif
@endforeach



