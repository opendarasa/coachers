<?php
               
  $clapped=App\Claps::where('blogid',$blog->id)->where('uid',Auth::user()->id)->count();
              
 ?>
 @if($clapped >0)
<a onclick="unclap('{{$blog->id}}')" id="unclap_{{$blog->id}}"  class="{{$class}}">
	<i class="fa fa-heart" id="unclapI_{{$blog->id}}">&nbsp;&nbsp;{{$blog->claps}}</i>
</a>
<a style="display:none;" onclick="clap('{{$blog->id}}')" id="clap_{{$blog->id}}"  class="{{$class}}">
	<i class="fa fa-heart-o" id="clapI_{{$blog->id}}">&nbsp;&nbsp;{{$blog->claps}}</i>
</a>
@else
<a id="clap_{{$blog->id}}" onclick="clap('{{$blog->id}}')" class="{{$class}}">
	<i class="fa fa-heart-o" id="clapI_{{$blog->id}}">&nbsp;&nbsp;{{$blog->claps}}</i>
</a>
<a style="display:none;" onclick="unclap('{{$blog->id}}')" id="unclap_{{$blog->id}}"  class="{{$class}}">
	<i class="fa fa-heart" id="unclapI_{{$blog->id}}">&nbsp;&nbsp;{{$blog->claps}}</i>
</a>
@endif
<input type="hidden" name="" value="{{$blog->claps}}" id="blogclaps_{{$blog->id}}">
@if(isset($readers)&&count($readers)>0)
<a data-toggle="modal" data-target="#exampleModalCenter" >
	<i class="fa fa-eye">
		
	</i>&nbsp;
	{{count($readers)}}
	&nbsp;
	{{__('app.views')}}
</a>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{__('app.readers')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @include('shared.readers')
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
@endif