<div class="col-sm-12" style="margin-bottom:4%;">
    
    <input class="col-xs-12" style="margin-bottom:3%;width:80%; display: inline-block;" class="inline" onkeyup="Mysort3();"  type="text" name="" id="mysearch3" placeholder="{{trans('app.enter_username')}}">
 
</div>
<div id="list3" style="">
@foreach($followings as $user)
<div class="container" id="desc">
    <div class="card flex-row flex-wrap">
        <div class="card-header border-0">
            <img class="profileimage" src="{{asset('img/users/'.$user->image)}}" alt="">
            <span>
                    @if(Auth::check())

                    <?php 
                     $checkIfFollow=App\Followers::where('follower_id',Auth::user()->id)->where('user_id',$user->id)->count();
                     $checkIfFollowing=App\Followers::where('user_id',Auth::user()->id)->where('follower_id',$user->id)->count();
                    ?>
                      
                      @if($user->id!=Auth::user()->id)
                          @if($checkIfFollow==0)
                      
                    <a style="margin-left:2%;" href="{{url('follow/'.$user->id)}}" class="badge badge-dark pull-right">
                        <i class="fa fa-plus">
                            
                        </i>
                        {{__('app.follow')}}
                    </a>
                          @else
                        <a style="margin-left:2%;"class="badge badge-success pull-right">
                        
                        {{__('app.following')}}
                       </a>

                          @endif
                       @endif
                        @if($checkIfFollowing && $user->id!=Auth::user()->id)
                       {{__('app.is_following_you')}}
                       @endif
                    @else
                    <a style="margin-left:2%;" href="{{url('follow/'.$user->id)}}" class="badge badge-dark pull-right">
                        <i class="fa fa-plus">
                            
                        </i>
                        {{__('app.follow')}}
                    </a>

                      
                    @endif
                </span>
        </div>
        <div class="card-block px-2">
            
                <h4 class="card-title">
                <span>
                    {{$user->name}} 
                </span>
                
               
               </h4>
            
            
            <span class="text-muted">{{trans('app.worked_at')}}: {{$user->company}}</span>
            <p class="card-text">{{mb_substr(strip_tags($user->description),0,100)}}...</p>
            <p class="card-text">
                {{(isset($user->experience))?$user->experience:''}} {{trans('app.years_experience')}}
            </p>
            <p>
                
                <?php $skills=explode(",",$user->skills);
                 
                 ?>
                @if(count($skills)>1)
                <h6>{{trans('app.skills')}}</h6>
                @foreach($skills as $skill)
                <a href="#" class="badge badge-dark badge-pill">{{$skill}}
                </a>
                @endforeach

                @else
                <h6>{{trans('app.no_skills-earned_yet')}}</h6>
                @endif

            </p>
            <p>
                <i class="fa fa-map-marker">
                    {{$user->location}}
                </i>
            </p>
            <a  href="{{route('users.show',$user->username)}}" class="btn btn-info">{{trans('app.view_user')}}</a>
            <br><br>
          </div>

        <div class="w-100"></div>
        <div class="card-footer w-100 text-muted">
            {{trans('app.joined').'  '.(new Carbon\Carbon($user->created_at))->diffForHumans()}}
        </div>
    </div>
    <br>
</div>
@endforeach
</div>

<script type="text/javascript">
    function Mysort3() {
    // Declare variables
    var input, filter, listDiv, span, h4, i;
    input = document.getElementById('mysearch3');
    filter = input.value.toUpperCase();
    listDiv = document.getElementById("list3");
    h4 = listDiv.getElementsByTagName('h4');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < h4.length; i++) {
        span = h4[i].getElementsByTagName("span")[0];
        var parentDiv=h4[i].parentNode.parentNode.parentNode;
        if (span.innerHTML.toUpperCase().indexOf(filter) > -1) {
            parentDiv.style.display = "";
        } else {
            parentDiv.style.display = "none";
        }
    }
}
</script>
