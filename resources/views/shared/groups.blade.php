<section class="box-typical">
			<header class="box-typical-header-sm">Start-ups you may like
				<a href="{{route('groups.create')}}" class="badge badge-default pull-right">
				<i class="fa fa-plus">
					
				</i>
			{{__('app.add_startup')}}
		    </a>
			</header>
			<div class="people-rel-list row">
				<ul class="people-rel-list-photos">
					<li><a href="#"><img src="img/users/1.jpg" alt=""></a></li>
					<li><a href="#"><img src="img/users/3.jpg" alt=""></a></li>
					<li><a href="#"><img src="img/users/4.jpg" alt=""></a></li>
					<li><a href="#"><img src="img/users/5.jpg" alt=""></a></li>
					<li><a href="#"><img src="img/users/6.jpg" alt=""></a></li>
					<li><a href="#"><img src="img/users/7.jpg" alt=""></a></li>
					<li><a href="#"><img src="img/users/8.jpg" alt=""></a></li>
					<li><a href="#"><img src="img/users/9.jpg" alt=""></a></li>
					<li><a href="#"><img src="img/users/10.jpg" alt=""></a></li>
					<li><a href="#"><img src="img/users/11.jpg" alt=""></a></li>
					<li><a href="#"><img src="img/users/12.jpg" alt=""></a></li>
					
				</ul>
				
				<form class="site-header-search">
					<input placeholder="Search for people" type="text">
					<button type="submit">
						<span class="fa fa-search"></span>
					</button>
					<div class="overlay"></div>
				</form>


			
					
			</div>

			
		</section>	