
<div class="row">

@foreach($works as $work)
         <div class="col-md-{{(count($works)==1)?12:4}} col-sm-{{(count($works)==1)?12:6}} portfolio-item" id="desc">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal{{$work->id}}">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="{{asset('img/portfolio/'.$work->image)}}" alt="">
            </a>
            <div class="portfolio-caption">
              @if(count($works)<=2)
              <h5>{{(strlen($work->title)>30)?mb_substr(strip_tags($work->title),0,30).'..':$work->title}}
              </h5>
              @else
              <h5>{{(strlen($work->title)>20)?mb_substr(strip_tags($work->title),0,20).'..':$work->title}}
              </h5>
              @endif
              <p class="text-muted">{{config('category.category_'.$work->cat_id)}}</p>
              <p>
                <?php $tags=explode(",",$work->tags); ?>
            @foreach($tags as $tag)
            {{'#'.$tag}}
            @endforeach
              </p>
              <p>
                @if(Auth::check() && Auth::user()->id==$work->user_id)
                 <a href="{{route('portofolio.edit',$work->slug)}}">
                  <i class="fa fa-pencil"></i>
                </a>
                @endif
                <i class="fa fa-clock-o">
                  
                </i>
                {{(new Carbon\Carbon($work->created_at))->diffForHumans()}}</p>
            </div>
          </div>
@endforeach

@if(Auth::check() && isset($user) && Auth::user()->id==$user->id)
@foreach($unpublishedWorks as $work)
         <div class="col-md-{{(count($unpublishedWorks)==1)?12:4}} col-sm-{{(count($unpublishedWorks)==1)?12:6}} portfolio-item" id="desc">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal{{$work->id}}">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="{{asset('img/portfolio/'.$work->image)}}" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>{{(strlen($work->title)>20)?mb_substr(strip_tags($work->title),0,20).'..':$work->title}}
              </h4>
              <p class="text-muted">{{config('category.category_'.$work->cat_id)}}</p>
              <p>
                <?php $tags=explode(",",$work->tags); ?>
            @foreach($tags as $tag)
            {{'#'.$tag}}
            @endforeach
              </p>
              <p>
                 <a href="{{route('portofolio.edit',$work->slug)}}">
                  <i class="fa fa-pencil"></i>
                </a>
                <i class="fa fa-lock" style="color:red;"></i>
                <i class="fa fa-clock-o">
                  
                </i>
                {{(new Carbon\Carbon($work->created_at))->diffForHumans()}}</p>
            </div>
          </div>
@endforeach

@endif


@if(isset($asc_works))
@foreach($asc_works as $work)
         <div class="col-md-{{(count($asc_works)==1)?12:4}} col-sm-{{(count($asc_works)==1)?12:6}} portfolio-item" id="asc" style="display:none;">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal{{$work->id}}">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="{{asset('img/portfolio/'.$work->image)}}" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>{{mb_substr(strip_tags($work->title),0,20)}}...
              </h4>
              <p class="text-muted">

                
                {{config('category.category_'.$work->cat_id)}}</p>
              <p>
                <?php $tags=explode(",",$work->tags); ?>
            @foreach($tags as $tag)
            {{'#'.$tag}}
            @endforeach
              </p>
              <p>
                @if(Auth::check() && Auth::user()->id==$work->user_id)
                 <a href="{{route('portofolio.edit',$work->slug)}}">
                  <i class="fa fa-pencil"></i>
                </a>
                @endif
                <i class="fa fa-clock-o">
                  
                </i>
                {{(new Carbon\Carbon($work->created_at))->diffForHumans()}}</p>
            </div>
          </div>
@endforeach

@endif
@if(count($works)>6)
<div style="text-align:center;">
    <a href="{{url('portofolio')}}" class="btn btn-primary">{{trans('app.see_more')}}</a>
</div>
@endif
</div>