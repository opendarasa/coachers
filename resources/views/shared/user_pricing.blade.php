
 <!--Portfolio-->
    <section class="bg-light" >
      <div class="container">
        <div class="row">
          <p>
            <a href="#user_pricing_modal" class="btn btn-info pull-right" data-toggle="modal">
                <i class="fa fa-pencil">
                  {{trans('app.edit_pricing')}}  
                </i>
            </a>
          </p>
            

    <p>      
    <div class=" col-sm-12 col-lg-12 text-center">
    <h2 class="section-heading text-uppercase">{{trans('app.pricing')}}</h2>

    </div>
    </div>
    <div class="row">
    <div class="col-lg-4">
    <div class="card" style="width: 18rem;">
    <div class="card-header">
    {{trans('app.mobile_dev')}}
    </div>
    <ul class="list-group list-group-flush">
    <li class="list-group-item">50$/hr</li>
    <li class="list-group-item">{{trans('app.delivery_in')}} : 3 {{trans('app.weeks')}}</li>
    <li class="list-group-item">
    <a id="hire_mobile" href="{{url('new-ui-case/'.$user->id)}}" class="btn btn-danger" role="button">{{trans('app.submit_project')}}</a>
    </li>
    </ul>
    </div>

    </div>
    <div class="col-lg-4">
    <div class="card" style="width: 18rem;">
    <div class="card-header">
    {{trans('app.web_design')}}
    </div>
    <ul class="list-group list-group-flush">
    <li class="list-group-item">120$/hr</li>
    <li class="list-group-item">{{trans('app.delivery_in')}} : 3 {{trans('app.weeks')}}</li>
    <li class="list-group-item">
    <a id="hire_web" href="{{url('new-web-case/'.$user->id)}}" class="btn btn-danger" role="button">{{trans('app.submit_project')}}</a>
    </li>
    </ul>
    </div>

    </div>
    <div class="col-lg-4">
    <div class="card" style="width: 18rem;">
    <div class="card-header">
    {{trans('app.seo')}}
    </div>
    <ul class="list-group list-group-flush">
    <li class="list-group-item">20$/hr</li>
    <li class="list-group-item">{{trans('app.delivery_in')}} : 3 {{trans('app.weeks')}}</li>
    <li class="list-group-item">
    <a id="hire_seo" href="{{url('new-seo-case/'.$user->id)}}" class="btn btn-danger" role="button">{{trans('app.submit_project')}}</a>
    </li>
    </ul>
    </div>

    </div>

    
      </div>
    </p>
  </div>
    </section>
<div class="portfolio-modal modal fade" id="user_pricing_modal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">{{trans('app.edit_pricing')}}</h2>
                  {{Form::open(['url' => 'user_edit_pricing'])}}
                  
                  <h4>{{trans('app.mobile_pricing')}}</h4>
                  
                    <div class="form-group">
                    {{Form::label('webpricing',trans('app.add_web_dev_price',['class'=>'form-control']))}}
                    {{Form::number('webprice', null, ['class'=>'form-control'])}}
                  </div>
                  
                  
                    <div class="form-group">
                    {{Form::label('webtime',trans('app.add_web_dev_time',['class'=>'form-control']))}}
                    {{Form::select('webtime',trans('app.project_time'),null,['class'=>'form-control'])}}
                  </div>
                  
                  <hr>
                  <h4>{{trans('app.mobile_pricing')}}</h4>
                  
                    <div class="form-group">
                    {{Form::label('mobilepricing',trans('app.add_mobile_dev_price',['class'=>'form-control']))}}
                    {{Form::number('mobileprice', null, ['class'=>'form-control'])}}
                  </div>
                  
                  
                    <div class="form-group">
                    {{Form::label('mobiletime',trans('app.add_mobile_dev_time',['class'=>'form-control']))}}
                    {{Form::select('mobiletime',trans('app.project_time'),null,['class'=>'form-control'])}}
                  </div>
                  
                  <hr>
                  <h4>{{trans('app.seo_pricing')}}</h4>
                  
                    <div class="form-group">
                    {{Form::label('seopricing',trans('app.add_seo_dev_price',['class'=>'form-control']))}}
                    {{Form::number('seoprice', null, ['class'=>'form-control'])}}
                  </div>
                  
                    <div class="form-group">
                    {{Form::label('seotime',trans('app.add_seo_dev_time',['class'=>'form-control']))}}
                    {{Form::select('seotime',trans('app.project_time'),null,['class'=>'form-control'])}}
                  </div>
                  

                    {{Form::submit(trans('app.send'),['class'=>'btn btn-info pull-right'])}}

                   {{Form::close()}}
                 
              
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    {{trans('app.close')}}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


     <div class="portfolio-modal modal fade" id="pricing" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="close-modal" data-dismiss="modal">
    <div class="lr">
    <div class="rl"></div>
    </div>
    </div>
    <div class="container">
    <div class="row">
    <div class="col-lg-12 mx-auto">
    <div class="modal-body">
    <!-- Project Details Go Here -->
    <h2 class="text-uppercase">{{trans('app.select_project_type')}}</h2>
    <p class="item-intro text-muted">{{trans('app.reminder_about_pricing')}} </p>


    <p>
    <section id="about">
    <div class="container">
    <div class="row">
    <div class=" col-sm-12 col-lg-12 text-center">
    <h2 class="section-heading text-uppercase">{{trans('app.pricing')}}</h2>

    </div>
    </div>
    <div class="row">
    <div class="col-lg-4">
    <div class="card" style="width: 18rem;">
    <div class="card-header">
    {{trans('app.mobile_dev')}}
    </div>
    <ul class="list-group list-group-flush">
    <li class="list-group-item">50$/hr</li>
    <li class="list-group-item">{{trans('app.delivery_in')}} : 3 {{trans('app.weeks')}}</li>
    <li class="list-group-item">
    <a id="hire_mobile" href="{{url('new-ui-case/'.$user->id)}}" class="btn btn-danger" role="button">{{trans('app.submit_project')}}</a>
    </li>
    </ul>
    </div>

    </div>
    <div class="col-lg-4">
    <div class="card" style="width: 18rem;">
    <div class="card-header">
    {{trans('app.web_design')}}
    </div>
    <ul class="list-group list-group-flush">
    <li class="list-group-item">120$/hr</li>
    <li class="list-group-item">{{trans('app.delivery_in')}} : 3 {{trans('app.weeks')}}</li>
    <li class="list-group-item">
    <a id="hire_web" href="{{url('new-web-case/'.$user->id)}}" class="btn btn-danger" role="button">{{trans('app.submit_project')}}</a>
    </li>
    </ul>
    </div>

    </div>
    <div class="col-lg-4">
    <div class="card" style="width: 18rem;">
    <div class="card-header">
    {{trans('app.seo')}}
    </div>
    <ul class="list-group list-group-flush">
    <li class="list-group-item">20$/hr</li>
    <li class="list-group-item">{{trans('app.delivery_in')}} : 3 {{trans('app.weeks')}}</li>
    <li class="list-group-item">
    <a id="hire_seo" href="{{url('new-seo-case/'.$user->id)}}" class="btn btn-danger" role="button">{{trans('app.submit_project')}}</a>
    </li>
    </ul>
    </div>

    </div>

    </div>
    </section>
    </p>

    <button class="btn btn-primary" data-dismiss="modal" type="button">
    <i class="fa fa-times"></i>
    {{trans('app.close')}}</button>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>