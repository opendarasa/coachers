<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
  <link rel="icon" type="image/png" href="{{asset('Login_template/images/icons/favicon.ico')}}"/>
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('Login_template/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('Login_template/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('Login_template/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('Login_template/fonts/iconic/css/material-design-iconic-font.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('Login_template/vendor/animate/animate.css')}}">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="{{asset('Login_template/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('Login_template/vendor/animsition/css/animsition.min.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('Login_template/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="{{asset('Login_template/vendor/daterangepicker/daterangepicker.css')}}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('Login_template/css/util.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('Login_template/css/main.css')}}">
<!--===============================================================================================-->
</head>
<body style="background-color: #999999;">
  
  <div class="limiter">
    <div class="container-login100">
      <div class="login100-more" style="background-image: url('{{asset('Login_template/images/bg-01.jpg')}}');"></div>

      <div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">

        <!-- signin form -->
        <form class="login100-form" id="signin" method="post" action="{{route('login')}}">
          {{csrf_field()}}
           <span class="login100-form-title p-b-59">
            {{__('app.signin')}}
          </span>
           <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
            <span class="label-input100">{{__('app.email')}}</span>
            <input class="input100" type="text" name="email" placeholder="Email addess..." required>
            <span class="focus-input100"></span>
          </div>

          <div class="wrap-input100 validate-input" data-validate = "Password is required">
            <span class="label-input100">{{__('app.password')}}</span>
            <input class="input100" type="password" name="password" placeholder="*************" required>
            <span class="focus-input100"></span>
          </div>

           <div class="container-login100-form-btn">
            <div class="wrap-login100-form-btn">
              <div class="login100-form-bgbtn"></div>
              <button onclick="submitSignin()"   class="login100-form-btn">
                {{__('app.signin')}}
              </button>
            </div>

            <button type="button" onclick="showSignup()"  class="dis-block txt3 hov1 p-r-30 p-t-10 p-b-10 p-l-30">
              {{__('app.signup')}}
              <i class="fa fa-long-arrow-right m-l-5"></i>
            </button>
          </div>

          <div class="container-login100-form-btn " style="margin-top:2%;">


            
                <a style="float:right;margin-left:51%;" class="dis-block txt3 hov1 p-r-30 p-t-10 p-b-10 p-l-30" href="{{ route('password.request') }}">
                  {{__('app.forgot_password')}}
                </a>
              
              
            
            
          </div>
          
        </form>
        @if($errors->any())
          <div class="alert alert-danger" >
            {{$errors->first()}}
          </div>
        @endif
        <!-- signup form -->
        <form method="post" action="{{ route('users.store') }}" class="login100-form validate-form" id="signup" style="display:none;">
          {{csrf_field()}}
          <span class="login100-form-title p-b-59">
            {{__('app.signup')}}
          </span>

          <div class="wrap-input100 validate-input" data-validate="Name is required">
            <span class="label-input100">{{__('app.full_name')}}</span>
            <input class="input100" type="text" name="name" placeholder="Name...">
            <span class="focus-input100"></span>
          </div>

          <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
            <span class="label-input100">{{__('app.email')}}</span>
            <input class="input100" type="text" name="email" placeholder="Email addess...">
            <span class="focus-input100"></span>
          </div>

          <div class="wrap-input100 validate-input" data-validate="Username is required">
            <span class="label-input100">{{__('app.username')}}
              <small style="color:red;" id="username_alert"></small>
            </span>
            <input class="input100" type="text" name="username" placeholder="Username...">
            <span class="focus-input100"></span>
          </div>

           

          <div class="wrap-input100 validate-input" data-validate = "Password is required">
            <span class="label-input100">{{__('app.password')}}
              <small style="color:red;" id="password_alert"></small>
            </span>
            <input class="input100" type="password" name="password" placeholder="*************" id="password">
            <span class="focus-input100"></span>
          </div>

          <div class="wrap-input100 validate-input" data-validate = "Repeat Password is required">
            <span class="label-input100">{{__('app.password_confirm')}}
              <small style="color:red;" id="passwordconfirm_alert"></small>
            </span>
            <input class="input100" type="password" placeholder="*************" id="passwordconfirm" name="password_confirmation">
            <span class="focus-input100"></span>
          </div>

          <div class="flex-m w-full p-b-33">
            <div class="contact100-form-checkbox">
              <input class="input-checkbox100" id="ckb1" type="checkbox" name="agree">
              <label class="label-checkbox100" for="ckb1">
                <span class="txt1">
                  I agree to the
                  <a href="#" class="txt2 hov1">
                    Terms of User
                  </a>
                  <small style="color:red;" id="agreealert"></small>
                </span>
              </label>
            </div>

            
          </div>

          <div class="container-login100-form-btn">
            <div class="wrap-login100-form-btn">
              <div class="login100-form-bgbtn"></div>
              <button  type="submit" class="login100-form-btn">
                {{__('app.signup')}}
              </button>
            </div>

            <a role="button" onclick="showSignin()"class="dis-block txt3 hov1 p-r-30 p-t-10 p-b-10 p-l-30">
              {{__('app.signin')}}
              <i class="fa fa-long-arrow-right m-l-5"></i>
            </a>
          </div>
        </form>
      </div>
    </div>
  </div>
  
<!--===============================================================================================-->
  <script src="{{asset('Login_template/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
  <script src="{{asset('Login_template/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
  <script src="{{asset('Login_template/vendor/bootstrap/js/popper.js')}}"></script>
  <script src="{{asset('Login_template/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
  <script src="{{asset('Login_template/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
  <script src="{{asset('Login_template/vendor/daterangepicker/moment.min.js')}}"></script>
  <script src="{{asset('Login_template/vendor/daterangepicker/daterangepicker.js')}}"></script>
<!--===============================================================================================-->
  <script src="{{asset('Login_template/vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
  
@include('layouts.sign_up_validate')
<script type="text/javascript">
  window.onload=function(){
    var view=localStorage.getItem('view');
    if(view!=undefined){
      if (view=='signup') {
         $("#signin").css({display:"none"});
      }else{
        $("#signup").css({display:"none"});
      }
      $("#"+view).css({display:"block"});
    }
  }
  function showSignin() {
    localStorage.setItem('view','signin');
    $("#signup").hide(400)
     $("#signin").show(400)
  }

  function showSignup() {
    $("#signin").hide(400);
     $("#signup").show(400);
     localStorage.setItem('view','signup');
  }
 
 
   function submitSignin() {

     $("#signin").submit(function(){

    var check=true;
    var email=$("#signinemail").val();
    var password=$("#signinpass").val();
    if(email==''){
      return false;
    }

    if(password==''){
      return false;
    }

     });
   }
</script>

</body>
</html>