@extends('layouts.newcase-template')

@section('meta')
@include('shared.meta')
@endsection
@section('content')

      <div class="container">
        
          <div class="col-sm-12 text-center">
            <h2 style="display: inline-block;" class="section-heading text-uppercase">{{trans('app.new_work')}}</h2>
             @if($work->status==0)
            <a style="display: inline-block;margin-left:2%;margin-bottom:3%;" href="{{action('HomeController@publishWork',$work->slug)}}" class="btn btn-danger">
              {{trans('app.publish')}}
            </a>
            @else
             <a style="display: inline-block;margin-left:2%;margin-bottom:3%;" href="{{action('HomeController@unpublishWork',$work->slug)}}" class="btn btn-warning">
              {{trans('app.unpublish')}}
            </a>
            @endif
            <a style="display: inline-block;margin-left:2%;margin-bottom:3%;" href="#previewWork" data-toggle="modal" class="btn btn-info">
              {{trans('app.preview')}}
            </a>
          </div>
        
        
      <div class="col-sm-12">
        @if($errors->any())
        <div class="alert alert-danger">
          {{$errors->first()}}
        </div>
        @endif
           {{Form::model($work, [
         'method' => 'PATCH',
         'route' => ['portofolio.update', $work->slug],
         'files'=>'true',
         ]) }}
          @include('shared.shared_form')
          <div class="form-group">
               {{Form::text('client',null,['class'=>'form-control','placeholder'=>trans('app.client_name')])}}
             <!--<input type="text" name="client" class="form-control" placeholder="Client name">-->
          </div>
          <div class="form-group">
            {{Form::submit(trans('app.save'),['class'=>'btn btn-info pull-right'])}}
            <!--<button type="submit" class="btn btn-info pull-right" name="save">
              Save
            </button>-->
            
          </div>
        {{Form::close()}}
      </div>
        
        
      </div>
    
  @include('shared.preview_work')
@endsection