<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portofolio extends Model
{
    //
    protected $fillable=['title','subtitle','content','image','cat_id','client','tags','user_id','slug'];
}
