<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reads extends Model
{
    //

    protected $fillable=['user_id','blog_id','read_count'];
}
