<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Repository;
use App\Group;
class GroupController extends Controller
{

    protected $model;

      public function __construct(Group $group)
     {
         # code...

        $this->model=new Repository($group);
     }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $metaparams=$this->set_default_meta();
        $ogdescription=$metaparams['ogdescription'];
        $ogtitle=$metaparams['ogtitle'];
        $ogimage=$metaparams['ogimage'];

        return view('create_group')->with(compact('ogimage','ogtitle','ogdescription'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    // set default meta paratmeters

     public  function set_default_meta()
   {
     return $this->model->set_default_meta(
             config('meta.blogimage'),
             trans('app.blogtagline'),
             trans('app.blogdescription')

     );
   }
}
