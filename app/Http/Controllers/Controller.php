<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App;
use Session;
use Carbon\Carbon;
use Request;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function __construct()
    {


    	$fulllang=Request::server('HTTP_ACCEPT_LANGUAGE');
        $lang = substr($fulllang, 0, 2);
    	switch ($lang) {
    		case 'fr':
    			App::setLocale('fr');
                Carbon::setLocale('fr');
    			break;
    		case'en':
            App::setLocale('en');
            Carbon::setLocale('en');
            break;
    		default:
    		     	App::setLocale('fr');
    		        Carbon::setLocale('fr');
    			break;
    	}

        //$this->middleware('auth');
    }
}
