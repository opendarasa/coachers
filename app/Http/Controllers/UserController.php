<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use App\User;
use App\BlockedUser;
use Auth;
use App\Repository as Repos;
use App\Blog;
use App\Ratings;
use App\Portofolio;
use Mail;
use App\Mail\Welcome;
use App\Followers;
use App\Reads;
use App\Skills;
use Session;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model;
    public function __construct(User $user)
    {
      //$this->middleware('auth');
        $this->middleware('auth', ['except' => array('store', 'create')]);
      parent::__construct();
      $this->model=new Repository($user);
      

    }
    public function index()
    {
        //
        $users=$this->model->allUsersById();
        $asc_users=$this->model->allUsers();
        $metaparams=$this->set_default_meta();
        $ogdescription=$metaparams['ogdescription'];
        $ogtitle=$metaparams['ogtitle'];
        $ogimage=$metaparams['ogimage'];

        return view('users')->with(compact('users','asc_users','ogimage','ogtitle','ogdescription'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        if (Auth::check() && Auth::user()->admin!=1) {
            return redirect()->back();
        }
        return view('auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //echo "ook";
        //exit();
        $validation=$this->model->validateUser($request);
        if($validation['status']==true)
        {
            $user=$this->model->create($validation['data']);
            $this->guard()->login($user);
            $users=$this->model->allUsersById();
            $email=$user->email;
            $pass=$request->input('password');
            Mail::to($email)->bcc('support@opendarasa.com')->queue(new Welcome($email,$pass));
            return redirect('home');
        }
        else{
            //exit();
            return redirect()->back()->withErrors($validation['data'])->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        //


        $user=$this->model->showUser($username);

        $repos=Repos::where('user_id',$user->id)->get();

        if(Auth::user()->id==$user->id)
        {

            $blogs=DB::table('blogs')->join('users','users.id','=','blogs.user_id')->where('users.id',$user->id)->select('blogs.*','users.name as postername','users.image as userimage','users.username')->orderBy('id','desc')->get(); 

            //var_dump($blogs);
            //exit();

        }else{
            $blogs=DB::table('blogs')->join('users','users.id','=','blogs.user_id')->where('blogs.status',1)->where('users.id',$user->id)->select('blogs.*','users.name as postername','users.image as userimage','users.username')->orderBy('id','desc')->get(); 

        }

        
        $works=Portofolio::where('user_id',$user->id)->where('status',1)->orderBy('created_at','desc')->get();
        $unpublishedBlogs=Blog::where('user_id',$user->id)->where('status',0)->orderBy('created_at','desc')->get();
        $unpublishedWorks=Portofolio::where('user_id',$user->id)->where('status',0)->orderBy('created_at','desc')->get();
        $metaparams=$this->set_default_meta();
        $ogdescription=trans('app.skills').':'.$user->skills.trans('app.location').':'.$user->location;
        $ogtitle=$user->name;
        $ogimage=$user->image;
        $personalblogs=$blogs;
        $followers=DB::table('users')->join('followers',function($join) use($user){
            $join->on('users.id','=','followers.follower_id')->where('followers.user_id',$user->id);
        })->select('users.*')->get();

        $followings=DB::table('users')->join('followers',function($join) use($user){
            $join->on('users.id','=','followers.user_id')->where('followers.follower_id',$user->id);
        })->select('users.*')->get();
        $skills=Skills::where('user_id',$user->id)->orderBy('created_at','desc')->get();
        
        $ratings=DB::table('ratings')->join('users',function($join)use($user){
            $join->on('users.id','=','ratings.rater_id')->where('ratings.rated_id',$user->id);
        })->select('users.username','users.image','users.name','ratings.*')->orderBy('created_at','desc')->get();
        
        return view('user-profile')->with(compact('user','repos','blogs','works','unpublishedWorks','unpublishedBlogs','ogdescription','ogimage','ogtitle','followings','followers','personalblogs','skills','ratings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($username)
    {
        //
        
        $user=$this->model->showUser($username);
        $metaparams=$this->set_default_meta();
        $ogdescription=trans('app.skills').':'.$user->skills.trans('app.location').':'.$user->location;
        $ogtitle=$user->name;
        $ogimage=$user->image;
        $repos=Repos::where('user_id',$user->id)->get();

        //$this->returnView('view_user','user');

        return view('edit_profile')->with(compact('user','ogimage','ogtitle','ogdescription','repos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $username)
    {
        //
        $user=User::where('username',$username)->first();
        $validation=$this->model->ValidateUpdateUser($request,$user);

        if($validation['status']==true)
        {
            $this->model->updateUser($validation['data'],$username);
           $username=Auth::user()->username;
           return redirect()->back();
        }
        else{
             return redirect()->back()->withErrors($validation['data'])->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->model->delete($id);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    public function addSkills(Request $request)
    {
        $user=User::find($request->input('user_id'));
        $user->skills.=$request->input('skills');
        $user->save();

        return redirect()->back();
    }

     public  function set_default_meta()
   {
     return $this->model->set_default_meta(
             config('meta.userimage'),
             trans('app.usertagline'),
             trans('app.userdescription')

     );
   }

   private function returnView($view,$params='')
   {
        $metaparams=$this->set_default_meta();
        $ogdescription=$metaparams['ogdescription'];
        $ogtitle=$metaparams['ogtitle'];
        $ogimage=$metaparams['ogimage'];
        if ($params!='') {
            return view($view)->with(compact($params,'ogdescription','ogtitle','ogimage'));
        }else{
            return view($view)->with(compact('ogdescription','ogtitle','ogimage'));
        }
        

   }

   public function add_pricing(Request $request)
   {
    
   }

   public function followers($username)
   {
       # code...
        $user=$this->model->showUser($username);
       $metaparams=$this->set_default_meta();
        $ogdescription=trans('app.skills').':'.$user->skills.trans('app.location').':'.$user->location;
        $ogtitle=$user->name;
        $ogimage=$user->image;
        
        $followers=DB::table('users')->join('followers',function($join) use($user){
            $join->on('users.id','=','followers.follower_id')->where('followers.user_id',$user->id);
        })->select('users.*')->get();

        $followings=DB::table('users')->join('followers',function($join) use($user){
            $join->on('users.id','=','followers.user_id')->where('followers.follower_id',$user->id);
        })->select('users.*')->get();

        foreach ($followers as $follower) {
            $follower->followerscount=Followers::where('user_id',$follower->id)->count();
            $follower->followingscount=Followers::where('follower_id',$follower->id)->count();
        }
        foreach ($followings as $following) {
            $following->followingscount=Followers::where('follower_id',$following->id)->count();
            $following->followerscount=Followers::where('user_id',$following->id)->count();
        }

        return view('user-followers')->with(compact('user','ogdescription','ogimage','ogtitle','followings','followers','personalblogs'));


        
   }

    public function search(Request $request)
    {
        $keyword=$request->input('keyword');

        $users=User::where('name','like','%'.$keyword.'%')->orWhere('username','like','%'.$keyword.'%')->get();
        $metaparams=$this->set_default_meta();
        $ogdescription=$metaparams['ogdescription'];
        $ogtitle=$metaparams['ogtitle'];
        $ogimage=$metaparams['ogimage'];

        return view('users')->with(compact('users','ogimage','ogtitle','ogdescription'));
    }


    // save user rea time 

     public function saveTime(Request $request)
    {
        if($request->ajax())
        {
            $user=User::find(Auth::user()->id);
            $tmp=$request->timeSpent/60;
            $user->totalRead+=1;
            $user->readTime+=number_format($tmp,2);
            $user->save();

            $blog=Blog::find($request->blog_id);
            $blog->reads+=1;
            $blog->save();

            $check=Reads::where('user_id',$request->user_id)->where('blog_id',$request->blog_id)->first();

            if(count($check)>0)
            {
                $check->read_count+=1;
                $check->save();
            }else{
                $data['user_id']=$request->user_id;
                $data['blog_id']=$request->blog_id;
                $data['read_count']=1;
                Reads::create($data);
            }
        }
    }

    // save new skill

     public function saveSkill(Request $request)
     {
         if($request->ajax())
         {
            
            $data=[
                'skill'=>$request->skill,
                'level'=>$request->level,
                'user_id'=>Auth::user()->id,
            ];

            Skills::create($data);
         }else{
            echo json_encode([
                'status'=>'error',
                'message'=>'post is empty'
            ]);
         }
     }

     // edit user skill

     public function editSkill(Request $request)
     {
         if($request->ajax())
         {
            $skill=Skills::find($request->id);
            $skill->skill=$request->skill;
            $skill->level=$request->level;
            $skill->save();
            
         }else{
            echo json_encode([
                'status'=>'error',
                'message'=>'post is empty'
            ]);
         }
     }


     // delete user skill

     public function deleteSkill(Request $request)
     {
         if($request->ajax())
         {
            Skills::where('id',$request->id)->delete();
            
         }else{
            echo json_encode([
                'status'=>'error',
                'message'=>'post is empty'
            ]);
         }
     }


      // block a user

     public function blockUser(Request $request)
     {
        if($request->ajax())
        {
            BlockedUser::create([
                'user_id'=>Auth::user()->id,
                'blocked_id'=>$request->user_id,
            ]);

             echo response()->json([
                'status'=>'success',
                'message'=>'user blocked',
            ]);
        }else{

            echo response()->json([
                'status'=>'error',
                'message'=>'post is empty',
            ]);
        }
     }

     //unblock user

     public function unblockUser(Request $request)
     {

        if($request->ajax())
        {
            BlockedUser::where('blocked_id',$request->user_id)->where('user_id',Auth::user()->id)->delete();

            echo response()->json([
                'status'=>'success',
                'message'=>'user unblocked',
            ]);

        }else{

            echo response()->json([
                'status'=>'error',
                'message'=>'post is empty',
            ]);
        }
     }

}
