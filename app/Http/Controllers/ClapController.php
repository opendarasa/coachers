<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Claps;
use Auth;
use Session;
class ClapController extends Controller
{
    //
     public function __construct()
    {
    	parent::__construct();

    	$this->middleware('auth');
    }

    public function Clap($id)
    {
    	
    		$blog=Blog::find($id);
            $blog->claps=$blog->claps+1;
            $blog->save();
            $data=[
                'uid'=>Auth::user()->id,
                'blogid'=>$id,
            ];
            Claps::create($data);
            Session::put('divid','clap');
    		return redirect()->back();
    	
    }


    public function ajaxClap(Request $request)
    {

        if($request->ajax())
        {
            $id=$request->id;
            $blog=Blog::find($id);
            $blog->claps=$blog->claps+1;
            $blog->save();
            $data=[
                'uid'=>Auth::user()->id,
                'blogid'=>$id,
            ];
            Claps::create($data);
        }else{

            $error=[
                'status'=>'error',
                'message'=>'post empty',
            ];

            echo json_encode($error);
        }
        
            
            //Session::put('divid','clap');
            //return redirect()->back();
        
    }

    public function removeClap($id)
    {
            $blog=Blog::find($id);
            $blog->claps=$blog->claps-1;
            $blog->save();

            Claps::where('uid',Auth::user()->id)->where('blogid',$id)->delete();
            Session::put('divid','clap');
            return redirect()->back();
    }


    public function ajaxremoveClap(Request $request)
    {

        if($request->ajax())
        {
            $id=$request->id;
            $blog=Blog::find($id);
            $blog->claps=$blog->claps-1;
            $blog->save();

            Claps::where('uid',Auth::user()->id)->where('blogid',$id)->delete();
            //Session::put('divid','clap');
            //return redirect()->back();
        }else{

            $error=[
                'status'=>'error',
                'message'=>'post empty',
            ];

            echo json_encode($error);
            }
    }

}
