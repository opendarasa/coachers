<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Functions\Functions;
use App\Contracts;
use Validator;
use PDF;
use App;
use Storage;
use Mail;
use App\Mail\NewLogoCase;
use App\Mail\YourLogoRequestWasSuccessful;
use App\Mail\NewWebCase;
use App\Mail\NewWebCaseWithUX;
use App\Mail\YourWebDesignRequestWasSuccessful;
use App\Mail\NewMobileDevCase;
use App\Mail\NewMobileDevCaseWithUX;
use App\Mail\YourMobileDevelopmentRequestWasSuccessful;
use App\Mail\NewSEOCase;
use App\Mail\NewSEOCaseWithUX;
use App\Mail\YourSEORequestWasSuccessful;
use Session;
use App\User;
class newcaseController extends Controller
{
    //
     protected $model ;
     private $freelance;
     public function __construct()
     {
       parent::__construct();
       $this->middleware('auth');
        
     }

     public function store(Request $request)
     {
        if (Session::has('user_id')) {
            $user_id=Session::get('user_id');
            $this->freelance=User::find($user_id);
        }

        $validator=Validator::make($request->all(),[
            'clientEmail'=>'required|email',
            'client_name'=>'string',
            'companyname'=>'string',
            'industry'=>'string',
            'booking'=>'required',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $data='<div>';
        $data.='<p>'.trans('app.client_name').': '.$request->input('client_name').'</p>';
        $data.='<p>'.trans('app.client_email').': '.$request->input('clientEmail').'</p>';
        $data.='<p>'.trans('app.companyname').': '.$request->input('companyname').'</p>';
        if (!empty($this->freelance)) {
            $data.='<p><b>'.trans('app.freelancer_name').' :</b>'.$this->freelance->username.'</p>';
        }
        $data.='<p>'.trans('app.companyIndustry').': '.$request->input('industry').'</p>';
        $data.='<p>'.trans('app.booking_date').': '.$request->input('booking').'</p>';
        $data.='<p>'.trans('app.extra_days').': '.$request->input('extra_days').'</p>';
        $data.='<p>'.trans('app.client_toughts').': '.$request->input('more').'</p>';

        $data.='<img src="'.$request->input('drawing').'" style="width:50%;margin-left:20%">';
        $data.='</div>';

        PDF::loadHTML($data)->save('img/files/'.$request->input('companyname').'.pdf');

         $filename='img/files/'.$request->input('companyname').'.pdf';

         Mail::to('ouedmenga@gmail.com')->send(new NewLogoCase($filename));
         Mail::to($request->input('clientEmail'))->send(new YourLogoRequestWasSuccessful($filename));

         return redirect()->back()->with('message',trans('app.verify_email'));
         
        
     }


     public function storeweb(Request $request)
     {
        if (Session::has('user_id')) {
            $user_id=Session::get('user_id');
            $this->freelance=User::find($user_id);
        }
        $validator=Validator::make($request->all(),[
        'company_name'=>'required|string',
        'company_industry'=>'required|string',
        'fName'=>'required|string',
        'lName'=>'required|string',
        'email'=>'required|email',
        'text'=>'required',

        ]);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $data='<div>';
        $data.='<div>';
        $data.='<img width="100px" height="100px" src="'.asset('img/500design.png').'" style="float:left;">';
        $data.='<h1 style="text-align:center;">'.trans('app.web_dev_order_detail').'</h1>';
        $data.='</div>';
        $data.='<hr>';
        $data.='<p><b>'.trans('app.first_name').'</b>: '.$request->input('fName').'</p>';
        $data.='<p><b>'.trans('app.last_name').'</b>: '.$request->input('lName').'</p>';
        $data.='<p><b>'.trans('app.client_email').'</b>: '.$request->input('email').'</p>';
        if($request->has('mobile'))
        {
          $data.='<p><b>'.trans('app.client_mobile').'</b>: '.$request->input('mobile').'</p>';
        }
        $data.='<p><b>'.trans('app.companyname').'</b>: '.$request->input('company_name').'</p>';
        if (isset($this->freelance)) {
            $data.='<p><b>'.trans('app.freelancer_name').' :</b>'.$this->freelance->username.'</p>';
        }
        $data.='<p><b>'.trans('app.companyIndustry').'</b>: '.$request->input('company_industry').'</p>';
        $data.='<p><b>'.trans('app.booking_date').'</b>: '.$request->input('booking').'</p>';
        $data.='<p><b>'.trans('app.project_description').'</b>: '.$request->input('text').'</p>';
        
        $data.='</div>';
        
        PDF::loadHTML($data)->save('img/files/'.$request->input('company_name').'_web.pdf');
        $filename='img/files/'.$request->input('company_name').'_web.pdf';

        if($request->has('file'))
        {

            $uxfile=$request->file('file');

            $path=public_path('img/files');

            $uxfilename=$uxfile->getClientOriginalName();

            $uxfile->move($path,$uxfilename);
            $fileUrl=config('app.url').'/img/files/'.$uxfilename;

            Mail::to('ouedmenga@gmail.com')->send(new NewWebCaseWithUX($filename,$fileUrl));
        }

        else
        {
            Mail::to('ouedmenga@gmail.com')->send(new NewWebCase($filename));
        }

        Mail::to($request->input('email'))->send(new YourWebDesignRequestWasSuccessful($filename));
         $message=trans('app.verify_email');
        return redirect()->back()->withOk($message);

     }

     public function storeui(Request $request)
     {
        if (Session::has('user_id')) {
            $user_id=Session::get('user_id');
            $this->freelance=User::find($user_id);
        }
        $validator=Validator::make($request->all(),[
        'company_name'=>'required|string',
        'company_industry'=>'required|string',
        'fName'=>'required|string',
        'lName'=>'required|string',
        'type'=>'required|string',
        'email'=>'required|email',
        'text'=>'required',

        ]);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $data='<div>';
        $data.='<div>';
        $data.='<img swidth="70px" height="70px" src="'.asset('img/500design.png').'" style="float:left;">';
        $data.='<h1 style="text-align:center;">'.trans('app.mobile_dev_order_detail').'</h1>';
        $data.='</div>';
        $data.='<hr>';
        $data.='<p> <b>'.trans('app.first_name').'</b>: '.$request->input('fName').'</p>';
        $data.='<p><b>'.trans('app.last_name').'</b>: '.$request->input('lName').'</p>';
        $data.='<p><b>'.trans('app.client_email').'</b>: '.$request->input('email').'</p>';
        if($request->has('mobile'))
        {
          $data.='<p><b>'.trans('app.client_mobile').'</b>: '.$request->input('mobile').'</p>';
        }

        $data.='<p><b>'.trans('app.companyname').'</b>: '.$request->input('company_name').'</p>';
        if (isset($this->freelance)) {
            $data.='<p><b>'.trans('app.freelancer_name').' :</b>'.$this->freelance->username.'</p>';
        }
        $data.='<p><b>'.trans('app.companyIndustry').'</b>: '.$request->input('company_industry').'</p>';
        $data.='<p><b>'.trans('app.booking_date').'</b>: '.$request->input('booking').'</p>';
        $data.='<p><b>'.trans('app.app_type').'</b>: '.$request->input('type');
        $data.='<p><b>'.trans('app.project_description').'</b><br>: '.$request->input('text').'</p>';
        
        $data.='</div>';
        
        PDF::loadHTML($data)->save('img/files/'.$request->input('company_name').'_ui.pdf');
        $filename='img/files/'.$request->input('company_name').'_ui.pdf';

        if($request->has('file'))
        {

            $uxfile=$request->file('file');

            $path=public_path('img/files');

            $uxfilename=$uxfile->getClientOriginalName();

            $uxfile->move($path,$uxfilename);
            $fileUrl=config('app.url').'/img/files/'.$uxfilename;

            Mail::to('ouedmenga@gmail.com')->send(new NewMobileDevCaseWithUX($filename,$fileUrl));
        }

        else
        {
            Mail::to('ouedmenga@gmail.com')->send(new NewMobileDevCase($filename));
        }

        Mail::to($request->input('email'))->send(new YourMobileDevelopmentRequestWasSuccessful($filename));
         $message=trans('app.verify_email');
        return redirect()->back()->withOk($message);

     }


public function storeposter(Request $request)
     {
        if (Session::has('user_id')) {
            $user_id=Session::get('user_id');
            $this->freelance=User::find($user_id);
        }
        $validator=Validator::make($request->all(),[
        'company_name'=>'required|string',
        'company_industry'=>'required|string',
        'fName'=>'required|string',
        'lName'=>'required|string',
        'email'=>'required|email',
        'text'=>'required',

        ]);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $data='<div>';
        $data.='<div>';
        $data.='<img width="70px" height="40px" src="'.asset('img/500design.png').'" style="float:left;">';
        $data.='<h1 style="text-align:center;">'.trans('app.seo_dev_order_detail').'</h1>';
        $data.='</div>';
        $data.='<hr>';
        $data.='<p><b>'.trans('app.first_name').'</b>: '.$request->input('fName').'</p>';
        $data.='<p><b>'.trans('app.last_name').'</b>: '.$request->input('lName').'</p>';
        $data.='<p><b>'.trans('app.client_email').'</b>: '.$request->input('email').'</p>';
        if($request->has('mobile'))
        {
          $data.='<p><b>'.trans('app.client_mobile').'</b>: '.$request->input('mobile').'</p>';
        }
        $data.='<p><b>'.trans('app.companyname').'</b>: '.$request->input('company_name').'</p>';
        if (isset($this->freelance)) {
            $data.='<p><b>'.trans('app.freelancer_name').' :</b>'.$this->freelance->username.'</p>';
        }
        $data.='<p><b>'.trans('app.companyIndustry').'</b>: '.$request->input('company_industry').'</p>';
        $data.='<p><b>'.trans('app.booking_date').'</b>: '.$request->input('booking').'</p>';
        $data.='<p><b>'.trans('app.project_description').'</b>: '.$request->input('text').'</p>';
        
        
        $data.='</div>';
        
        PDF::loadHTML($data)->save('img/files/'.$request->input('company_name').'_seo.pdf');
        $filename='img/files/'.$request->input('company_name').'_seo.pdf';

        if($request->has('file'))
        {

            $uxfile=$request->file('file');

            $path=public_path('img/files');

            $uxfilename=$uxfile->getClientOriginalName();

            $uxfile->move($path,$uxfilename);
            $fileUrl=config('app.url').'/img/files/'.$uxfilename;

            Mail::to('ouedmenga@gmail.com')->send(new NewSEOCaseWithUX($filename,$fileUrl));
        }

        else
        {
            Mail::to('ouedmenga@gmail.com')->send(new NewSEOCase($filename));
        }

        Mail::to($request->input('email'))->send(new YourSEORequestWasSuccessful($filename));
         $message=trans('app.verify_email');
        return redirect()->back()->withOk($message);

     }

     private function savecookie($id,$view)
     {
        
          Session::put('user_id',$id);

          return redirect($view);
        
     }

     public function storeSeoClient($id)
     {
       return $this->savecookie($id,'new-seo-case');
     }
     
     public function storeWebClient($id){

        return $this->savecookie($id,'new-web-case');

     }
    public function storeMobileClient($id){
      return $this->savecookie($id,'new-ui-case');
    }

}
