<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Comments;
use App\Blog;
use App\User;
use Carbon\Carbon;
use Auth;
use Session;
use App\Notifications\NewComment;
class CommentController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'comment'=>$request->input('comment'),
            'blog_id'=>$request->input('blog_id'),
            'user_id'=>Auth::user()->id,
        ];
        $comment=Comments::create($data);
        //var_dump($comment);
        //exit();
        
        $blog=Blog::find($data['blog_id']);
        if (count($blog)>0 && $blog->user_id!=Auth::user()->id) {
            $user=User::find($blog->user_id);
            $data=Auth::user()->name.' '.trans('app.commented_on_your_article').' :<span style="color:black;">'.mb_substr($comment->comment,0,30).'...</span>';
            $image=asset('img/users/'.Auth::user()->image);
            $url=url('blog/'.$blog->slug);
            $user->notify(new NewComment($data,$url,$image));

        }
        Session::put('divid','mycomment_'.$comment->id);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Reply to the comment resource from storage.
     *
     * @param  int  $request
     * @return \Illuminate\Http\Response
     */

     public function reply(Request $request)
     {
        $now=Carbon::now();
         
         foreach ($now as $key => $value) {
             $now=$value;
             break;
         }

        
         $data=[
            'comment'=>$request->input('comment'),
            'name'=>Auth::user()->name,
            'image'=>Auth::user()->image,
            'created_at'=>$now,
        ];
        
        
        $comment=Comments::find($request->input('comment_id'));
        
        if ($comment->replies!=null||$comment->replies!='') {
           
           
           $comment->replies.=",".json_encode($data);
        }else{
            $comment->replies=json_encode($data);
        }

        $comment->save();

        $blog=Blog::find($comment->blog_id);
        if ( $comment->user_id!=Auth::user()->id) {
            $user=User::find($comment->user_id);
            $data=Auth::user()->name.' '.trans('app.replied_to_your_comment').' :<span style="color:black;">'.mb_substr($data['comment'],0,30).'..</span>';
            $image=asset('img/users/'.Auth::user()->image);
            $url=url('blog/'.$blog->slug);
            $user->notify(new NewComment($data,$url,$image));

        }
        //$comment=Comments::find($request->input('comment_id'));
        //var_dump();
        //exit();
         Session::put('divid','mycomment_'.$comment->id);
        return redirect()->back();
     }
}
