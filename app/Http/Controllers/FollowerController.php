<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Followers;
use App\User;
use App\Ratings;
use App\Notifications\NewFollower;
use App\Notifications\NewRating;
use Auth;
use Session;
class FollowerController extends Controller
{
    //

    public function __construct()
    {
    	parent::__construct();
      $this->middleware('auth');
    	//$this->middleware('auth')->only('add','add_rating');
    }

     public function add($user_id)
     {
       $check=Followers::where('user_id',$user_id)->where('follower_id',Auth::user()->id)->count();

       if ($check==0) {
       	if (Auth::user()->id!=$user_id) {

       		Followers::create([
       			'user_id'=>$user_id,
       			'follower_id'=>Auth::user()->id,
       		]);

       	$user=User::find($user_id);
       	$url=url('users/'.Auth::user()->username);
       	$data=Auth::user()->name." ".trans('app.is_now_following_you');
        $subject=trans('app.new_follower_notification');
        $image=asset('img/users/'.Auth::user()->image);
       	$user->notify(new NewFollower($data,$url,$image,$subject));
       	}
       		

       	}
       	return redirect()->back();	
     }

     // unfollow user


     public function ajaxfollow(Request $request)
     {

      

      if($request->ajax())
      {

        $user_id=$request->id;
          $check=Followers::where('user_id',$user_id)->where('follower_id',Auth::user()->id)->count();

       if ($check==0) {
        if (Auth::user()->id!=$user_id) {

          Followers::create([
            'user_id'=>$user_id,
            'follower_id'=>Auth::user()->id,
          ]);

        $user=User::find($user_id);
        $url=url('users/'.Auth::user()->username);
        $data=Auth::user()->name." ".trans('app.is_now_following_you');
        $subject=trans('app.new_follower_notification');
        $image=asset('img/users/'.Auth::user()->image);
        $user->notify(new NewFollower($data,$url,$image,$subject));
        }
          

        }
      }
       
        //return redirect()->back();  
     }

     public function unfollow($user_id)
     {


       $check=Followers::where('user_id',$user_id)->where('follower_id',Auth::user()->id)->count();

       if ($check>0) {
        
        if (Auth::user()->id!=$user_id) {

          Followers::where('user_id',$user_id)->where('follower_id',Auth::user()->id)->delete();

        $user=User::find($user_id);
        $url=url('users/'.Auth::user()->username);
        $data=Auth::user()->name." ".trans('app.is_unfollowed_you');
        $subject=trans('app.new_unfollower_notification');
        $image=asset('img/users/'.Auth::user()->image);
        $user->notify(new NewFollower($data,$url,$image,$subject));
        Session::put('divid','aboutposter');
        }
          

        }
        //return redirect()->back();  
     }


     public function ajaxunfollow(Request $request)
     {

       if($request->ajax())
       {
        $user_id=$request->id;
         $check=Followers::where('user_id',$user_id)->where('follower_id',Auth::user()->id)->count();

       if ($check>0) {
        
        if (Auth::user()->id!=$user_id) {

              Followers::where('user_id',$user_id)->where('follower_id',Auth::user()->id)->delete();

            $user=User::find($user_id);
            $url=url('users/'.Auth::user()->username);
            $data=Auth::user()->name." ".trans('app.is_unfollowed_you');
            $subject=trans('app.new_unfollower_notification');
            $image=asset('img/users/'.Auth::user()->image);
            $user->notify(new NewFollower($data,$url,$image,$subject));
            Session::put('divid','aboutposter');
          }
          

           }
      }
       
        //return redirect()->back();  
     }


     public function readNotification(Request $request)
     {
     	if($request->ajax())
     	{
          Auth::user()->unreadNotifications->where('id',$request->id)->markAsRead();
     	}
     }

     public function all_notifications()
     {
     	Session::put('profile-tab','notication');

     	return redirect()->route('users.show',['username'=>Auth::user()->username]);
     }

      public function add_rating(Request $request)
      {
      	if($request->ajax())
      	{

      	  $check=Ratings::where('rater_id',Auth::user()->id)->where('rated_id',$request->user_id)->count();
      	  if($check==0){


		          $user=User::find($request->user_id);
		          
		          $user->rating=($user->totalRatings+$request->rating)/($user->reviews+1);

		          $user->reviews=$user->reviews+1;

		          $user->totalRatings=$user->totalRatings+$request->rating;
		          $badge='F';
		          if ((($user->reviews+1)>=30 && ($user->reviews+1)<70) && ($user->rating>=4)) {
		          	$badge='B';
		          }elseif((($user->reviews+1)>=70 && ($user->reviews+1)<100)&& ($user->rating>=4)) {
		          	$badge='S';
		          }elseif((($user->reviews+1)>=100 ) && ($user->rating>=4)) {
		          	$badge='G';
		          }
		          $user->badge=$badge;
		          $user->save();

		          Ratings::create([
		          	'rater_id'=>Auth::user()->id,
		          	'rated_id'=>$request->user_id,
		          	'rating'=>$request->rating,
		          	'comment'=>$request->comment,
		          ]);

		          $data=Auth::user()->name.' '.trans('app.just_rated').' '.$request->rating.' '.trans('app.stars');
		          $url=url('users/'.$user->username);
		          $user->notify(new NewRating($data,$url));
		          return json_encode(['status'=>'success','message'=>trans('app.rating_success')]);
		    }else{

		    	return json_encode(['status'=>'error','message'=>trans('app.rating_already_exist')]);
		    }
      	}
      }
}
