<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\User;
use Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Newsletter;
use App\Mail\Newtalent;
use Mail;
use Illuminate\Support\Facades\Log;
class ApiController extends Controller
{
    //
    public $apiUrl;
    public $client_id;
    public $client_secret;
    public $userDataURL;
    public $callbackURL;

    public $access_token;

     public function __construct()
    {
    	$this->apiUrl='https://www.opendarasa.com/oauth/token';

    	$this->client_secret=config('opendarasaAPI.client_secret');
    	$this->client_id=config('opendarasaAPI.client_id');

    	$this->callbackURL=config('opendarasaAPI.callbackURL');
    	$this->userDataURL=config('opendarasaAPI.userDataURL');
    	$this->access_token=config('opendarasaAPI.access_token');

    }

    public function getData(Request $request)
    {
    	 $http = new Client();
    
          
            
         	$response = $http->post($this->apiUrl, [
                'form_params' => [
                'grant_type' => 'password',
                'client_id' => $this->client_id,
                'client_secret' =>$this->client_secret,
                'username' => 'ouedmenga@gmail.com',
                'password' => 'ouepar01',
                'scope' => '*',
                ],
              'http_errors'=>false

        
    ]);
    return json_decode((string) $response->getBody(), true);
  
    }

    public function grantAccess(Request $request)
    {

    $http = new Client();

    $response = $http->post($this->apiUrl, [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => $this->client_id,
            'client_secret' =>$this->client_secret,
            'redirect_uri' => $this->callbackURL,
            'code' => $request->code,
            'http_errors'=>false,
        ],
    ]);

    $resultArray=json_decode((string) $response->getBody(), true);

    $accesstoken=isset($resultArray['access_token'])?$resultArray['access_token']:false;

    
     $token = 'Bearer '.$accesstoken;
     $client = new Client(['base_uri' => 'https://www.opendarasa.com/api/']);
     $headers = [
    'Authorization' =>$token,        
    'Accept'        => 'application/json',
     ];

     $response = $client->request('GET', 'user', [
        'headers' => $headers
    ]);

      $newuser=json_decode($response->getBody());

      $user=User::where('email',$newuser->email)->first();

      if(empty($user))
      {
      	$myuser=User::create([
            'name' => $newuser->name,
            'email' => $newuser->email,
            'username'=>explode('@',$newuser->email)[0],
            'password' => Hash::make($newuser->email),
        ]);
       
        Auth::login($myuser);
      }else{
      	
      	Auth::login($user);
      }
     return redirect('home');


    
      
    }

    public function getTalents()
    {
    	$talents=$this->makeRequest('talents');
    	$talents=json_decode($talents);
        if (!isset($talents->error)) {
        	foreach ($talents as $talent) {
        		$user=User::where('email',$talent->email)->first();

        		if(empty($user))
        		{
        			User::create([
        			 'name'=>$talent->name,
        			 'email'=>$talent->email,
        			 'password'=>Hash::make($talent->email),
        			 'username'=>explode("@",$talent->email)[0]
        			]);
                    
                    Mail::to('ouedmenga@gmail.com')->send(new Newtalent($talent->name,$talent->courseName));
                    
                    Log::info($talent->name." was added to talent List ad was sent email on ".date('Y-m-d'));
                    Newsletter::subscribe($talent->email,['FNAME'=>$talent->name,'LNAME'=>$talent->courseName]);

        		}



        		
        	}
        }



    }


    public function makeRequest($apipath)
    {
     if (isset($this->access_token)) {

     	$token = 'Bearer '.$this->access_token;
        $client = new Client(['base_uri' => 'https://www.opendarasa.com/api/']);
        $headers = [
       'Authorization' =>$token,        
       'Accept'        => 'application/json',
        ];

        $response = $client->request('GET',$apipath, [
        'headers' => $headers
       ]);

         $result=$response->getBody();


        return $result;
    	
    }else{

    	return response()->json(['error'=>'404','message'=>'acces token or api pathname is not correct']);
    }
     	
     }
     
}
