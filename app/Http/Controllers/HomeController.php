<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Portofolio;
use App\User;
use App\Mail\ThankYouForYourMessage;
use Mail;
use App\Repositories\Repository;
use App\Skills;
use App\Followers;
use App;
use Session;
use Auth;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
            
       
           $blogs=DB::table('blogs')->join('users','users.id','=','blogs.user_id')->where('blogs.status',1)->select('blogs.*','users.name as postername','users.image as userimage','users.username')->orderBy('id','desc')->get(); 
        
        
        $works=Portofolio::where('status',1)->orderBy('id','desc')->limit(6)->get();
        $users=User::where('hidden',0)->orderBy('rating','desc')->orderBy('reviews','desc')->limit(9)->get();

        $metaparams=$this->set_default_meta();
        $ogdescription=$metaparams['ogdescription'];
        $ogtitle=$metaparams['ogtitle'];
        $ogimage=$metaparams['ogimage'];
        $user=Auth::user();
         $followers=DB::table('users')->join('followers',function($join) use($user){
            $join->on('users.id','=','followers.follower_id')->where('followers.user_id',$user->id);
        })->select('users.*')->get();

         // get user followers and followings

        $followings=DB::table('users')->join('followers',function($join) use($user){
            $join->on('users.id','=','followers.user_id')->where('followers.follower_id',$user->id);
        })->select('users.*')->get();

         $personalblogs=Blog::where('user_id',$user->id)->where('status',1)->orderBy('created_at','desc')->get();

         // get user mutuals

         $mutuals=array();
         $rand=0;
         foreach ($followings as $following) {
             $rand=rand(1,9);
             //if($rand%rand(2,3)==0)
             //{
                $followingFollowers=DB::table('users')->join('followers',function($join) use($following){
                    $join->on('followers.follower_id','=','users.id')->where('followers.user_id',$following->id);
                })->select('users.*')->get();
                $hold=array();
                $count=0;
                foreach ($followingFollowers as $mutual) {
                    if($count<9){

                                if(!in_array($mutual->id, $hold))
                            {
                                $checkIffolling=Followers::where('follower_id',Auth::user()->id)->where('user_id',$mutual->id)->count();

                                if($checkIffolling==0)
                                {
                                   $mutuals[]=[
                                    'id'=>$mutual->id,
                                    'name'=>$mutual->name,
                                    'username'=>$mutual->username,
                                    'image'=>$mutual->image,
                                  ];

                                   $hold[]=$mutual->id;   
                                }
                                

                            }
                    }

                    $count++;
                    
                        
                        
                }

                //var_dump($hold);
                //exit();
             //}
         }
   
      $skills=Skills::where('user_id',$user->id)->orderBy('created_at','desc')->get();

         
        return view('home')->with(compact('blogs','works','users','ogimage','ogdescription','ogtitle','favblogs','followers','followings','personalblogs','user','mutuals','skills'));
    }

    public function sendMessage(Request $request)
    {
        $name=$request->input('name');
        $email=$request->input('email');
        $phone=$request->input('phone');
        $message=$request->input('message');
        Mail::to('ouedmenga@gmail.com')->send(new ThankYouForYourMessage($name,$email,$phone,$message));

    }

    public function publishBlog($slug)
    {
        $this->model=new Repository(new Blog());
        $blog=$this->model->show($slug);
        $blog->status=1;
        $blog->save();
        return redirect()->route('blog.show',['slug'=>$blog->slug]);
    }
    public function publishWork($slug)
    {
        $this->model=new Repository(new Portofolio());
        $work=$this->model->show($slug);
        $work->status=1;
        $work->save();
        return redirect()->route('portofolio.show',['slug'=>$work->slug]);
    }

    public function unpublishBlog($slug)
    {
        $this->model=new Repository(new Blog());
        $blog=$this->model->show($slug);
        $blog->status=0;
        $blog->save();
        return redirect()->route('blog.show',['slug'=>$blog->slug]);
    }
    public function unpublishWork($slug)
    {
        $this->model=new Repository(new Portofolio());
        $work=$this->model->show($slug);
        $work->status=0;
        $work->save();
        return redirect()->route('portofolio.show',['slug'=>$work->slug]);
    }
    public function changeLanguage($lang)
    {
        

       Session::put('lang',$lang);
       App::setLocale($lang);
       //echo App::getLocale();
       
       return redirect()->back();

    }

    public  function set_default_meta()
   {
    $this->model=new Repository(new Blog());
     return $this->model->set_default_meta(
             config('meta.defaultimage'),
             trans('app.defaulttagline'),
             trans('app.description')

     );
   }

   private function returnView($view,$params='')
   {
        $metaparams=$this->set_default_meta();
        $ogdescription=$metaparams['ogdescription'];
        $ogtitle=$metaparams['ogtitle'];
        $ogimage=$metaparams['ogimage'];
        if ($params!='') {
            return view($view)->with(compact($params,'ogdescription','ogtitle','ogimage'));
        }else{
            return view($view)->with(compact('ogdescription','ogtitle','ogimage'));
        }
        

   }

   public function saveprofile(Request $request)
   {
     if( $request->input('check')=="on"){

       $user_id=$request->input('user_id');
       $user=User::find($user_id);
       $user->hidden=0;
       $user->save(); 
     }else{
        $user_id=$request->input('user_id');
       $user=User::find($user_id);
       $user->hidden=1;
       $user->save();

     }
       

       return redirect()->back();
   }
}
