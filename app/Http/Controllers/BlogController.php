<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\Repository;
use App\Blog;
use Validator;
use App\Portofolio;
use App\User;
use App\SearchLog;
use Auth;
use App\Comments;
class BlogController extends Controller
{



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model;

    public function __construct(Blog $blog)
    {
        parent::__construct();
        $this->model=new Repository($blog);
        $this->middleware('auth');
    }
    public function index()
    {  
    if (Auth::check()) {
            $favblogs=DB::table('blogs')->join('followers',function($join){
                $join->on('blogs.user_id','=','followers.user_id')->where('followers.follower_id',Auth::user()->id);

            })->where('blogs.status',1)->orderBy('id','desc')->select('blogs.*')->limit(9)->get();
        } 
        $blogs=$this->model->allById();
        $asc_blogs=$this->model->all();
        $metaparams=$this->set_default_meta();
        $ogdescription=$metaparams['ogdescription'];
        $ogtitle=$metaparams['ogtitle'];
        $ogimage=$metaparams['ogimage'];
        return view('blog')->with(compact('blogs','asc_blogs','ogdescription','ogtitle','ogimage','favblogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if(Auth::check())
      {
       $param='';

       return $this->returnView('create_blog_post',$param); 
      }else{
        return redirect("login");
      }
      
        //return view('create_blog_post','ogimage','ogtitle','ogdescription');
    }

    /**
     * Search article by keyword .
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        
      $keyword=$request->input('keyword');
      
      
        $blogs=DB::table('blogs')->join('users','users.id','=','blogs.user_id')->where('blogs.status',1)->where('blogs.title','like','%'.$keyword.'%')->orWhere('blogs.tags','like','%'.$keyword.'%')->orWhere('blogs.subtitle','like','%'.$keyword.'%')->orWhere('users.name','like','%'.$keyword.'%')->select('users.username as username','users.image as userimage','users.name as postername','blogs.*')->orderBy('blogs.id','desc')->get();

        $check=SearchLog::where('keyword','like','%'.$keyword.'%')->first();

        if(count($check)>0)
        {
            $check->volume+=1;
            $check->save();
        }else{
            SearchLog::create([
                'user_id'=>Auth::user()->id,
                'keyword'=>$keyword,
            ]);
        }
        
        $metaparams=$this->set_default_meta();
        $ogdescription=$metaparams['ogdescription'];
        $ogtitle=$metaparams['ogtitle'];
        $ogimage=$metaparams['ogimage'];
        $user=Auth::user();
        return view('view_by_category')->with(compact('blogs','ogdescription','ogtitle','ogimage','user'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation=$this->model->validateBlog($request);

        if ($validation['status']==true) {

        $data=$validation['data'];
        $check=Blog::where('slug',$data['slug'])->first();

        if(count($check)>0)
        {
            $data['slug'].='_'.rand(1,1000);
        }

        $blog=$this->model->create($data);

        
        return redirect()->route('blog.edit',['slug'=>$blog->slug]);
          
      }else{
        return redirect()->back()->withErrors($validation['data'])->withInput();
      }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        
        
        $blog=DB::table('blogs')->join('users',function($join) use($slug){
            $join->on('blogs.user_id','=','users.id')->where('blogs.slug',$slug);
        })->select('users.username','users.cover','users.name as postername','users.image as userimage','users.description as userdescription','blogs.*')->first();
        $user=User::find($blog->user_id);
        $latest_works=Portofolio::where('status',1)->orderBy('id','desc')->limit(5)->get();

        $relateds=DB::table('blogs')->join('users','users.id','=','blogs.user_id')->where('blogs.status',1)->where('blogs.cat_id',$blog->cat_id)->where('blogs.id','!=',$blog->id)->select('blogs.*','users.name as postername','users.image as userimage','users.username')->orderBy('blogs.claps','desc')->limit(3)->get(); 
        $metaparams=$this->set_default_meta();
        $ogdescription=$blog->subtitle;
        $ogtitle=$blog->title;
        $ogimage=$blog->image;

    $comments=DB::table('comments')->join('users',function($join) use($blog){
        $join->on('users.id','=','comments.user_id')->where('comments.blog_id',$blog->id);
        })->select('comments.*','users.name','users.image','users.username')->get();

    $readers=DB::table('reads')->join('users','users.id','=','reads.user_id')->join('blogs',function($join) use($blog){
        $join->on('reads.blog_id','=','blogs.id')->where('blogs.id',$blog->id);
    })->select('users.*','reads.read_count')->orderBy('reads.created_at','desc')->get(); 
        return view('view_blog2')->with(compact('blog','latest_works','blogs','user','ogdescription','ogimage','ogtitle','numclaps','comments','relateds','readers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        if (Auth::check()) {
            
            $blog=$this->model->show($slug);
            $metaparams=$this->set_default_meta();
        $ogdescription=$metaparams['ogdescription'];
        $ogtitle=$metaparams['ogtitle'];
        $ogimage=$metaparams['ogimage'];
            return view('edit_blog')->with(compact('ogimage','ogtitle','ogdescription','blog'));
        }else{
            return redirect("login");
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $check=Blog::where('slug',$slug)->count();
        if($check>0)
        {
             $validation=$this->model->validateUpdatedBlog($request,$slug);
        if($validation['status']==true)
        {
            $data=$validation['data'];
            $this->model->update($data,$slug);
            
            return redirect()->route('blog.edit',['slug'=>$data['slug']]);
        }else{
            return redirect()->back()->withErrors($validation['data'])->withInput();
        }

        }else{
            return redirect('home');
        }
        
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->model->delete($id);

        return redirect()->route('users.show',['usernam'=>Auth::user()->username]);
    }

   public  function set_default_meta()
   {
     return $this->model->set_default_meta(
             config('meta.blogimage'),
             trans('app.blogtagline'),
             trans('app.blogdescription')

     );
   }

   private function returnView($view,$params='')
   {
        $metaparams=$this->set_default_meta();
        $ogdescription=$metaparams['ogdescription'];
        $ogtitle=$metaparams['ogtitle'];
        $ogimage=$metaparams['ogimage'];
         $hottopics=SearchLog::where('volume','>',10)->orderBy('volume','desc')->get();
        if ($params!='') {
            return view($view)->with(compact($params,'ogdescription','ogtitle','ogimage','hottopics'));
        }else{

            return view($view)->with(compact('ogdescription','ogtitle','ogimage','hottopics'));
        }
        

   }

   public function view_by_category($category)
   {
       $types=trans('app.blog_types');
       $cat_id=array_search($category, $types);

        $metaparams=$this->set_default_meta();
        $ogdescription=$metaparams['ogdescription'];
        $ogtitle=$metaparams['ogtitle'];
        $ogimage=$metaparams['ogimage'];
        $user=Auth::user();
       $blogs=DB::table('blogs')->join('users',function($join) use($cat_id){
            $join->on('users.id','=','blogs.user_id')->where('blogs.cat_id',$cat_id);
       })->where('blogs.status',1)->select('blogs.*','users.name as postername','users.image as userimage','users.username')->orderBy('created_at','desc')->get();

       return view('view_by_category')->with(compact('blogs','ogimage','ogdescription','ogtitle','user'));
   }
    


    
}
