<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    //
    protected $fillable = ['title','subtitle','content','image','cat_id','tags','user_id','slug'];
}
