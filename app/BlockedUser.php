<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockedUser extends Model
{
    //

    protected $fillable=['user_id','blocked_id'];
}
