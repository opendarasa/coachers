<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claps extends Model
{
    //
    protected $fillable = ['uid','blogid'];
}
