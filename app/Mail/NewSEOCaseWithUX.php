<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewSEOCaseWithUX extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $filename;
    public $fileUrl;
    public function __construct($filename,$fileUrl)
    {
        //
        $this->filename=$filename;
        $this->fileUrl=$fileUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.NewPosterCaseWithUX')->attach($this->filename);
    }
}
