<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ThankYouForYourMessage extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *$name,$email,$phone,$message
     * @return void
     */
    public $name;
    public $email;
    public $phone;
    public $message;
    public function __construct($name,$email,$phone,$message)
    {
        //
        $this->name=$name;
        $this->email=$email;
        $this->phone=$phone;
        $this->message=$message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.thankyou');
    }
}
