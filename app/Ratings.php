<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ratings extends Model
{
    //

   protected $fillable=['rater_id','rated_id','rating','comment'];
}
